﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizzHeavenTests.TestHelpers.MockHelpers
{
    public class AspNetIConfigurationMock
    {
        public IConfiguration Configuration { get; private set; }

        public AspNetIConfigurationMock()
        {
            var IConfigurationDictionaryMock = new Dictionary<string, string>
            {
                {"UserAuthJwt:Issuer", "https://localhost:44341"},
                {"UserAuthJwt:SigningKey", "LongTestPassword123456"},
                {"UserAuthJwt:ExpiryInMinutes", "60"}
            };

            Configuration = new ConfigurationBuilder().AddInMemoryCollection(IConfigurationDictionaryMock).Build();
        }
    }
}