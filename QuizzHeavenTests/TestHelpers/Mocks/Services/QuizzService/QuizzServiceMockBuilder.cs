﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using QuizzHeaven.Data;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Services;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Factories;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeavenTests.TestHelpers.Mocks
{
    public class QuizzServiceMockBuilder
    {
        MockAppDbContext dbContext;
        UserManager<AppUser> userManager;

        public QuizzServiceMockBuilder()
        {
        }

        public void SetDbContext(MockAppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void SetUserManager(UserManager<AppUser> userManager)
        {
            this.userManager = userManager;
        }

        public IQuizzService Build()
        {
            var crudFacade = new CrudFacade<Quizz>(dbContext);
            var mapper = new MapperConfiguration(config =>
            {
                config.AddProfile(new CreateQuizzInputProfile());
                config.AddProfile(new EditQuizzInputProfile());
                config.AddProfile(new QuizzVmProfile());
            }).CreateMapper();

            var quizzService = new QuizzService(crudFacade, mapper, userManager);
            return quizzService;
        }
    }
}
