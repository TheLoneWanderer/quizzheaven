﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using QuizzHeaven.Data;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Services;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeavenTests.TestHelpers.Mocks
{
    public class AnswersServiceMockBuilder
    {
        MockAppDbContext dbContext;
        UserManager<AppUser> userManager;

        public AnswersServiceMockBuilder()
        {
        }

        public void SetDbContext(MockAppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void SetUserManager(UserManager<AppUser> userManager)
        {
            this.userManager = userManager;
        }

        public IAnswersService Build()
        {
            var displayCrudFacade = new DisplayOrderedCrudFacade<Answer>(dbContext);
            var answersMapper = new MapperConfiguration(config =>
            {
                config.AddProfile(new CreateAnswerInputProfile());
                config.AddProfile(new EditAnswerInputProfile());
                config.AddProfile(new AnswerVmProfile());
            }).CreateMapper();

            var quizzServiceBuilder = new QuizzServiceMockBuilder();
            quizzServiceBuilder.SetDbContext(dbContext);
            quizzServiceBuilder.SetUserManager(userManager);

            var quizzService = quizzServiceBuilder.Build();

            var answersService = new AnswersService(displayCrudFacade, answersMapper, quizzService);
            return answersService;
        }
    }
}
