﻿using QuizzHeaven.Services;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeavenTests.TestHelpers.Mocks
{
    public class QuizzContentServiceMockFactory
    {
        MockAppDbContext dbContext;

        public QuizzContentServiceMockFactory(MockAppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public IQuizzContentService Create()
        {
            var solvedStatsService = new SolvedQuizzStatsServiceMockFactory(dbContext).Create();
            var quizzContentService = new QuizzContentService(dbContext, solvedStatsService);

            return quizzContentService;
        }
    }
}
