﻿using QuizzHeaven.Services;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeavenTests.TestHelpers.Mocks
{
    public class QuizzResultsServiceMockFactory
    {
        MockAppDbContext dbContext;

        public QuizzResultsServiceMockFactory(MockAppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public IQuizzResultsService Create()
        {
            var solvedStatsService = new SolvedQuizzStatsServiceMockFactory(dbContext).Create();
            var quizzResultsService = new QuizzResultsService(dbContext, solvedStatsService);

            return quizzResultsService;
        }
    }
}
