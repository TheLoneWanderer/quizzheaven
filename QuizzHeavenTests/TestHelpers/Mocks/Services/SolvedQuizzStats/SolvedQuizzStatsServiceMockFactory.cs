﻿using AutoMapper;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Services;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeavenTests.TestHelpers.Mocks
{
    public class SolvedQuizzStatsServiceMockFactory
    {
        MockAppDbContext dbContext;

        public SolvedQuizzStatsServiceMockFactory(MockAppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public ISolvedQuizzStatsService Create()
        {
            var solvedStatsMapper = new MapperConfiguration(config =>
            {
                config.AddProfile(new SolvedQuizzStatsVmProfile());
            }).CreateMapper();

            var solvedQuizzStatsService = new SolvedQuizzStatsService(dbContext, solvedStatsMapper);
            return solvedQuizzStatsService;
        }
    }
}
