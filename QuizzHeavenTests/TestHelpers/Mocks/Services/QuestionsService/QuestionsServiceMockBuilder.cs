﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using QuizzHeaven.Data;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Services;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeavenTests.TestHelpers.Mocks
{
    public class QuestionsServiceMockBuilder
    {
        MockAppDbContext dbContext;
        UserManager<AppUser> userManager;

        public QuestionsServiceMockBuilder()
        {
        }

        public void SetDbContext(MockAppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void SetUserManager(UserManager<AppUser> userManager)
        {
            this.userManager = userManager;
        }

        public IQuestionsService Build()
        {
            var displayCrudFacade = new DisplayOrderedCrudFacade<Question>(dbContext);
            var questionsMapper = new MapperConfiguration(config =>
            {
                config.AddProfile(new CreateQuestionInputProfile());
                config.AddProfile(new EditQuestionInputProfile());
                config.AddProfile(new QuestionVmProfile());
            }).CreateMapper();

            var quizzServiceBuilder = new QuizzServiceMockBuilder();
                quizzServiceBuilder.SetDbContext(dbContext);
                quizzServiceBuilder.SetUserManager(userManager);

            var quizzService = quizzServiceBuilder.Build();

            var questionsService = new QuestionsService(displayCrudFacade, questionsMapper, quizzService);
            return questionsService;
        }
    }
}
