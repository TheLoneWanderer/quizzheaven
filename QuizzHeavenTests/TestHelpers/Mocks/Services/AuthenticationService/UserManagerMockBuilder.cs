﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using QuizzHeaven.Models.DTO;
using System;
using System.Collections.Generic;

namespace QuizzHeavenTests.TestHelpers.Mocks
{
    public class UserManagerMockBuilder<TAppUser> where TAppUser : IdentityUser
    {
        Mock<UserManager<TAppUser>> userManagerMock;

        public UserManagerMockBuilder() 
        {
            userManagerMock = new Mock<UserManager<TAppUser>>(
                    new Mock<IUserStore<TAppUser>>().Object,
                    new Mock<IOptions<IdentityOptions>>().Object,
                    new Mock<IPasswordHasher<TAppUser>>().Object,
                    new IUserValidator<TAppUser>[0],
                    new IPasswordValidator<TAppUser>[0],
                    new Mock<ILookupNormalizer>().Object,
                    new Mock<IdentityErrorDescriber>().Object,
                    new Mock<IServiceProvider>().Object,
                    new Mock<ILogger<UserManager<TAppUser>>>().Object);
        }

        public UserManager<TAppUser> GetResultMock()
        {
            return userManagerMock.Object;
        }
        
        public void SetupMock_FindBy_ReturnsUser(TAppUser returnedUser)
        {
            userManagerMock.Setup(userMan => userMan
                .FindByEmailAsync(It.IsAny<string>()))
                .ReturnsAsync(returnedUser);

            userManagerMock.Setup(userMan => userMan
                .FindByNameAsync(It.IsAny<string>()))
                .ReturnsAsync(returnedUser);
        }

        public void SetupMock_FindBy_DoesNotReturnUser()
        {
            userManagerMock.Setup(userMan => userMan
                .FindByEmailAsync(It.IsAny<string>()))
                .ReturnsAsync(() => null);

            userManagerMock.Setup(userMan => userMan
                .FindByNameAsync(It.IsAny<string>()))
                .ReturnsAsync(() => null);
        }

        public void SetupMock_AddToRoleAsync_Succeeds()
        {
            userManagerMock.Setup(userMan => userMan
                .AddToRoleAsync(It.IsAny<TAppUser>(), It.IsAny<string>()))
                .ReturnsAsync(new IdentityResult());
        }

        
        public void SetupMock_GetRoles_ReturnsRole()
        {
            userManagerMock.Setup(userMan => userMan
                .GetRolesAsync(It.IsAny<TAppUser>()))
                .ReturnsAsync(new List<string>() { "User" });
        }
        
        public void SetupMock_GetRoles_ReturnsNoRoles()
        {
            userManagerMock.Setup(userMan => userMan
                .GetRolesAsync(It.IsAny<TAppUser>()))
                .ReturnsAsync(() => null);
        }

        public void SetupMock_CreateAsync_Succeeds()
        {
            userManagerMock.Setup(userMan =>
                userMan.CreateAsync(It.IsAny<TAppUser>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Success);
        }
        
        public void SetupMock_CreateAsync_Fails()
        {
            userManagerMock.Setup(userMan =>
                userMan.CreateAsync(It.IsAny<TAppUser>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Failed());
        }
    }
}
