﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeavenTests.TestHelpers.Mocks
{
    public class SignInManagerMockBuilder<TAppUser> where TAppUser : IdentityUser
    {
        Mock<SignInManager<TAppUser>> signInManagerMock;

        public SignInManagerMockBuilder(UserManager<TAppUser> userManager)
        {
            signInManagerMock = new Mock<SignInManager<TAppUser>>(
                userManager,
                new Mock<IHttpContextAccessor>().Object,
                new Mock<IUserClaimsPrincipalFactory<TAppUser>>().Object,
                new Mock<IOptions<IdentityOptions>>().Object,
                new Mock<ILogger<SignInManager<TAppUser>>>().Object,
                new Mock<IAuthenticationSchemeProvider>().Object,
                new Mock<IUserConfirmation<TAppUser>>().Object);
        }

        public SignInManager<TAppUser> GetResultMock()
        {
            return signInManagerMock.Object;
        }

        public void SetupMock_PasswordSignInAsync_Succeeds()
        {
            signInManagerMock.Setup(signInMan => signInMan
                .PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .ReturnsAsync(SignInResult.Success);
        }

        public void SetupMock_PasswordSignInAsync_Fails()
        {
            signInManagerMock.Setup(signInMan => signInMan
                .PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .ReturnsAsync(SignInResult.Failed);
        }
    }
}
