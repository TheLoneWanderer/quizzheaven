﻿using Microsoft.AspNetCore.Http;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeavenTests.TestHelpers.Mocks
{
    public class ClaimsPrincipalMockBuilder
    {
        ClaimsPrincipal claimsPrincipal;

        public ClaimsPrincipalMockBuilder()
        {
            claimsPrincipal = new ClaimsPrincipal();
        }

        public ClaimsPrincipal GetResultMock()
        {
            return claimsPrincipal;
        }

        public ClaimsPrincipalMockBuilder MockSetup_HasUserLoggedIn(string userId)
        {
            var userNameIdentifierClaim = new Claim[] { new Claim(ClaimTypes.NameIdentifier, userId) };
            var claimsIdentity = new ClaimsIdentity(userNameIdentifierClaim);

            claimsPrincipal.AddIdentity(claimsIdentity);

            return this;
        }
    }
}
