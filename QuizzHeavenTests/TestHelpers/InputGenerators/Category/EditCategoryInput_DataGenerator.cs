﻿using QuizzHeaven.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeavenTests.TestHelpers.InputGenerators.Category
{
    public class EditCategoryInput_DataGenerator
    {
        public static IEnumerable<object[]> CorrectInput()
        {
            yield return new object[] { new EditCategoryInput { Id = 1 } };
            yield return new object[] { new EditCategoryInput { Id = 1, Name = "testName" } };
            yield return new object[] { new EditCategoryInput { Id = 1, ParentId = 0 } };
            yield return new object[] { new EditCategoryInput { Id = 1, ParentId = 100 } };
            yield return new object[] { new EditCategoryInput { Id = 1, Name = "testName", ParentId = 100 } };
        }

        public static IEnumerable<object[]> InvalidInput()
        {
            yield return new object[] { new EditCategoryInput { Id = 1, Name = "" } };
            yield return new object[] { new EditCategoryInput { Id = 1, Name = "", ParentId = 100 } };
        }
    }
}
