﻿using QuizzHeaven.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeavenTests.TestHelpers.InputGenerators.Category
{
    public class CreateCategoryInput_DataGenerator
    {
        public static IEnumerable<object[]> CorrectInput()
        {
            yield return new object[] { new CreateCategoryInput { Name = "testName" } };
            yield return new object[] { new CreateCategoryInput { Name = "testName", ParentId = 100 } };
        }

        public static IEnumerable<object[]> InvalidInput()
        {
            yield return new object[] { new CreateCategoryInput {} };
            yield return new object[] { new CreateCategoryInput { Name = "negativeParentId", ParentId = -1 } };
            yield return new object[] { new CreateCategoryInput { Name = "", ParentId = 0 } };
        }
    }
}
