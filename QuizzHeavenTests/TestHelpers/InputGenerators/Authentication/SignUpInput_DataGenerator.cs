﻿using QuizzHeaven.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeavenTests.TestHelpers.InputGenerators.Authentication
{
    public class SignUpInput_DataGenerator
    {
        public static IEnumerable<object[]> CorrectCredentials()
        {
            yield return new object[] { new SignUpInput { Login = "login", Password = "pass", ConfirmedPassword = "pass", Email = "email@g.com"} };
        }

        public static IEnumerable<object[]> InvalidCredentials()
        {
            yield return new object[] { new SignUpInput { Login = "", Password = "", ConfirmedPassword = "", Email = "" } };
            yield return new object[] { new SignUpInput { Login = "login", Password = "", ConfirmedPassword = "", Email = "" } };
            yield return new object[] { new SignUpInput { Login = "login", Password = "", ConfirmedPassword = "", Email = "email@g.com" } };
            yield return new object[] { new SignUpInput { Login = "login", Password = "pass", ConfirmedPassword = "pass", Email = "invalidEmail" } };
            yield return new object[] { new SignUpInput { Login = "login", Password = "pass", ConfirmedPassword = "notEqualPass", Email = "email@g.com" } };
        }
    }
}
