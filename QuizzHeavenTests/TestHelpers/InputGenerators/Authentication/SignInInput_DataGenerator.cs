﻿using QuizzHeaven.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeavenTests.TestHelpers.InputDataGenerators
{
    public class SignInInput_DataGenerator
    {
        public static IEnumerable<object[]> CorrectCredentials()
        {
            yield return new object[] { new SignInInput { Login = "login", Password = "pass" } };
            yield return new object[] { new SignInInput { Login = "longLoginWithoutNumbers", Password = "pass" } };
            yield return new object[] { new SignInInput { Login = "longLoginWithNumbers99", Password = "pass" } };
            yield return new object[] { new SignInInput { Login = "longEmailAsLogin99@g.com", Password = "pass" } };                
        }

        public static IEnumerable<object[]> InvalidFormatCredentials()
        {
            yield return new object[] { new SignInInput { Login = "", Password = "" } };
            yield return new object[] { new SignInInput { Login = "login", Password = "" } };
            yield return new object[] { new SignInInput { Login = "loginWithUnderscores___", Password = "pass" } };
            yield return new object[] { new SignInInput { Login = "___", Password = "pass" } };
            yield return new object[] { new SignInInput { Login = "@", Password = "pass" } };
            yield return new object[] { new SignInInput { Login = "@g", Password = "pass" } };
            yield return new object[] { new SignInInput { Login = "@g.com", Password = "pass" } };
        }
    }
}
