﻿using QuizzHeaven.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeavenTests.TestHelpers.InputGenerators.Quizz
{
    public class EditQuizzInput_DataGenerator
    {
        public static IEnumerable<object[]> CorrectInput()
        {
            yield return new object[] { new EditQuizzInput {} };
            yield return new object[] { new EditQuizzInput { Name = "quizzName" } };
            yield return new object[] { new EditQuizzInput { Description = "" } };
            yield return new object[] { new EditQuizzInput { Description = "description" } };
            yield return new object[] { new EditQuizzInput { CategoryId = 1 } };
            yield return new object[] { new EditQuizzInput { Name = "quizzName", Description = "", CategoryId = 1 } };
        }

        public static IEnumerable<object[]> InvalidInput()
        {
            yield return new object[] { new EditQuizzInput { Name = "" } };
        }
    }
}
