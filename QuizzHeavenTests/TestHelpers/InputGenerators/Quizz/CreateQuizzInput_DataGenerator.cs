﻿using QuizzHeaven.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeavenTests.TestHelpers.InputGenerators.Quizz
{
    public class CreateQuizzInput_DataGenerator
    {
        public static IEnumerable<object[]> CorrectInput()
        {
            yield return new object[] { new CreateQuizzInput { Name = "quizzName", Description = "", CategoryId = 1, AuthorId = "userId" } };
            yield return new object[] { new CreateQuizzInput { Name = "quizzName", Description = "description", CategoryId = 1, AuthorId = "userId" } };
        }

        public static IEnumerable<object[]> InvalidInput()
        {
            yield return new object[] { new CreateQuizzInput {} };
            yield return new object[] { new CreateQuizzInput { Name = "onlyName" } };
        }
    }
}
