﻿using QuizzHeaven.Models.DTO.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeavenTests.TestHelpers.InputGenerators.Common.Search
{
    public class SearchParameters_DataGenerator
    {
        public static IEnumerable<object[]> CorrectInput()
        {
            yield return new object[] { new SearchParameters { PageNumber = 1, PageSize = 10 } };
        }

        public static IEnumerable<object[]> InvalidInput()
        {
            yield return new object[] { new SearchParameters { PageNumber = 1, PageSize = 0 } };
            yield return new object[] { new SearchParameters { PageNumber = 0, PageSize = 1 } };
        }
    }
}
