﻿using QuizzHeaven.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeavenTests.TestHelpers.InputGenerators.Question
{
    public class CreateQuestionInput_DataGenerator
    {
        public static IEnumerable<object[]> CorrectInput()
        {
            yield return new object[] { new CreateQuestionInput { Content = "test", QuizzId = 1, DisplayOrder = 1 } };
        }

        public static IEnumerable<object[]> InvalidInput()
        {
            yield return new object[] { new CreateQuestionInput { } };
            yield return new object[] { new CreateQuestionInput { Content = "", QuizzId = 1, DisplayOrder = 1 } };
            yield return new object[] { new CreateQuestionInput { Content = "test", QuizzId = 1, DisplayOrder = 0 } };
        }
    }
}
