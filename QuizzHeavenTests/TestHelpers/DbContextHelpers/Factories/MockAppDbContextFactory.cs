﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using QuizzHeaven.Data;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z.EntityFramework.Extensions;

namespace QuizzHeavenTests.TestHelpers.DbContextHelpers.Factories
{
    public class MockAppDbContextFactory
    {
        public MockAppDbContextFactory()
        {
        }

        public MockAppDbContext Create()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>()
                                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                                .ConfigureWarnings(config => config.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                                .Options;

            EntityFrameworkManager.ContextFactory = context => new MockAppDbContext(options);

            var dbContext = new MockAppDbContext(options);
            return dbContext;
        }
    }
}
