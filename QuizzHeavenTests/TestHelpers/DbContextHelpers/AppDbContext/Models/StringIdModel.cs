﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuizzHeavenTests.TestHelpers.DbContextHelpers.Models
{
    public class StringIdModel
    {
        public string Id { get; set; }
        public string ParentId { get; set; }

        public virtual StringIdModel ParentEntity { get; set; }
        public virtual ICollection<StringIdModel> ChildEntities { get; set; }
    }
}
