﻿using QuizzHeaven.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeavenTests.TestHelpers.DbContextHelpers.Models
{
    public class DisplayOrderableModel : IDisplayOrderable
    {
        public DisplayOrderableModel()
        {
        }

        public DisplayOrderableModel(int id, short displayOrder)
        {
            Id = id;
            DisplayOrder = displayOrder;
        }

        public int Id { get; set; }
        public short DisplayOrder { get; set; }
    }
}
