﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuizzHeavenTests.TestHelpers.DbContextHelpers.Models
{
    public class NumericIdModel
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }

        public virtual NumericIdModel ParentEntity { get; set; }
        public virtual ICollection<NumericIdModel> ChildEntities { get; set; }
    }
}
