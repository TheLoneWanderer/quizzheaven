﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Models.Domain;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeavenTests.TestHelpers.DbContextHelpers.Seeders
{
    public class MockAppDbContextDataSeeder
    {
        MockAppDbContext dbContext;

        public MockAppDbContextDataSeeder(MockAppDbContext mockDbContext)
        {
            dbContext = mockDbContext;
        }

        public EntityEntry<TEntity> AddEntity<TEntity>(TEntity entity) where TEntity : class
        {
            var entityEntry = dbContext.Set<TEntity>().Add(entity);

            dbContext.SaveChanges();
            dbContext.DetachAllEntities();

            return entityEntry;
        }

        public EntityEntry<StringIdModel> AddStringIdModel(string entityKey = "test", string entityParentKey = null)
        {
            var entity = CreateStringIdModel(entityKey, entityParentKey);
            var entityEntry = dbContext.Add(entity);
            dbContext.SaveChanges();
            dbContext.DetachAllEntities();

            return entityEntry;
        }

        public void AddRandomStringIdModels(int newEntitiesAmount)
        {
            for (int i = 0; i < newEntitiesAmount; i++)
            {
                var testEntity = CreateStringIdModel(Guid.NewGuid().ToString());
                dbContext.Add(testEntity);
            }
            dbContext.SaveChanges();
            dbContext.DetachAllEntities();
        }

        public StringIdModel CreateStringIdModel(string entityKey = "test", string entityParentKey = null)
        {
            return new StringIdModel()
            {
                Id = entityKey,
                ParentId = entityParentKey
            };
        }


        public EntityEntry<NumericIdModel> AddNumericIdModel(int id = 0, int? parentId = null)
        {
            var entity = CreateNumericIdModel(id, parentId);
            var entityEntry = dbContext.Add(entity);
            dbContext.SaveChanges();
            dbContext.DetachAllEntities();

            return entityEntry;
        }

        public void AddRandomNumericIdModels(int newEntitiesAmount, int smallestRandomId = 1000000)
        {
            var rng = new Random();
            for (int i = 0; i < newEntitiesAmount; i++)
            {
                var testEntity = CreateNumericIdModel(rng.Next(smallestRandomId, int.MaxValue));
                dbContext.Add(testEntity);
            }

            dbContext.SaveChanges();
            dbContext.DetachAllEntities();
        }

        public NumericIdModel CreateNumericIdModel(int id = 0, int? parentId = null)
        {
            return new NumericIdModel()
            {
                Id = id,
                ParentId = parentId
            };
        }

        /// <summary>
        /// Each entity will have its display order incremented automatically
        /// <br></br>
        /// For example, if 10 entities are added, their displayOrders will be in the range from 1 to 10
        /// </summary>
        /// <param name="displayOrder"></param>
        /// <param name="entitiesAmount"></param>
        public void AddDisplayOrderedModels(int entitiesAmount = 1)
        {
            for(int i=1; i<=entitiesAmount; i++)
            {
                var entity = new DisplayOrderableModel() { Id = i, DisplayOrder = (short)i };
                dbContext.Add(entity);
            }

            dbContext.SaveChanges();
            dbContext.DetachAllEntities();
        }



        public Quizz AddQuizzEntity(int id = 1)
        {
            var quizz = new Quizz() 
            { 
                Id = id, 
                VersionStamp = Guid.Empty 
            };

            AddEntity(quizz);
            return quizz;
        }

        /// <summary>
        /// The associated Quizz entity will be created either if not found
        /// </summary>
        /// <param name="id"></param>
        /// <param name="quizzId"></param>
        /// <returns></returns>
        public Question AddQuestionEntity(int id = 10, int quizzId = 1)
        {
            var associatedQuizz = dbContext.Find<Quizz>(quizzId);
            if(associatedQuizz == null)
            {
                AddQuizzEntity();
            }

            var someQuestion = new Question() 
            { 
                Id = id, 
                Content = "questionContent", 
                DisplayOrder = 1, 
                QuizzId = quizzId 
            };

            AddEntity(someQuestion);
            return someQuestion;
        }

        /// <summary>
        /// The associated Quizz and Question entities will be created either if not found
        /// </summary>
        /// <param name="isCorrectAnswer"></param>
        /// <param name="id"></param>
        /// <param name="questionId"></param>
        /// <param name="quizzId"></param>
        /// <returns></returns>
        public Answer AddAnswerEntity(bool isCorrectAnswer = false, int id = 100, int questionId = 10, int quizzId = 1)
        {
            var associatedQuestion = dbContext.Find<Question>(questionId);
            if(associatedQuestion == null)
            {
                AddQuestionEntity(questionId, quizzId);
            }

            var someAnswer = new Answer() 
            { 
                Id = id, 
                Content = "answerContent", 
                IsCorrectAnswer = isCorrectAnswer, 
                DisplayOrder = (short)id, 
                QuestionId = questionId 
            };

            AddEntity(someAnswer);
            return someAnswer;
        }


        /// <summary>
        /// Each answer will have its ID incremented, starting from the chosen value
        /// </summary>
        /// <param name="answersAmount"></param>
        /// <param name="firstAnswerId"></param>
        /// <param name="questionId"></param>
        /// <param name="quizzId"></param>
        public void AddQuestionWithAnswers(int answersAmount, int firstAnswerId = 100, int questionId = 10, int quizzId = 1)
        {
            var associatedQuestion = dbContext.Find<Question>(questionId);
            if (associatedQuestion == null)
            {
                AddQuestionEntity(questionId, quizzId);
            }

            AddAnswerEntity(true, firstAnswerId, questionId, quizzId);
            for (int answerIndex = 1; answerIndex < answersAmount; answerIndex++)
            {
                AddAnswerEntity(false, firstAnswerId + answerIndex, questionId, quizzId);
            }
        }
    }
}
