﻿using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks
{
    public class MockAppDbContext : AppDbContext
    {
        public DbSet<StringIdModel> StringIdModels { get; set; }
        public DbSet<NumericIdModel> NumericModels { get; set; }
        public DbSet<DisplayOrderableModel> DisplayOrderableModels { get; set; }
        

        public MockAppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<StringIdModel>().HasKey(m => m.Id);
            modelBuilder.Entity<NumericIdModel>().HasKey(m => m.Id);
            modelBuilder.Entity<DisplayOrderableModel>().HasKey(m => m.Id);

            modelBuilder.Entity<StringIdModel>()
                .HasMany(m => m.ChildEntities)
                .WithOne(m => m.ParentEntity)
                .HasForeignKey(m => m.ParentId);
            
            modelBuilder.Entity<NumericIdModel>()
                .HasMany(m => m.ChildEntities)
                .WithOne(m => m.ParentEntity)
                .HasForeignKey(m => m.ParentId);
        }
    }
}
