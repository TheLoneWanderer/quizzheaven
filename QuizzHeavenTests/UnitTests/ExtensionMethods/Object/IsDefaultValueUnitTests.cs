﻿using QuizzHeaven.Helpers.ExtensionMethods.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace QuizzHeavenTests.UnitTests.ExtensionMethods
{
    public class IsDefaultValueUnitTests
    {

        [Theory]
        [InlineData(0)]
        [InlineData(null)]
        public void IsDefaultValue_DefaultValues_ShouldReturnTrue(object value)
        {
            Assert.True(value.IsDefaultValue());
        }
    }
}
