﻿using QuizzHeaven.Helpers.ExtensionMethods.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace QuizzHeavenTests.UnitTests.ExtensionMethods.Object
{
    public class IsOfTypeOrValueUnitTests
    {
        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        public void IsAnEmptyString_CorrectValues_ShouldSucceed(object value)
        {
            Assert.True(value.IsAnEmptyString());
        }

        [Theory]
        [InlineData(0)]
        [InlineData(null)]
        [InlineData("notEmptyValue")]
        public void IsAnEmptyString_IncorrectValues_ShouldFail(object value)
        {
            Assert.False(value.IsAnEmptyString());
        }
    }
}
