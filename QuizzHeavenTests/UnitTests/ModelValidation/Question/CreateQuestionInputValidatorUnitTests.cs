﻿using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Seeders;
using QuizzHeavenTests.TestHelpers.InputGenerators.Question;
using QuizzHeavenTests.TestHelpers.MockHelpers;
using QuizzHeavenTests.TestHelpers.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace QuizzHeavenTests.UnitTests.ModelValidation
{
    public class CreateQuestionInputValidatorUnitTests
    {
        MockAppDbContextDataSeeder dbContextSeeder;

        CreateQuestionInputValidator validator;

        public CreateQuestionInputValidatorUnitTests()
        {
            var dbContextOptions = new DbContextOptionsBuilder<AppDbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
            var dbContext = new MockAppDbContext(dbContextOptions);
            dbContextSeeder = new MockAppDbContextDataSeeder(dbContext);

            validator = new CreateQuestionInputValidator(dbContext);
        }

        [Theory]
        [MemberData(nameof(CreateQuestionInput_DataGenerator.CorrectInput), MemberType = typeof(CreateQuestionInput_DataGenerator))]
        public void Validate_CorrectInput(CreateQuestionInput input)
        {
            var quizzAuthorId = "testUser";
            dbContextSeeder.AddEntity(new Quizz() { Id = input.QuizzId, AuthorId = quizzAuthorId });

            var validationResult = validator.Validate(input);
            Assert.True(validationResult.IsValid);
        }

        [Theory]
        [MemberData(nameof(CreateQuestionInput_DataGenerator.InvalidInput), MemberType = typeof(CreateQuestionInput_DataGenerator))]
        public void Validate_InvalidInput(CreateQuestionInput input)
        {
            var quizzAuthorId = "testUser";
            dbContextSeeder.AddEntity(new Quizz() { Id = input.QuizzId, AuthorId = quizzAuthorId });

            var validationResult = validator.Validate(input);
            Assert.False(validationResult.IsValid);
        }
    }
}
