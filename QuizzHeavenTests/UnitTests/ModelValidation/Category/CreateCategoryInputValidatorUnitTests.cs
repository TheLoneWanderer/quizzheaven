﻿using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Factories;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Seeders;
using QuizzHeavenTests.TestHelpers.InputGenerators.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace QuizzHeavenTests.UnitTests.ModelValidation
{
    public class CreateCategoryInputValidatorUnitTests
    {
        MockAppDbContextDataSeeder dbDataSeeder;
        CreateCategoryInputValidator validator;

        public CreateCategoryInputValidatorUnitTests()
        {
            var mockAppDbContext = new MockAppDbContextFactory().Create();

            dbDataSeeder = new MockAppDbContextDataSeeder(mockAppDbContext);
            validator = new CreateCategoryInputValidator(mockAppDbContext);
        }

        [Theory]
        [MemberData(nameof (CreateCategoryInput_DataGenerator.CorrectInput), MemberType = typeof(CreateCategoryInput_DataGenerator))]
        public void CreateCategoryValidator_CorrectInput(CreateCategoryInput input)
        {
            SeedInputRelatedEntities(input);

            var validationResult = validator.Validate(input);
            Assert.True(validationResult.IsValid);
        }

        [Theory]
        [MemberData(nameof (CreateCategoryInput_DataGenerator.InvalidInput), MemberType = typeof(CreateCategoryInput_DataGenerator))]
        public void CreateCategoryValidator_IncorrectInput(CreateCategoryInput input)
        {
            SeedInputRelatedEntities(input);

            var validationResult = validator.Validate(input);
            Assert.False(validationResult.IsValid);
        }
        
        [Fact]
        public void CreateCategoryValidator_ParentDoesNotExist_ShouldFail()
        {
            var categoryWithNonExistentParent = new CreateCategoryInput() { Name = "someName", ParentId = 10 };
            Assert.Throws<NotFoundException>(() => { validator.Validate(categoryWithNonExistentParent); });
        }

        private void SeedInputRelatedEntities(CreateCategoryInput input)
        {
            if (input.ParentId > 0)
            {
                var quizzCategory = new Category() { Id = input.ParentId.Value, Name = Guid.NewGuid().ToString() };
                dbDataSeeder.AddEntity(quizzCategory);
            }
        }
    }
}
