﻿using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Factories;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Seeders;
using QuizzHeavenTests.TestHelpers.InputGenerators.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace QuizzHeavenTests.UnitTests.ModelValidation
{
    public class EditCategoryInputValidatorUnitTests
    {
        MockAppDbContextDataSeeder dbDataSeeder;
        EditCategoryInputValidator validator;

        public EditCategoryInputValidatorUnitTests()
        {
            var mockAppDbContext = new MockAppDbContextFactory().Create();

            dbDataSeeder = new MockAppDbContextDataSeeder(mockAppDbContext);
            validator = new EditCategoryInputValidator(mockAppDbContext);
        }

        [Theory]
        [MemberData(nameof(EditCategoryInput_DataGenerator.CorrectInput), MemberType = typeof(EditCategoryInput_DataGenerator))]
        public void EditCategoryValidator_CorrectInput(EditCategoryInput input)
        {
            SeedInputRelatedEntities(input);

            var validationResult = validator.Validate(input);
            Assert.True(validationResult.IsValid);
        }

        [Theory]
        [MemberData(nameof(EditCategoryInput_DataGenerator.InvalidInput), MemberType = typeof(EditCategoryInput_DataGenerator))]
        public void EditCategoryValidator_IncorrectInput(EditCategoryInput input)
        {
            SeedInputRelatedEntities(input);

            var validationResult = validator.Validate(input);
            Assert.False(validationResult.IsValid);
        }

        [Fact]
        public void EditCategoryInputValidator_CategoryDoesNotExist_ShouldFail()
        {
            var nonExistentCategory = new EditCategoryInput() { Id = 1, Name = "someName" };
            Assert.Throws<NotFoundException>(() => { validator.Validate(nonExistentCategory); });
        }
        
        [Fact]
        public void EditCategoryInputValidator_BeeingItsOwnParent_ShouldFail()
        {
            var categoryId = 1;
            var originalEntity = new Category() { Id = categoryId, Name = "someName", ParentId = null };

            var becomeYourOwnParentInput = new EditCategoryInput() { Id = 1, ParentId = categoryId };
            Assert.Throws<NotFoundException>(() => { validator.Validate(becomeYourOwnParentInput); });
        }



        private void SeedInputRelatedEntities(EditCategoryInput input)
        {
            if (input.Id == 0)
            {
                throw new ArgumentNullException("EditCategoryInput is missing the ID property");
            }

            var unmodifiedCategory = new Category() { Id = input.Id, Name = Guid.NewGuid().ToString(), ParentId = null };
            dbDataSeeder.AddEntity(unmodifiedCategory);

            if (input.ParentId > 0)
            {
                var parentCategory = new Category() { Id = input.ParentId.Value, Name = Guid.NewGuid().ToString() };
                dbDataSeeder.AddEntity(parentCategory);
            }
        }
    }
}
