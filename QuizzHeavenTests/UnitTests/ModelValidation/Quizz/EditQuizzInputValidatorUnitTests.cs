﻿using FluentValidation.TestHelper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Moq;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Factories;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Seeders;
using QuizzHeavenTests.TestHelpers.InputGenerators.Quizz;
using QuizzHeavenTests.TestHelpers.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace QuizzHeavenTests.UnitTests.ModelValidation
{
    public class EditQuizzInputValidatorUnitTests
    {
        MockAppDbContextDataSeeder dbContextDataSeeder;
        MockAppDbContext dbContext;

        public EditQuizzInputValidatorUnitTests()
        {
            var dbContextFactory = new MockAppDbContextFactory();

            dbContext = dbContextFactory.Create();
            dbContextDataSeeder = new MockAppDbContextDataSeeder(dbContext);
        }

        [Theory]
        [MemberData(nameof(EditQuizzInput_DataGenerator.CorrectInput), MemberType = typeof(EditQuizzInput_DataGenerator))]
        public void Validate_CorrectInput(EditQuizzInput input)
        {
            SeedInputRelatedEntities(input);

            var validationResult = new EditQuizzInputValidator(dbContext).Validate(input);
            Assert.True(validationResult.IsValid);
        }

        [Theory]
        [MemberData(nameof(EditQuizzInput_DataGenerator.InvalidInput), MemberType = typeof(EditQuizzInput_DataGenerator))]
        public void Validate_InvalidInput(EditQuizzInput input)
        {
            SeedInputRelatedEntities(input);

            var validationResult = new EditQuizzInputValidator(dbContext).Validate(input);
            Assert.False(validationResult.IsValid);
        }

        [Fact]
        public void Validate_ShouldThrowConflict_WhenQuizzNameAlreadyExists()
        {
            var existingQuizz = dbContextDataSeeder.AddEntity(new Quizz() { Name = "testName" }).Entity;
            var duplicateInput = new EditQuizzInput() { Name = existingQuizz.Name };

            var validator = new EditQuizzInputValidator(dbContext);
            Assert.Throws<ConflictException>(() => validator.Validate(duplicateInput));
        }

        [Fact]
        public void Validate_ShouldThrowNotFound_WhenCategoryDoesNotExist()
        {
            var quizzWithNonExistingCategory = new EditQuizzInput { Name = "quizzName", Description = "", CategoryId = 57 };

            var validator = new EditQuizzInputValidator(dbContext);
            Assert.Throws<NotFoundException>(() => validator.Validate(quizzWithNonExistingCategory));
        }

        private void SeedInputRelatedEntities(EditQuizzInput input)
        {
            if (input.CategoryId != 0)
            {
                var quizzCategory = new Category() { Id = input.CategoryId };
                dbContextDataSeeder.AddEntity(quizzCategory);
            }
        }
    }
}
