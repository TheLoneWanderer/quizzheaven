﻿using FluentValidation.TestHelper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Moq;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Factories;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Seeders;
using QuizzHeavenTests.TestHelpers.InputGenerators.Quizz;
using QuizzHeavenTests.TestHelpers.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace QuizzHeavenTests.UnitTests.ModelValidation
{
    public class CreateQuizzInputValidatorUnitTests
    {
        MockAppDbContextDataSeeder dbContextDataSeeder;
        MockAppDbContext dbContext;

        public CreateQuizzInputValidatorUnitTests()
        {
            var dbContextFactory = new MockAppDbContextFactory();

            dbContext = dbContextFactory.Create();
            dbContextDataSeeder = new MockAppDbContextDataSeeder(dbContext);
        }

        [Theory]
        [MemberData(nameof(CreateQuizzInput_DataGenerator.CorrectInput), MemberType = typeof(CreateQuizzInput_DataGenerator))]
        public void Validate_CorrectInput(CreateQuizzInput input)
        {
            SeedInputRelatedEntities(input);

            var validationResult = new CreateQuizzInputValidator(dbContext).Validate(input);
            Assert.True(validationResult.IsValid);
        }

        [Theory]
        [MemberData(nameof(CreateQuizzInput_DataGenerator.InvalidInput), MemberType = typeof(CreateQuizzInput_DataGenerator))]
        public void Validate_InvalidInput(CreateQuizzInput input)
        {
            SeedInputRelatedEntities(input);

            var validationResult = new CreateQuizzInputValidator(dbContext).Validate(input);
            Assert.False(validationResult.IsValid);
        }

        [Fact]
        public void Validate_ShouldThrowConflict_WhenQuizzNameAlreadyExists()
        {
            var existingQuizz = dbContextDataSeeder.AddEntity(new Quizz() { Name = "testName" }).Entity;
            var duplicateInput = new CreateQuizzInput() { Name = existingQuizz.Name };

            var validator = new CreateQuizzInputValidator(dbContext);
            Assert.Throws<ConflictException>(() => validator.Validate(duplicateInput));
        }

        [Fact]
        public void Validate_ShouldThrowNotFound_WhenCategoryDoesNotExist()
        {
            var quizzWithNonExistingCategory = new CreateQuizzInput { Name = "quizzName", Description = "", CategoryId = 57 };

            var validator = new CreateQuizzInputValidator(dbContext);
            Assert.Throws<NotFoundException>(() => validator.Validate(quizzWithNonExistingCategory));
        }

        private void SeedInputRelatedEntities(CreateQuizzInput input)
        {
            if (!string.IsNullOrWhiteSpace(input.AuthorId))
            {
                var quizzAuthor = new AppUser() { Id = input.AuthorId };
                dbContextDataSeeder.AddEntity(quizzAuthor);
            }

            if (input.CategoryId != 0)
            {
                var quizzCategory = new Category() { Id = input.CategoryId };
                dbContextDataSeeder.AddEntity(quizzCategory);
            }
        }
    }
}
