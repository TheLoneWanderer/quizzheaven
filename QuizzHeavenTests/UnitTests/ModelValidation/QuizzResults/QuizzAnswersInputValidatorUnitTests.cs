﻿using Moq;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Factories;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Seeders;
using QuizzHeavenTests.TestHelpers.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace QuizzHeavenTests.UnitTests.ModelValidation
{
    public class QuizzAnswersInputValidatorUnitTests
    {
        MockAppDbContext dbContext;
        MockAppDbContextDataSeeder dbContextSeeder;
        QuizzAnswersInputValidator validator;

        public QuizzAnswersInputValidatorUnitTests()
        {
            dbContext = new MockAppDbContextFactory().Create();
            dbContextSeeder = new MockAppDbContextDataSeeder(dbContext);

            var quizzContentService = new QuizzContentServiceMockFactory(dbContext).Create();
            validator = new QuizzAnswersInputValidator(quizzContentService);
        }

        [Fact]
        public void Validate_ShouldSucceed_WhenInputIsCorrect()
        {
            dbContextSeeder.AddQuestionWithAnswers(answersAmount: 2, firstAnswerId: 100, questionId: 10);
            dbContextSeeder.AddQuestionWithAnswers(answersAmount: 2, firstAnswerId: 110, questionId: 11);
            var input = createInputModel(new List<Tuple<int, int>>()
            {
                new Tuple<int, int>(10, 100),
                new Tuple<int, int>(11, 110)
            });

            var validationResult = validator.Validate(input);
            Assert.True(validationResult.IsValid);
        }

        [Fact]
        public void Validate_ShouldFail_WhenQuestionsAreDuplicated()
        {
            dbContextSeeder.AddQuestionWithAnswers(answersAmount: 2, firstAnswerId: 100, questionId: 10);
            dbContextSeeder.AddQuestionWithAnswers(answersAmount: 2, firstAnswerId: 110, questionId: 11);
            var input = createInputModel(new List<Tuple<int, int>>()
            {
                new Tuple<int, int>(10, 100),
                new Tuple<int, int>(10, 100)
            });

            var validationResult = validator.Validate(input);
            Assert.False(validationResult.IsValid);
        }

        [Fact]
        public void Validate_ShouldFail_WhenQuestionsAmountIsWrong()
        {
            dbContextSeeder.AddQuestionWithAnswers(answersAmount: 2, firstAnswerId: 100, questionId: 10);
            dbContextSeeder.AddQuestionWithAnswers(answersAmount: 2, firstAnswerId: 110, questionId: 11);
            var input = createInputModel(new List<Tuple<int, int>>()
            {
                new Tuple<int, int>(10, 100)
            });

            var validationResult = validator.Validate(input);
            Assert.False(validationResult.IsValid);
        }
        
        [Fact]
        public void Validate_ShouldFail_WhenQuestionsDoNotBelongToTheQuizz()
        {
            dbContextSeeder.AddQuestionWithAnswers(answersAmount: 2, firstAnswerId: 100, questionId: 10);
            dbContextSeeder.AddQuestionWithAnswers(answersAmount: 2, firstAnswerId: 110, questionId: 11);
            var input = createInputModel(new List<Tuple<int, int>>()
            {
                new Tuple<int, int>(20, 100),
                new Tuple<int, int>(21, 100)
            });

            var validationResult = validator.Validate(input);
            Assert.False(validationResult.IsValid);
        }

        private QuizzAnswersInput createInputModel(List<Tuple<int, int>> questionAndAnswerIdPairs)
        {
            var quizzId = 1;
            if (!dbContext.Exists<Quizz>(quizzId))
            {
                dbContext.Add(new Quizz() { Id = quizzId });
                dbContext.SaveChanges();
            }

            var userId = "randomUserId";
            if (!dbContext.Exists<AppUser>(userId))
            {
                dbContext.Add(new AppUser() { Id = userId });
                dbContext.SaveChanges();
            }


            var input = new QuizzAnswersInput()
            {
                QuizzId = quizzId,
                UserId = userId,
                AnsweredQuestions = new List<AnsweredQuestionInput>()
            };

            for (int i=0; i<questionAndAnswerIdPairs.Count; i++)
            {
                var answeredQuestionInput = new AnsweredQuestionInput()
                {
                    QuestionId = questionAndAnswerIdPairs[i].Item1,
                    AnswerId = questionAndAnswerIdPairs[i].Item2
                };

                input.AnsweredQuestions.Add(answeredQuestionInput);
            }

            return input;
        }
    }
}
