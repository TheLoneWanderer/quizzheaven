﻿using QuizzHeaven.Models.DTO;
using QuizzHeaven.Models.DTO.Authentication.SignIn;
using QuizzHeavenTests.TestHelpers.InputDataGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace QuizzHeavenTests.UnitTests.ModelValidation
{
    public class SignInInputValidatorUnitTests
    {
        SignInInputValidator validator;

        public SignInInputValidatorUnitTests()
        {
            validator = new SignInInputValidator();
        }

        [Theory]
        [MemberData(nameof(SignInInput_DataGenerator.CorrectCredentials), MemberType = typeof(SignInInput_DataGenerator))]
        public void SignInInputValidator_CorrectCredentials(SignInInput input)
        {
            var validationResult = validator.Validate(input);
            Assert.True(validationResult.IsValid);
        }

        [Theory]
        [MemberData(nameof(SignInInput_DataGenerator.InvalidFormatCredentials), MemberType = typeof(SignInInput_DataGenerator))]
        public void SignInInputValidator_InvalidCredentials(SignInInput input)
        {
            var validationResult = validator.Validate(input);
            Assert.False(validationResult.IsValid);
        }
    }
}
