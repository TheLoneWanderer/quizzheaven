﻿using QuizzHeaven.Models.DTO;
using QuizzHeaven.Models.DTO.Authentication.SignUp;
using QuizzHeavenTests.TestHelpers.InputDataGenerators;
using QuizzHeavenTests.TestHelpers.InputGenerators.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace QuizzHeavenTests.UnitTests.ModelValidation
{
    public class SignUpInputValidatorUnitTests
    {
        SignUpInputValidator validator;

        public SignUpInputValidatorUnitTests()
        {
            validator = new SignUpInputValidator();
        }
        
        [Theory]
        [MemberData(nameof(SignUpInput_DataGenerator.InvalidCredentials), MemberType = typeof(SignUpInput_DataGenerator))]
        public void SignUpInputValidator_InvalidCredentials(SignUpInput input)
        {
            var validationResult = validator.Validate(input);
            Assert.False(validationResult.IsValid);
        }
    }
}
