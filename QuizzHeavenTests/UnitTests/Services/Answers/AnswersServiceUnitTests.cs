﻿using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Services;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Factories;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Seeders;
using QuizzHeavenTests.TestHelpers.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace QuizzHeavenTests.UnitTests.Services
{
    public class AnswersServiceUnitTests
    {
        IAnswersService answersService;
        MockAppDbContext dbContext;
        MockAppDbContextDataSeeder dbDataSeeder;

        public AnswersServiceUnitTests()
        {
            dbContext = new MockAppDbContextFactory().Create();
            dbDataSeeder = new MockAppDbContextDataSeeder(dbContext);

            var answersServiceBuilder = new AnswersServiceMockBuilder();
                answersServiceBuilder.SetDbContext(dbContext);
                answersServiceBuilder.SetUserManager(new UserManagerMockBuilder<AppUser>().GetResultMock());

            answersService = answersServiceBuilder.Build();
        }

        [Fact]
        public async void CreateAsync_ShouldUpdateQuizzVersionStamp()
        {
            var question = dbDataSeeder.AddQuestionEntity();
            

            var createAnswerInput = new CreateAnswerInput() { Content = "content", DisplayOrder = 1, IsCorrectAnswer = false, QuestionId = question.Id };
            await answersService.CreateAsync(createAnswerInput);


            var updatedVersionStamp = (await dbContext.FindAsync<Quizz>(question.QuizzId)).VersionStamp;

            Assert.NotEqual(Guid.Empty, updatedVersionStamp);
        }
        
        [Fact]
        public async void EditPartiallyAsync_ShouldUpdateQuizzVersionStamp()
        {
            var question = dbDataSeeder.AddQuestionEntity();
            var answer = dbDataSeeder.AddAnswerEntity();


            var editAnswerInput = new EditAnswerInput() { Id = answer.Id, Content = "editedContent", DisplayOrder = 1, IsCorrectAnswer = false };
            await answersService.EditPartiallyAsync(editAnswerInput, editAnswerInput.Id);


            var updatedVersionStamp = (await dbContext.FindAsync<Quizz>(question.QuizzId)).VersionStamp;

            Assert.NotEqual(Guid.Empty, updatedVersionStamp);
        }

        [Fact]
        public async void DeleteAsync_ShouldUpdateQuizzVersionStamp()
        {
            var question = dbDataSeeder.AddQuestionEntity();
            var answer = dbDataSeeder.AddAnswerEntity();


            await answersService.DeleteAsync(answer.Id);


            var updatedVersionStamp = (await dbContext.FindAsync<Quizz>(question.QuizzId)).VersionStamp;

            Assert.NotEqual(Guid.Empty, updatedVersionStamp);
        }

        [Fact]
        public async void CreateAsync_ShouldInvalidateExistingCorrectAnswer_WhenInputAnswerIsCorrect()
        {
            var question = dbDataSeeder.AddQuestionEntity();
            var existingCorrectAnswer = dbDataSeeder.AddAnswerEntity(true, 1);


            var createCorrectAnswerInput = new CreateAnswerInput() { Content = "content", IsCorrectAnswer = true, DisplayOrder = 1, QuestionId = question.Id };
            await answersService.CreateAsync(createCorrectAnswerInput);

            
            var previousCorrectAnswer = await dbContext.FindAsync<Answer>(existingCorrectAnswer.Id);

            Assert.False(previousCorrectAnswer.IsCorrectAnswer);
        }

        [Fact]
        public async void EditPartiallyAsync_ShouldInvalidateExistingCorrectAnswer_WhenInputAnswerIsCorrect()
        {
            var existingCorrectAnswer = dbDataSeeder.AddAnswerEntity(true, 1);
            var existingIncorrectAnswer = dbDataSeeder.AddAnswerEntity(false, 2);


            var editAnswerInput = new EditAnswerInput() { Id = existingIncorrectAnswer.Id, Content = "content", IsCorrectAnswer = true, DisplayOrder = 1 };
            await answersService.EditPartiallyAsync(editAnswerInput, editAnswerInput.Id);


            var previousCorrectAnswer = await dbContext.FindAsync<Answer>(existingCorrectAnswer.Id);

            Assert.False(previousCorrectAnswer.IsCorrectAnswer);
        }
    }
}
