﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Models.DTO.Search;
using QuizzHeaven.Services;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Factories;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Seeders;
using QuizzHeavenTests.TestHelpers.MockHelpers;
using QuizzHeavenTests.TestHelpers.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace QuizzHeavenTests.UnitTests.Services
{
    public class QuizzServiceUnitTests
    {
        IQuizzService quizzService;
        MockAppDbContext dbContext;
        MockAppDbContextDataSeeder dbDataSeeder;

        public QuizzServiceUnitTests()
        {
            var dbContextFactory = new MockAppDbContextFactory();
            dbContext = dbContextFactory.Create();
            dbDataSeeder = new MockAppDbContextDataSeeder(dbContext);

            var quizzServiceBuilder = new QuizzServiceMockBuilder();
                quizzServiceBuilder.SetDbContext(dbContext);
                quizzServiceBuilder.SetUserManager(new UserManagerMockBuilder<AppUser>().GetResultMock());

            quizzService = quizzServiceBuilder.Build();
        }

        [Fact]
        public async Task EditPartiallyAsync_ShouldUpdateVersionStamp()
        {
            var initialQuizz = dbDataSeeder.AddQuizzEntity();

            var modifiedQuizz = new EditQuizzInput() { Name = "modifiedName" };
            await quizzService.EditPartiallyAsync(modifiedQuizz, initialQuizz.Id);

            var fetchedQuizz = await dbContext.Set<Quizz>().Where(quizz => quizz.Id == initialQuizz.Id).FirstOrDefaultAsync();
            Assert.NotEqual(Guid.Empty, fetchedQuizz.VersionStamp);
        }

        [Fact]
        public async Task OnQuizzModified_ShouldUpdateVersionStamp()
        {
            var initialQuizz = dbDataSeeder.AddQuizzEntity();

            await quizzService.OnQuizzModifiedAsync(initialQuizz.Id);

            var fetchedQuizz = await dbContext.Set<Quizz>().Where(quizz => quizz.Id == initialQuizz.Id).FirstOrDefaultAsync();
            Assert.NotEqual(Guid.Empty, fetchedQuizz.VersionStamp);
        }
    }
}
