﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Moq;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Models.DTO.Authentication.SignUp;
using QuizzHeaven.Services;
using QuizzHeavenTests.TestHelpers.InputDataGenerators;
using QuizzHeavenTests.TestHelpers.InputGenerators.Authentication;
using QuizzHeavenTests.TestHelpers.MockHelpers;
using QuizzHeavenTests.TestHelpers.Mocks;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace QuizzHeavenTests.UnitTests.Services
{
    public class AuthenticationServiceUnitTests
    {
        UserManagerMockBuilder<AppUser> userManagerMockBuilder;
        SignInManagerMockBuilder<AppUser> signInManagerMockBuilder;
        AspNetIConfigurationMock configurationMock;

        IAuthenticationService authenticationService;

        public AuthenticationServiceUnitTests()
        {
            userManagerMockBuilder = new UserManagerMockBuilder<AppUser>();
            signInManagerMockBuilder = new SignInManagerMockBuilder<AppUser>(userManagerMockBuilder.GetResultMock());
            configurationMock = new AspNetIConfigurationMock();

            authenticationService = new AuthenticationService(userManagerMockBuilder.GetResultMock(), signInManagerMockBuilder.GetResultMock(), configurationMock.Configuration);
        }

        [Theory]
        [MemberData(nameof(SignUpInput_DataGenerator.CorrectCredentials), MemberType = typeof(SignUpInput_DataGenerator))]
        public async Task SignUp_ShouldSucceed_WhenCorrectDataProvided(SignUpInput mockSignUpInput)
        {
            userManagerMockBuilder.SetupMock_CreateAsync_Succeeds();
            userManagerMockBuilder.SetupMock_AddToRoleAsync_Succeeds();

            var exception = await Record.ExceptionAsync(() => authenticationService.SignUpAsync(mockSignUpInput));

            Assert.Null(exception);
        }

        [Fact]
        public async Task SignUp_ShouldFail_WhenFailedToCreateUser()
        {
            var mockSignUpInput = new SignUpInput() { Login = "login", Password = "pass", ConfirmedPassword = "pass", Email = "email@g.com" };
            userManagerMockBuilder.SetupMock_CreateAsync_Fails();

            await Assert.ThrowsAsync<UnprocessableLogicException>(() => authenticationService.SignUpAsync(mockSignUpInput));
        }

        [Theory]
        [MemberData(nameof(SignInInput_DataGenerator.CorrectCredentials), MemberType = typeof(SignInInput_DataGenerator))]
        public async Task SignIn_ShouldSucceed_WhenCredentialsAreCorrect(SignInInput mockSignInInput)
        {
            var returnedUser = new AppUser() { UserName = mockSignInInput.Login };

            userManagerMockBuilder.SetupMock_FindBy_ReturnsUser(returnedUser);
            userManagerMockBuilder.SetupMock_GetRoles_ReturnsRole();
            signInManagerMockBuilder.SetupMock_PasswordSignInAsync_Succeeds();


            var exception = await Record.ExceptionAsync(() => authenticationService.SignInAsync(mockSignInInput));

            Assert.Null(exception);
        }

        [Fact]
        public async Task SignIn_ShouldFail_WhenPasswordSignInFailed()
        {
            var mockSignInInput = new SignInInput() { Login = "login", Password = "pass" };
            var returnedUser = new AppUser() { UserName = "Username" };

            userManagerMockBuilder.SetupMock_FindBy_ReturnsUser(returnedUser);
            signInManagerMockBuilder.SetupMock_PasswordSignInAsync_Fails();


            await Assert.ThrowsAsync<UnauthorizedException>(() => authenticationService.SignInAsync(mockSignInInput));
        }

        [Fact]
        public async Task SignIn_ShouldFail_WhenEmailHasNotBeenFound()
        {
            var mockSignInInput = new SignInInput() { Login = "nonExistingEmail@g.com", Password = "pass" };
            userManagerMockBuilder.SetupMock_FindBy_DoesNotReturnUser();

            await Assert.ThrowsAsync<UnauthorizedException>(() => authenticationService.SignInAsync(mockSignInInput));
        }
    }
}
