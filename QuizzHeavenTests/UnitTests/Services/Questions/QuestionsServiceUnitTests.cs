﻿using AutoMapper;
using Castle.Windsor;
using QuizzHeaven.Data;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Services;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Factories;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Seeders;
using QuizzHeavenTests.TestHelpers.MockHelpers;
using QuizzHeavenTests.TestHelpers.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace QuizzHeavenTests.UnitTests.Services
{
    public class QuestionsServiceUnitTests
    {
        IQuestionsService questionsService;
        MockAppDbContext dbContext;
        MockAppDbContextDataSeeder dbDataSeeder;

        public QuestionsServiceUnitTests()
        {
            var dbContextFactory = new MockAppDbContextFactory();
            dbContext = dbContextFactory.Create();
            dbDataSeeder = new MockAppDbContextDataSeeder(dbContext);

            var questionsServiceBuilder = new QuestionsServiceMockBuilder();
                questionsServiceBuilder.SetDbContext(dbContext);
                questionsServiceBuilder.SetUserManager(new UserManagerMockBuilder<AppUser>().GetResultMock());

            questionsService = questionsServiceBuilder.Build();
        }

        [Fact]
        public async void CreateAsync_ShouldUpdateQuizzVersionStamp()
        {
            var associatedQuizz = dbDataSeeder.AddQuizzEntity();


            var createQuestionInput = new CreateQuestionInput() { Content = "content", DisplayOrder = 1, QuizzId = associatedQuizz.Id };
            await questionsService.CreateAsync(createQuestionInput);

            var updatedVersionStamp = (await dbContext.FindAsync<Quizz>(associatedQuizz.Id)).VersionStamp;


            Assert.NotEqual(Guid.Empty, updatedVersionStamp);
        }

        [Fact]
        public async void EditPartiallyAsync_ShouldUpdateQuizzVersionStamp()
        {
            var someQuestion = dbDataSeeder.AddQuestionEntity();


            var editQuestionInput = new EditQuestionInput() { Id = someQuestion.Id, Content = "differentContent" };
            await questionsService.EditPartiallyAsync(editQuestionInput, editQuestionInput.Id);

            var updatedVersionStamp = (await dbContext.FindAsync<Quizz>(someQuestion.QuizzId)).VersionStamp;


            Assert.NotEqual(Guid.Empty, updatedVersionStamp);
        }

        [Fact]
        public async void DeleteAsync_ShouldUpdateQuizzVersionStamp()
        {
            var someQuestion = dbDataSeeder.AddQuestionEntity();


            await questionsService.DeleteAsync(someQuestion.Id);
            var updatedVersionStamp = (await dbContext.FindAsync<Quizz>(someQuestion.QuizzId)).VersionStamp;


            Assert.NotEqual(Guid.Empty, updatedVersionStamp);
        }
    }
}
