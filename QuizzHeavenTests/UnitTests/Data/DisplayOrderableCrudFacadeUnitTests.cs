﻿using QuizzHeaven.Data;
using QuizzHeaven.Services;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Factories;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Models;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Seeders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace QuizzHeavenTests.UnitTests.Data
{
    public class DisplayOrderableCrudFacadeUnitTests
    {
        MockAppDbContext dbContext;
        MockAppDbContextDataSeeder dbContextSeeder;

        IDisplayOrderedCrudFacade<DisplayOrderableModel> displayCrudFacade;


        public DisplayOrderableCrudFacadeUnitTests()
        {
            dbContext = new MockAppDbContextFactory().Create();
            dbContextSeeder = new MockAppDbContextDataSeeder(dbContext);

            displayCrudFacade = new DisplayOrderedCrudFacade<DisplayOrderableModel>(dbContext);
        }

        [Theory]
        [InlineData(16, 1)]
        [InlineData(16, 8)]
        [InlineData(16, 17)]
        public async Task AddAsync_ShouldUpdateDisplayOrder(int existingEntitiesAmount, short newEntityDisplayOrder)
        {
            dbContextSeeder.AddDisplayOrderedModels(existingEntitiesAmount);


            var newEntityId = existingEntitiesAmount + 1; 
            var newEntity = new DisplayOrderableModel(newEntityId, newEntityDisplayOrder);
            await displayCrudFacade.AddAsync(newEntity, entity => entity.Id != 0, newEntityId);


            var allEntities = await displayCrudFacade.GetAllAsync(1, existingEntitiesAmount + 1);
            Assert.True(verifyDisplayOrderContinuity(allEntities));
        }

        [Theory]
        [InlineData(16, 1, 1)]
        [InlineData(16, 1, 8)]
        [InlineData(16, 17, 8)]
        [InlineData(16, 17, 17)]
        public async Task UpsertAsync_ShouldUpdateDisplayOrder(int existingEntitiesAmount, int entityId, short entityDisplayOrder)
        {
            dbContextSeeder.AddDisplayOrderedModels(existingEntitiesAmount);


            var newEntity = new DisplayOrderableModel(entityId, entityDisplayOrder);
            await displayCrudFacade.UpsertAsync(newEntity, entity => entity.Id != 0, entityId);


            var allEntities = await displayCrudFacade.GetAllAsync(1, existingEntitiesAmount + 1);
            Assert.True(verifyDisplayOrderContinuity(allEntities));
        }

        [Theory]
        [InlineData(16, 4, 4)]
        [InlineData(16, 4, 8)]
        [InlineData(16, 8, 4)]
        public async Task EditAsync_ShouldUpdateDisplayOrder(int existingEntitiesAmount, int editedEntityId, short editedEntityDisplayOrder)
        {
            dbContextSeeder.AddDisplayOrderedModels(existingEntitiesAmount);

            var editedEntity = new DisplayOrderableModel(editedEntityId, editedEntityDisplayOrder);
            await displayCrudFacade.EditPartiallyAsync(editedEntity, entity => entity.Id != 0, editedEntityId);

            var allEntities = await displayCrudFacade.GetAllAsync(1, existingEntitiesAmount);
            Assert.True(verifyDisplayOrderContinuity(allEntities));
        }

        [Theory]
        [InlineData(16, 1)]
        [InlineData(16, 8)]
        [InlineData(16, 16)]
        public async Task DeleteAsync_ShouldUpdateDisplayOrder(int existingEntitiesAmount, int entityId)
        {
            dbContextSeeder.AddDisplayOrderedModels(existingEntitiesAmount);

            await displayCrudFacade.DeleteAsync(entity => entity.Id != 0, entityId);

            var allEntities = await displayCrudFacade.GetAllAsync(1, existingEntitiesAmount);
            Assert.True(verifyDisplayOrderContinuity(allEntities));
        }

        private bool verifyDisplayOrderContinuity(IEnumerable<DisplayOrderableModel> entities)
        {
            var orderedEntities = entities.OrderBy(entity => entity.DisplayOrder);

            var isContinuous = true;
                isContinuous = orderedEntities.First().DisplayOrder == 1;
                isContinuous = orderedEntities.Last().DisplayOrder == orderedEntities.Count();
                isContinuous = orderedEntities.Distinct().Count() == orderedEntities.Count();

            return isContinuous;
        }
    }
}
