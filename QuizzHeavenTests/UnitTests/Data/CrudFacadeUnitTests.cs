﻿using Microsoft.EntityFrameworkCore;
using Moq;
using QuizzHeaven.Data;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Services;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Mocks;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Seeders;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Models;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Helpers.ExtensionMethods;
using Microsoft.EntityFrameworkCore.Diagnostics;
using QuizzHeaven.Models.DTO.Search;
using QuizzHeavenTests.TestHelpers.DbContextHelpers.Factories;

namespace QuizzHeavenTests.UnitTests.Data
{
    public class CrudFacadeUnitTests
    {
        MockAppDbContext dbContext;
        MockAppDbContextDataSeeder dbContextSeeder;

        ICrudFacade<StringIdModel> stringCrudFacade;
        ICrudFacade<NumericIdModel> numericCrudFacade;

        public CrudFacadeUnitTests()
        {
            dbContext = new MockAppDbContextFactory().Create();
            dbContextSeeder = new MockAppDbContextDataSeeder(dbContext);

            stringCrudFacade = new CrudFacade<StringIdModel>(dbContext);
            numericCrudFacade = new CrudFacade<NumericIdModel>(dbContext);
        }

        [Fact]
        public async Task AddAsync_ShouldBeAdded_WhenEntityAndKeyProvided()
        {
            var entityMock = dbContextSeeder.CreateStringIdModel();

            await stringCrudFacade.AddAsync(entityMock, entityMock.Id);
            var countAfterAdding = await dbContext.StringIdModels.CountAsync();

            Assert.Equal(1, countAfterAdding);
        }

        [Fact]
        public async Task AddAsync_ShouldThrow_WhenEntityIsNull()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => stringCrudFacade.AddAsync(null, ""));
        }

        [Fact]
        public async Task AddAsync_ShouldThrow_WhenEntityExists()
        {
            var entityMock = dbContextSeeder.CreateStringIdModel();       
            await stringCrudFacade.AddAsync(entityMock, entityMock.Id);

            await Assert.ThrowsAsync<ConflictException>(() => stringCrudFacade.AddAsync(entityMock, entityMock.Id));
        }


        [Fact]
        public async Task GetAllAsync_ShouldReturnAll_WhenEntitiesExist()
        {
            int existingEntitiesAmount = 5;
            dbContextSeeder.AddRandomStringIdModels(existingEntitiesAmount);

            var returnedEntitiesList = await stringCrudFacade.GetAllAsync(1, 100);
            var returnedAmount = returnedEntitiesList.Count();

            Assert.Equal(existingEntitiesAmount, returnedAmount);
        }

        [Fact]
        public async Task GetAllAsync_ShouldReturnEmpty_WhenEntitiesDoNotExist()
        {
            var returnedEntitiesList = await stringCrudFacade.GetAllAsync(1, 100);
            var returnedAmount = returnedEntitiesList.Count();

            Assert.NotNull(returnedEntitiesList);
            Assert.Equal(0, returnedAmount);
        }


        [Fact]
        public async Task GetByKeyAsync_ShouldReturnSingle_WhenKeyExists()
        {
            var entityKey = "test";
            dbContextSeeder.AddStringIdModel(entityKey);
            dbContextSeeder.AddRandomStringIdModels(2);

            var existingEntity = await stringCrudFacade.GetByKeyAsync(entityKey);

            Assert.Equal(existingEntity.Id, entityKey);
        }

        [Fact]
        public async Task GetByKeyAsync_ShouldThrowNotFound_WhenKeyDoesNotExist()
        {
            dbContextSeeder.AddRandomStringIdModels(2);

            var nonExistingKey = "test2";
            await Assert.ThrowsAsync<NotFoundException>(() => stringCrudFacade.GetByKeyAsync(nonExistingKey));
        }


        [Fact]
        public async Task EditPartiallyAsync_ShouldModifyEntity_WhenExistingKeyProvided()
        {
            var existingEntity = dbContextSeeder.AddStringIdModel("test").Entity;
            var modifiedEntity = dbContextSeeder.CreateStringIdModel(existingEntity.Id, "modified");

            await stringCrudFacade.EditPartiallyAsync(modifiedEntity, modifiedEntity.Id);
            var fetchedEntity = await stringCrudFacade.GetByKeyAsync(modifiedEntity.Id);

            Assert.Equal(modifiedEntity.ParentId, fetchedEntity.ParentId);
        }

        [Fact]
        public async Task EditPartiallyAsync_ShouldNullifyForeignKey_WhenEmptyStringPassed()
        {
            var existingEntity = dbContextSeeder.AddStringIdModel("test", "testForeignKey").Entity;
            var modifiedEntity = dbContextSeeder.CreateStringIdModel(existingEntity.Id, "");

            await stringCrudFacade.EditPartiallyAsync(modifiedEntity, modifiedEntity.Id);
            var fetchedEntity = await stringCrudFacade.GetByKeyAsync(modifiedEntity.Id);

            Assert.Null(fetchedEntity.ParentId);
        }
        
        [Fact]
        public async Task EditPartiallyAsync_ShouldNullifyForeignKey_WhenEmptyNullableIntPassed()
        {
            var parentEntity = dbContextSeeder.AddNumericIdModel(10, null);
            var childEntity = dbContextSeeder.AddNumericIdModel(1, parentEntity.Entity.Id);

            var nullifyParentInput = dbContextSeeder.CreateNumericIdModel(childEntity.Entity.Id, 0);
            await numericCrudFacade.EditPartiallyAsync(nullifyParentInput, nullifyParentInput.Id);
            var fetchedEntity = await numericCrudFacade.GetByKeyAsync(nullifyParentInput.Id);

            Assert.Null(fetchedEntity.ParentId);
        }
        
        [Fact]
        public async Task EditPartiallyAsync_ShouldThrowNotFound_WhenKeyDoesNotExist()
        {
            dbContextSeeder.AddStringIdModel("test");
            var nonExistingEntity = dbContextSeeder.CreateStringIdModel("test2", null);

            await Assert.ThrowsAsync<NotFoundException>(() => stringCrudFacade.EditPartiallyAsync(nonExistingEntity, nonExistingEntity.Id));
        }


        [Fact]
        public async Task DeleteAsync_ShouldDeleteSingle_WhenExistingKeyProvided()
        {
            var entityEntry = dbContextSeeder.AddStringIdModel();

            var countBeforeDelete = await dbContext.StringIdModels.CountAsync();
            await stringCrudFacade.DeleteAsync(entityEntry.Entity.Id);
            var countAfterDelete = await dbContext.StringIdModels.CountAsync();

            Assert.Equal(1, countBeforeDelete);
            Assert.Equal(0, countAfterDelete);
        }

        [Fact]
        public async Task DeleteAsync_ShouldThrow_WhenEntityDoesNotExists()
        {
            var nonExistingEntity = dbContextSeeder.CreateStringIdModel();
            await Assert.ThrowsAsync<NotFoundException>(() => stringCrudFacade.DeleteAsync(nonExistingEntity.Id));
        }
    }
}
