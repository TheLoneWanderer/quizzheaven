﻿using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Services.OwnershipValidators
{
    public class QuestionOwnershipValidator : IQuestionOwnershipValidator
    {
        DbContext dbContext;

        public QuestionOwnershipValidator(AppDbContext appDbContext)
        {
            dbContext = appDbContext;
        }

        public async Task<bool> IsOwner(string userId, string entityId)
        {
            var questionIdInputIsNumeric = int.TryParse(entityId, out var questionId);
            if(!questionIdInputIsNumeric)
            {
                throw new BadRequestException("Question ID must be a numeric value");
            }

            var quizzAuthorId = await (from questions in dbContext.Set<Question>()
                                       join quizzes in dbContext.Set<Quizz>() on questions.QuizzId equals quizzes.Id
                                       where questions.Id == questionId
                                       select quizzes.AuthorId)
                                       .FirstOrDefaultAsync();

            if(quizzAuthorId == null)
            {
                throw new NotFoundException("Question does not exist");
            }

            return quizzAuthorId == userId;
        }
    }
}
