﻿using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Services.OwnershipValidators
{
    public class QuizzOwnershipValidator : IQuizzOwnershipValidator
    {
        DbContext dbContext;

        public QuizzOwnershipValidator(AppDbContext appDbContext)
        {
            dbContext = appDbContext;
        }

        public async Task<bool> IsOwner(string userId, string entityId)
        {
            var quizzIdInputIsNumeric = int.TryParse(entityId, out var accessedQuizzId);
            if(!quizzIdInputIsNumeric)
            {
                throw new BadRequestException("Quizz ID must be a numeric value");
            }

            var quizzAuthorId = await dbContext.Set<Quizz>().Where(quizz => quizz.Id == accessedQuizzId).Select(quizz => quizz.AuthorId).FirstOrDefaultAsync();
            if(quizzAuthorId == null)
            {
                throw new NotFoundException("Quizz does not exist");
            }

            return quizzAuthorId == userId;
        }
    }
}
