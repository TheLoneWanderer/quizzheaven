﻿using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Services.OwnershipValidators
{
    public class AnswerOwnershipValidator : IAnswerOwnershipValidator
    {
        DbContext dbContext;

        public AnswerOwnershipValidator(AppDbContext appDbContext)
        {
            dbContext = appDbContext;
        }

        public async Task<bool> IsOwner(string userId, string entityId)
        {
            var answerIdInputIsNumeric = int.TryParse(entityId, out var answerId);
            if(!answerIdInputIsNumeric)
            {
                throw new BadRequestException("Answer ID must be a numeric value");
            }

            var quizzAuthorId = await (from answers in dbContext.Set<Answer>()
                                       join questions in dbContext.Set<Question>() on answers.QuestionId equals questions.Id
                                       join quizzes in dbContext.Set<Quizz>() on questions.QuizzId equals quizzes.Id
                                       where answers.Id == answerId
                                       select quizzes.AuthorId)
                                       .FirstOrDefaultAsync();

            if (quizzAuthorId == null)
            {
                throw new NotFoundException("Answer does not exist");
            }

            return quizzAuthorId == userId;
        }
    }
}
