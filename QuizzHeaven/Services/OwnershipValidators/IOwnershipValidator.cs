﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Services
{
    public interface IOwnershipValidator
    {
        Task<bool> IsOwner(string userId, string entityId);
    }

    public interface IQuizzOwnershipValidator : IOwnershipValidator
    {
    }

    public interface IQuestionOwnershipValidator : IOwnershipValidator
    {
    }
    
    public interface IAnswerOwnershipValidator : IOwnershipValidator
    {
    }
}
