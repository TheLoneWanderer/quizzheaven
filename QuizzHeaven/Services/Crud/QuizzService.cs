﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Data.Consts.SQL;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Helpers.ExtensionMethods.IQueryable;
using QuizzHeaven.Helpers.ExtensionMethods.String;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Models.DTO.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Transactions;

namespace QuizzHeaven.Services
{
    public class QuizzService : IQuizzService
    {
        ICrudFacade<Quizz> crudFacade;
        IMapper mapper;
        UserManager<AppUser> userManager;

        public QuizzService(ICrudFacade<Quizz> crudFacade, IMapper mapper, UserManager<AppUser> userManager)
        {
            this.crudFacade = crudFacade;
            this.mapper = mapper;
            this.userManager = userManager;
        }

        public async Task<IEnumerable<QuizzVm>> GetByCategoryAsync(int categoryId, SearchParameters searchParams)
        {
            List<Quizz> quizzesDomain;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.RepeatableRead }, TransactionScopeAsyncFlowOption.Enabled))
            {
                await crudFacade.Context.EnsureEntityExists<Category, int>(cat => cat.Id, cat => cat.Id == categoryId);
                quizzesDomain = await crudFacade.Context.Set<Quizz>().FromSqlInterpolated($"{StoredProceduresConsts.GetQuizzesByCategoryStoredProcName} {categoryId}, {searchParams.SearchKey}, {searchParams.PageNumber}, {searchParams.PageSize}").ToListAsync();

                transaction.Complete();
            }
                
            var quizzesVm = mapper.Map<List<Quizz>, List<QuizzVm>>(quizzesDomain);
            return quizzesVm;
        }

        public async Task<IEnumerable<QuizzVm>> GetByUserAsync(string username)
        {
            var quizzesAuthor = await userManager.FindByNameAsync(username);
            if(quizzesAuthor == null)
            {
                throw new NotFoundException("Specified user has not been found");
            }

            var quizzesDomain = await crudFacade.Context.Set<Quizz>().Where(quizz => quizz.AuthorId == quizzesAuthor.Id).ToListAsync();
            var quizzesVm = mapper.Map<List<Quizz>, List<QuizzVm>>(quizzesDomain);
            return quizzesVm;
        }

        public async Task<QuizzVm> CreateAsync(CreateQuizzInput newEntity)
        {
            var quizzDomain = mapper.Map<CreateQuizzInput, Quizz>(newEntity);
                quizzDomain = await crudFacade.AddAsync(quizzDomain, 0);

            var quizzVm = mapper.Map<Quizz, QuizzVm>(quizzDomain);
            return quizzVm;
        }

        public async Task<IEnumerable<QuizzVm>> GetAllAsync(SearchParameters searchParams)
        {
            var getAllQuery = crudFacade.Context.Set<Quizz>().AsQueryable();

            if (!string.IsNullOrWhiteSpace(searchParams.SearchKey))
            {
                getAllQuery = getAllQuery.Where(quizz => EF.Functions.FreeText(quizz.Name, searchParams.SearchKey));
            }

            getAllQuery = getAllQuery.Page(searchParams.PageNumber, searchParams.PageSize);
            getAllQuery = sortByParameter(getAllQuery, searchParams.SortBy, searchParams.IsDescending);
               
            var quizzesDomain = await getAllQuery.ToListAsync();
            var quizzesVm = mapper.Map<List<Quizz>, List<QuizzVm>>(quizzesDomain);

            return quizzesVm;
        }

        public async Task<QuizzVm> GetByKeyAsync(params object[] entityKeys)
        {
            var quizzDomain = await crudFacade.GetByKeyAsync(entityKeys);
            var quizzVm = mapper.Map<Quizz, QuizzVm>(quizzDomain);

            return quizzVm;
        }

        public async Task EditPartiallyAsync(EditQuizzInput inputEntity, params object[] entityKeys)
        {
            var quizzDomain = mapper.Map<EditQuizzInput, Quizz>(inputEntity);
                quizzDomain.OnModified();

            await crudFacade.EditPartiallyAsync(quizzDomain, entityKeys);
        }

        public async Task DeleteAsync(params object[] entityKeys)
        {
            await crudFacade.DeleteAsync(entityKeys);
        }

        public async Task OnQuizzModifiedAsync(int quizzId)
        {
            var modifiedVersionQuizz = new Quizz() { Id = quizzId };
                modifiedVersionQuizz.OnModified();

            crudFacade.Context.Entry(modifiedVersionQuizz).Property(prop => prop.VersionStamp).IsModified = true;
            await crudFacade.Context.SaveChangesAsync();
        }

        private IQueryable<Quizz> sortByParameter(IQueryable<Quizz> query, string sortPropertyKey, bool isDescending)
        {
            var sortingProperties = new Dictionary<string, Expression<Func<Quizz, object>>>()
            {
                [nameof(QuizzVm.Id).ToUpperInvariant()] = (quizz) => quizz.Id,
                [nameof(QuizzVm.Name).ToUpperInvariant()] = (quizz) => quizz.Name,
                [nameof(QuizzVm.AuthorUsername).ToUpperInvariant()] = (quizz) => quizz.Author.UserName
            };

            sortPropertyKey = sortPropertyKey.ToUpperInvariantOrEmpty();
            var chosenProperty = sortingProperties.GetValueOrDefault(sortPropertyKey, (quizz) => quizz.Id);

            return query.OrderByDirectionally(chosenProperty, isDescending);
        }
    }
}
