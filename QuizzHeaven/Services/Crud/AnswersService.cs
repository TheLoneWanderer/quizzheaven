﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Models.DTO.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Z.EntityFramework.Plus;

namespace QuizzHeaven.Services
{
    public class AnswersService : IAnswersService
    {
        IDisplayOrderedCrudFacade<Answer> crudFacade;
        IMapper mapper;
        IQuizzService quizzService;

        public AnswersService(IDisplayOrderedCrudFacade<Answer> crudFacade, IMapper mapper, IQuizzService quizzService)
        {
            this.crudFacade = crudFacade;
            this.mapper = mapper;
            this.quizzService = quizzService;
        }

        public async Task<AnswerVm> CreateAsync(CreateAnswerInput newAnswerInput)
        {
            var newAnswer = mapper.Map<CreateAnswerInput, Answer>(newAnswerInput);
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.Serializable }, TransactionScopeAsyncFlowOption.Enabled))
            {
                if(newAnswer.IsCorrectAnswer)
                {
                    await invalidateExistingCorrectAnswerAsync(newAnswer.QuestionId);
                }

                newAnswer = await crudFacade.AddAsync(newAnswer, answer => answer.QuestionId == newAnswer.QuestionId, 0);

                var questionAndQuizzId = await getQuestionAndQuizzIdsAsync(newAnswer.Id);
                await quizzService.OnQuizzModifiedAsync(questionAndQuizzId.Item2);

                transaction.Complete();
            }

            var answerVm = mapper.Map<Answer, AnswerVm>(newAnswer);
            return answerVm;
        }

        public async Task<IEnumerable<AnswerVm>> GetByQuestionAsync(int questionId)
        {
            List<Answer> answersDomain;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.RepeatableRead }, TransactionScopeAsyncFlowOption.Enabled))
            {
                await crudFacade.Context.EnsureEntityExists<Question, int>(question => question.Id, question => question.Id == questionId);

                answersDomain = await crudFacade.Context.Set<Answer>()
                                                 .Where(answer => answer.QuestionId == questionId)
                                                 .OrderBy(answer => answer.DisplayOrder)
                                                 .ToListAsync();

                transaction.Complete();
            }

            var answersVm = mapper.Map<List<Answer>, List<AnswerVm>>(answersDomain);
            return answersVm;
        }

        public async Task<IEnumerable<AnswerVm>> GetAllAsync(SearchParameters searchParams)
        {
            throw new NotImplementedException();
        }

        public async Task<AnswerVm> GetByKeyAsync(params object[] entityKeys)
        {
            var answer = await crudFacade.GetByKeyAsync(entityKeys);
            var answerVm = mapper.Map<Answer, AnswerVm>(answer);

            return answerVm;
        }

        public async Task EditPartiallyAsync(EditAnswerInput updatedAnswerInput, params object[] entityKeys)
        {
            var updatedAnswer = mapper.Map<EditAnswerInput, Answer>(updatedAnswerInput);
            using(var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.RepeatableRead }, TransactionScopeAsyncFlowOption.Enabled))
            {
                var questionAndQuizzId = await getQuestionAndQuizzIdsAsync((int)entityKeys[0]);

                if (updatedAnswer.IsCorrectAnswer)
                {
                    await invalidateExistingCorrectAnswerAsync(questionAndQuizzId.Item1);
                }

                await crudFacade.EditPartiallyAsync(updatedAnswer, answer => answer.QuestionId == questionAndQuizzId.Item1, entityKeys);
                await quizzService.OnQuizzModifiedAsync(questionAndQuizzId.Item2);

                transaction.Complete();
            }
        }

        public async Task DeleteAsync(params object[] entityKeys)
        {
            using(var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.RepeatableRead }, TransactionScopeAsyncFlowOption.Enabled))
            {
                var questionAndQuizzId = await getQuestionAndQuizzIdsAsync((int)entityKeys[0]);

                await crudFacade.DeleteAsync(answer => answer.QuestionId == questionAndQuizzId.Item1, entityKeys);
                await quizzService.OnQuizzModifiedAsync(questionAndQuizzId.Item2);

                transaction.Complete();
            }
        }

        private async Task invalidateExistingCorrectAnswerAsync(int questionId)
        {
            crudFacade.Context.Set<Answer>()
                              .Where(answer => answer.QuestionId == questionId)
                              .Update(answer => new Answer() { IsCorrectAnswer = false });

            await crudFacade.Context.SaveChangesAsync();
        }

        /// <summary>
        /// Question ID is the first item and Quizz ID is the second
        /// </summary>
        /// <param name="answerId"></param>
        /// <returns></returns>
        private async Task<Tuple<int, int>> getQuestionAndQuizzIdsAsync(int answerId)
        {
            var idsPair = await (from answers in crudFacade.Context.Set<Answer>()
                                 join questions in crudFacade.Context.Set<Question>() on answers.QuestionId equals questions.Id
                                 join quizzes in crudFacade.Context.Set<Quizz>() on questions.QuizzId equals quizzes.Id
                                 where answers.Id == answerId
                                 select new Tuple<int, int>(questions.Id, quizzes.Id))
                                .FirstOrDefaultAsync();

            return idsPair;
        }
    }
}
