﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Models.DTO.Search;
using QuizzHeaven.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace QuizzHeaven.Services
{
    public class QuestionsService : IQuestionsService
    {
        IDisplayOrderedCrudFacade<Question> crudFacade;
        IMapper mapper;
        IQuizzService quizzService;

        public QuestionsService(IDisplayOrderedCrudFacade<Question> crudFacade, IMapper mapper, IQuizzService quizzService)
        {
            this.crudFacade = crudFacade;
            this.mapper = mapper;
            this.quizzService = quizzService;
        }

        public async Task<QuestionVm> CreateAsync(CreateQuestionInput newQuestionInput)
        {
            var newQuestion = mapper.Map<CreateQuestionInput, Question>(newQuestionInput);
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.Serializable }, TransactionScopeAsyncFlowOption.Enabled))
            {
                newQuestion = await crudFacade.AddAsync(newQuestion, question => question.QuizzId == newQuestionInput.QuizzId, 0);
                await quizzService.OnQuizzModifiedAsync(newQuestion.QuizzId);

                transaction.Complete();
            }

            var questionVm = mapper.Map<Question, QuestionVm>(newQuestion);
            return questionVm;
        }

        public async Task<IEnumerable<QuestionVm>> GetByQuizzAsync(int quizzId)
        {
            List<Question> questionsDomain;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.RepeatableRead }, TransactionScopeAsyncFlowOption.Enabled))
            {
                await crudFacade.Context.EnsureEntityExists<Quizz, int>(quizz => quizz.Id, quizz => quizz.Id == quizzId);
                questionsDomain = await crudFacade.Context.Set<Question>()
                                                       .Where(question => question.QuizzId == quizzId)
                                                       .OrderBy(question => question.DisplayOrder)
                                                       .ToListAsync();

                transaction.Complete();
            }
            
            var questionsVm = mapper.Map<List<Question>, List<QuestionVm>>(questionsDomain);
            return questionsVm;
        }

        public Task<IEnumerable<QuestionVm>> GetAllAsync(SearchParameters searchParams)
        {
            throw new NotImplementedException();
        }

        public async Task<QuestionVm> GetByKeyAsync(params object[] entityKeys)
        {
            var question = await crudFacade.GetByKeyAsync(entityKeys);
            var questionVm = mapper.Map<Question, QuestionVm>(question);

            return questionVm;
        }

        public async Task EditPartiallyAsync(EditQuestionInput updatedQuestionInput, params object[] entityKeys)
        {
            var updatedQuestion = mapper.Map<EditQuestionInput, Question>(updatedQuestionInput);

            using(var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.RepeatableRead }, TransactionScopeAsyncFlowOption.Enabled))
            {
                var associatedQuizzId = (await crudFacade.GetByKeyAsync(entityKeys)).QuizzId;

                await crudFacade.EditPartiallyAsync(updatedQuestion, question => question.QuizzId == associatedQuizzId, entityKeys);
                await quizzService.OnQuizzModifiedAsync(associatedQuizzId);

                transaction.Complete();
            }
        }

        public async Task DeleteAsync(params object[] entityKeys)
        {
            using(var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.RepeatableRead }, TransactionScopeAsyncFlowOption.Enabled))
            {
                var associatedQuizzId = (await crudFacade.GetByKeyAsync(entityKeys)).QuizzId;

                await crudFacade.DeleteAsync(question => question.QuizzId == associatedQuizzId, entityKeys);
                await quizzService.OnQuizzModifiedAsync(associatedQuizzId);

                transaction.Complete();
            }
        }
    }
}
