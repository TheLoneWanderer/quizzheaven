﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Helpers.ExtensionMethods.IQueryable;
using QuizzHeaven.Helpers.ExtensionMethods.Object;
using QuizzHeaven.Helpers.ExtensionMethods.String;
using QuizzHeaven.Helpers.Mappers;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Models.DTO.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace QuizzHeaven.Services
{
    public class CategoriesService : ICategoriesService
    {
        ICrudFacade<Category> crudFacade;
        IMapper mapper;

        public CategoriesService(ICrudFacade<Category> crudFacade, IMapper mapper)
        {
            this.crudFacade = crudFacade;
            this.mapper = mapper;
        }

        public async Task<CategoryVm> CreateAsync(CreateCategoryInput newEntity)
        {
            var categoryDomain = mapper.Map<CreateCategoryInput, Category>(newEntity);
                categoryDomain = await crudFacade.AddAsync(categoryDomain, categoryDomain.Id);

            var categoryVm = mapper.Map<Category, CategoryVm>(categoryDomain);
            return categoryVm;
        }

        public async Task<IEnumerable<CategoryVm>> GetAllAsync(SearchParameters searchParams)
        {
            var getAllQuery = crudFacade.Context.Set<Category>().AsQueryable();
                getAllQuery = getAllQuery.Page(searchParams.PageNumber, searchParams.PageSize);
                getAllQuery = SortByParameter(getAllQuery, searchParams.SortBy, searchParams.IsDescending);

            var categoriesDomain = await getAllQuery.AsNoTracking().ToListAsync();

            var parentChildMapper = new ParentChildHierarchyMapper(mapper);
            var categoriesVm = parentChildMapper.UnflattenList<Category, CategoryVm, int?>(categoriesDomain, cat => cat.Id, cat => cat.ParentId, catVm => catVm.Id, catVm => catVm.Subcategories);

            return categoriesVm;
        }

        public async Task<CategoryVm> GetByKeyAsync(params object[] entityKeys)
        {
            var categoryDomain = await crudFacade.GetByKeyAsync(entityKeys);
            var categoryVm = mapper.Map<Category, CategoryVm>(categoryDomain);
            
            return categoryVm;
        }

        public async Task EditPartiallyAsync(EditCategoryInput updatedEntity, params object[] entityKeys)
        {
            var domainEntity = mapper.Map<EditCategoryInput, Category>(updatedEntity);
            await crudFacade.EditPartiallyAsync(domainEntity, entityKeys);
        }

        public async Task DeleteAsync(params object[] entityKeys)
        {
            await crudFacade.DeleteAsync(entityKeys);
        }

        IQueryable<Category> SortByParameter(IQueryable<Category> query, string sortPropertyKey, bool isDescending)
        {
            var sortingProperties = new Dictionary<string, Expression<Func<Category, object>>>()
            {
                [nameof(CategoryVm.Name).ToUpperInvariant()] = (quizz) => quizz.Name
            };

            sortPropertyKey = sortPropertyKey.ToUpperInvariantOrEmpty();
            var chosenProperty = sortingProperties.GetValueOrDefault(sortPropertyKey, (quizz) => quizz.Name);

            return query.OrderByDirectionally(chosenProperty, isDescending);
        }
    }
}
