﻿using AutoMapper;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Helpers.ExtensionMethods.IQueryable;
using QuizzHeaven.Helpers.ExtensionMethods.String;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Models.DTO.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Transactions;

namespace QuizzHeaven.Services
{
    public class SolvedQuizzStatsService : ISolvedQuizzStatsService
    {
        AppDbContext dbContext;
        IMapper mapper;

        public SolvedQuizzStatsService(AppDbContext dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        public async Task BeginSolvingAttemptAsync(int quizzId, string solvingUserId)
        {
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.RepeatableRead }, TransactionScopeAsyncFlowOption.Enabled))
            {
                await dbContext.EnsureEntityExists<AppUser, string>(user => user.Id, user => user.Id == solvingUserId);
                await dbContext.EnsureEntityExists<Quizz, int>(quizz => quizz.Id, quizz => quizz.Id == quizzId);

                await dbContext.SolvedQuizzesStats.Where(stats => stats.UserId == solvingUserId && stats.QuizzId == quizzId && stats.SolvedDate == null).DeleteFromQueryAsync();

                var quizzNameAndVersion = await dbContext.Quizzes.Where(quizz => quizz.Id == quizzId).Select(quizz => new { Name = quizz.Name, VersionStamp = quizz.VersionStamp }).FirstOrDefaultAsync();
                var newUnsolvedStats = new SolvedQuizzStats()
                {
                    BeginningDate = DateTime.UtcNow,

                    QuizzNameSnapshot = quizzNameAndVersion.Name,
                    QuizzVersionSnapshot = quizzNameAndVersion.VersionStamp,

                    UserId = solvingUserId,
                    QuizzId = quizzId
                };

                await dbContext.SolvedQuizzesStats.AddAsync(newUnsolvedStats);
                await dbContext.SaveChangesAsync();

                transaction.Complete();
            }
        }

        public async Task<SolvedQuizzStatsVm> CompleteSolvingAttemptAsync(int quizzId, string solvingUserId, float percentageScore)
        {
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.RepeatableRead }, TransactionScopeAsyncFlowOption.Enabled))
            {
                await dbContext.EnsureEntityExists<AppUser, string>(user => user.Id, user => user.Id == solvingUserId);
                await dbContext.EnsureEntityExists<Quizz, int>(quizz => quizz.Id, quizz => quizz.Id == quizzId);

                var existingStats = await dbContext.SolvedQuizzesStats.Where(stats => stats.UserId == solvingUserId && stats.QuizzId == quizzId && stats.SolvedDate == null).FirstOrDefaultAsync();
                if (existingStats == null)
                {
                    throw new ConflictException("Existing solve attempt not found. Make sure you had started the desired quizz first.");
                }

                existingStats.BeginningDate = DateTime.SpecifyKind(existingStats.BeginningDate, DateTimeKind.Utc);
                existingStats.SolvedDate = DateTime.UtcNow;
                existingStats.PercentageScore = percentageScore;
                await dbContext.SaveChangesAsync();

                transaction.Complete();

                var solvedStatsVm = mapper.Map<SolvedQuizzStats, SolvedQuizzStatsVm>(existingStats);
                return solvedStatsVm;
            }
        }

        public async Task<IEnumerable<SolvedStatsByQuizzVm>> GetByQuizzAsync(int quizzId, SearchParameters searchParams)
        {
            var statsByQuizzQuery = dbContext.Set<SolvedQuizzStats>()
                                             .Where(stats => stats.QuizzId == quizzId && stats.SolvedDate != null)
                                             .Select(stats => new SolvedStatsByQuizzVm
                                             {
                                                 SolvedDate = stats.SolvedDate.Value,
                                                 PercentageScore = stats.PercentageScore,
                                                 Username = dbContext.Set<AppUser>().Where(user => user.Id == stats.UserId).Select(user => user.UserName).FirstOrDefault()
                                             });

            var sortingProperties = new Dictionary<string, Expression<Func<SolvedStatsByQuizzVm, object>>>()
            {
                [nameof(SolvedStatsByQuizzVm.SolvedDate).ToUpperInvariant()] = (statsVm) => statsVm.SolvedDate,
                [nameof(SolvedStatsByQuizzVm.PercentageScore).ToUpperInvariant()] = (statsVm) => statsVm.PercentageScore
            };

            statsByQuizzQuery = sortByParameter(statsByQuizzQuery, searchParams.SortBy, searchParams.IsDescending, sortingProperties, stats => stats.SolvedDate);
            statsByQuizzQuery = statsByQuizzQuery.Page(searchParams.PageNumber, searchParams.PageSize);

            var statsByQuizz = await statsByQuizzQuery.ToListAsync();
            foreach(var stats in statsByQuizz)
            {
                stats.SolvedDate = DateTime.SpecifyKind(stats.SolvedDate, DateTimeKind.Utc);
            }

            return statsByQuizz;
        }

        public async Task<IEnumerable<SolvedStatsByUserVm>> GetByUserAsync(string userId, SearchParameters searchParams)
        {
            var statsByUserQuery = dbContext.Set<SolvedQuizzStats>()
                                            .Where(stats => stats.UserId == userId && stats.SolvedDate != null)
                                            .Select(stats => new SolvedStatsByUserVm
                                            {
                                                SolvedDate = stats.SolvedDate.Value,
                                                PercentageScore = stats.PercentageScore,
                                                QuizzId = stats.QuizzId,
                                                CurrentQuizzName = dbContext.Set<Quizz>().Where(quizz => quizz.Id == stats.QuizzId).Select(quizz => quizz.Name).FirstOrDefault(),
                                                QuizzNameSnapshot = stats.QuizzNameSnapshot
                                            });

            statsByUserQuery = statsByUserQuery.OrderByDirectionally(stats => stats.SolvedDate, searchParams.IsDescending);
            statsByUserQuery = statsByUserQuery.Page(searchParams.PageNumber, searchParams.PageSize);

            var statsByUser = await statsByUserQuery.ToListAsync();
            foreach (var stats in statsByUser)
            {
                stats.SolvedDate = DateTime.SpecifyKind(stats.SolvedDate, DateTimeKind.Utc);
            }

            return statsByUser;
        }

        public async Task<IEnumerable<StatsForUserByQuizzVm>> GetForUserByQuizzAsync(string userId, int quizzId, SearchParameters searchParams)
        {
            var statsQuery = dbContext.Set<SolvedQuizzStats>()
                                      .Where(stats => stats.UserId == userId && stats.QuizzId == quizzId && stats.SolvedDate != null)
                                      .Select(stats => new StatsForUserByQuizzVm
                                      {
                                          BeginningDate = stats.BeginningDate,
                                          SolvedDate = stats.SolvedDate.Value,
                                          PercentageScore = stats.PercentageScore,
                                          QuizzNameSnapshot = stats.QuizzNameSnapshot,
                                          QuizzVersionSnapshot = stats.QuizzVersionSnapshot
                                      });

            var sortingProperties = new Dictionary<string, Expression<Func<StatsForUserByQuizzVm, object>>>()
            {
                [nameof(StatsForUserByQuizzVm.SolvedDate).ToUpperInvariant()] = (statsVm) => statsVm.SolvedDate,
                [nameof(StatsForUserByQuizzVm.PercentageScore).ToUpperInvariant()] = (statsVm) => statsVm.PercentageScore
            };

            statsQuery = sortByParameter(statsQuery, searchParams.SortBy, searchParams.IsDescending, sortingProperties, stats => stats.SolvedDate);
            statsQuery = statsQuery.Page(searchParams.PageNumber, searchParams.PageSize);

            var statsList = await statsQuery.ToListAsync();
            foreach (var stats in statsList)
            {
                stats.BeginningDate = DateTime.SpecifyKind(stats.BeginningDate, DateTimeKind.Utc);
                stats.SolvedDate = DateTime.SpecifyKind(stats.SolvedDate, DateTimeKind.Utc);
            }

            return statsList;
        }

        public async Task<IEnumerable<TotalSolvedPerCategoryVm>> GetTotalSolvedForUserByCategoriesAsync(string userId, SearchParameters searchParams)
        {
            var totalSolvedPerCategoriesQuery = from stats in dbContext.Set<SolvedQuizzStats>()
                                   join quizzes in dbContext.Set<Quizz>() on stats.QuizzId equals quizzes.Id
                                   join categories in dbContext.Set<Category>() on quizzes.CategoryId equals categories.Id
                                   where stats.UserId == userId && stats.SolvedDate != null
                                   group new { stats, quizzes, categories } by new { categories.Name } into statsWithCategories
                                   select new TotalSolvedPerCategoryVm
                                   {
                                       CategoryName = statsWithCategories.Key.Name,
                                       TotalSolved = statsWithCategories.Count()
                                   };

            var sortingProperties = new Dictionary<string, Expression<Func<TotalSolvedPerCategoryVm, object>>>()
            {
                [nameof(TotalSolvedPerCategoryVm.CategoryName).ToUpperInvariant()] = (statsVm) => statsVm.CategoryName,
                [nameof(TotalSolvedPerCategoryVm.TotalSolved).ToUpperInvariant()] = (statsVm) => statsVm.TotalSolved
            };

            totalSolvedPerCategoriesQuery = sortByParameter(totalSolvedPerCategoriesQuery, searchParams.SortBy, searchParams.IsDescending, sortingProperties, stats => stats.TotalSolved);
            totalSolvedPerCategoriesQuery = totalSolvedPerCategoriesQuery.Page(searchParams.PageNumber, searchParams.PageSize);

            var totalSolvedPerCategories = await totalSolvedPerCategoriesQuery.ToListAsync();
            return totalSolvedPerCategoriesQuery;
        }

        private IQueryable<TViewModel> sortByParameter<TViewModel>(IQueryable<TViewModel> query, string sortPropertyKey, bool isDescending, Dictionary<string, Expression<Func<TViewModel, object>>> sortPropertiesDictionary, Expression<Func<TViewModel, object>> defaultSortProperty)
        {
            sortPropertyKey = sortPropertyKey.ToUpperInvariantOrEmpty();
            var chosenProperty = sortPropertiesDictionary.GetValueOrDefault(sortPropertyKey, defaultSortProperty);

            return query.OrderByDirectionally(chosenProperty, isDescending);
        }
    }
}
