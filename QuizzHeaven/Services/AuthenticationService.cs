﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using QuizHeaven.Helpers.OperationResult;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.Consts;
using QuizzHeaven.Helpers.CustomExceptions;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Helpers.ExtensionMethods.IdentityUserManager;
using QuizzHeaven.Helpers.Validators;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Transactions;

namespace QuizzHeaven.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private UserManager<AppUser> userManager;
        private SignInManager<AppUser> signInManager;
        private IConfiguration configuration;

        public AuthenticationService(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, IConfiguration configuration)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.configuration = configuration;
        }

        public async Task SignUpAsync(SignUpInput signUpInput)
        {
            AppUser newUser = new AppUser
            {
                UserName = signUpInput.Login,
                Email = signUpInput.Email
            };

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var signUpResult = await userManager.CreateAsync(newUser, signUpInput.Password);
                if (!signUpResult.Succeeded)
                {
                    var errors = signUpResult.Errors
                        .Select(error => error.Description)
                        .ToList();

                    throw new UnprocessableLogicException(errors);
                }

                var addToRoleResult = await userManager.AddToRoleAsync(newUser, AuthConsts.Roles.UserRole);
                if (addToRoleResult.Succeeded)
                {
                    scope.Complete();
                }
            }
        }

        public async Task<AuthToken> SignInAsync(SignInInput signInInput)
        {
            var currentUser = await userManager.FindByNameOrEmailAsync(signInInput.Login);
            signInInput.Login = currentUser.UserName;

            var signInResult = await signInManager.PasswordSignInAsync(signInInput.Login, signInInput.Password, signInInput.RememberMe, true);
            if (!signInResult.Succeeded)
            {
                if (signInResult.IsLockedOut)
                {
                    throw new UnauthorizedException("You have entered the wrong credentials too many times. Please try again later");
                }

                throw new UnauthorizedException("Entered credentials don't match any account");
            }

            var signInToken = await createAuthJwt(currentUser.Id);
            return signInToken;
        }

        public async Task SignOutAsync()
        {
            await signInManager.SignOutAsync();
        }

        private async Task<AuthToken> createAuthJwt(string userId)
        {
            var userClaims = new List<Claim>()
            {
                new Claim(JwtRegisteredClaimNames.Sub, userId),
            };

            var appUser = await userManager.FindByIdAsync(userId);
            var usernameClaim = new Claim(ClaimTypes.Name, appUser.UserName);
            userClaims.Add(usernameClaim);

            var appUserRoles = await userManager.GetRolesAsync(appUser);
            foreach (var role in appUserRoles)
            {
                var roleClaim = new Claim(ClaimTypes.Role, role);
                userClaims.Add(roleClaim);
            }

            var signInKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["AuthJwt:Key"]));
            int expiresInMinutes = Convert.ToInt32(configuration["AuthJwt:ExpiryInMinutes"]);
            
            var token = new JwtSecurityToken(
                issuer: configuration["AuthJwt:Issuer"],
                audience: configuration["AuthJwt:Issuer"],
                claims: userClaims,
                expires: DateTime.UtcNow.AddMinutes(expiresInMinutes),
                signingCredentials: new SigningCredentials(signInKey, SecurityAlgorithms.HmacSha256)
            );

            AuthToken createdToken = new AuthToken()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                ExpirationDate = token.ValidTo
            };

            return createdToken;
        }
    }
}
