﻿using QuizHeaven.Helpers.OperationResult;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Models.DTO.Search;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace QuizzHeaven.Services
{
    public interface IAuthenticationService
    {
        Task SignUpAsync(SignUpInput signUpInput);
        Task<AuthToken> SignInAsync(SignInInput signInInput);
        Task SignOutAsync();
    }

    public interface ICrudService<TCreateEntityInput, TEditEntityInput, TViewEntity> where TCreateEntityInput : class where TViewEntity : class
    {
        Task<TViewEntity> CreateAsync(TCreateEntityInput newEntity);
        Task<IEnumerable<TViewEntity>> GetAllAsync(SearchParameters searchParams);
        Task<TViewEntity> GetByKeyAsync(params object[] entityKeys);
        Task EditPartiallyAsync(TEditEntityInput updatedEntity, params object[] entityKeys);
        Task DeleteAsync(params object[] entityKeys);
    }
   
    public interface ICategoriesService : ICrudService<CreateCategoryInput, EditCategoryInput, CategoryVm>
    {
    }

    public interface IQuizzService : ICrudService<CreateQuizzInput, EditQuizzInput, QuizzVm>
    {
        Task<IEnumerable<QuizzVm>> GetByCategoryAsync(int categoryId, SearchParameters searchParams);
        Task<IEnumerable<QuizzVm>> GetByUserAsync(string username);
        Task OnQuizzModifiedAsync(int quizzId);
    }

    public interface IQuestionsService : ICrudService<CreateQuestionInput, EditQuestionInput, QuestionVm>
    {
        Task<IEnumerable<QuestionVm>> GetByQuizzAsync(int quizzId);
    }

    public interface IAnswersService : ICrudService<CreateAnswerInput, EditAnswerInput, AnswerVm>
    {
        Task<IEnumerable<AnswerVm>> GetByQuestionAsync(int questionId);
    }

    public interface ISolvedQuizzStatsService
    {
        Task BeginSolvingAttemptAsync(int quizzId, string solvingUserId);
        Task<SolvedQuizzStatsVm> CompleteSolvingAttemptAsync(int quizzId, string solvingUserId, float percentageScore);

        Task<IEnumerable<SolvedStatsByQuizzVm>> GetByQuizzAsync(int quizzId, SearchParameters searchParams);
        Task<IEnumerable<SolvedStatsByUserVm>> GetByUserAsync(string userId, SearchParameters searchParams);
        Task<IEnumerable<StatsForUserByQuizzVm>> GetForUserByQuizzAsync(string userId, int quizzId, SearchParameters searchParams);
        Task<IEnumerable<TotalSolvedPerCategoryVm>> GetTotalSolvedForUserByCategoriesAsync(string userId, SearchParameters searchParams);
    }

    public interface IQuizzContentService
    {
        Task<int> GetQuestionsPerSolveAttemptAmount(int quizzId);
        Task<IEnumerable<int>> GetAvailableQuestionsIds(int quizzId);
        Task<IEnumerable<QuestionContentVm>> BeginStaticQuestionsAttemptAsync(int quizzId, string solvingUserId);
        Task<IEnumerable<QuestionContentVm>> BeginRandomQuestionsAttemptAsync(int quizzId, string solvingUserId);
    }

    public interface IQuizzResultsService
    {
        Task<SolvedQuizzStatsVm> ValidateAnswersAsync(QuizzAnswersInput input);
    }
}
