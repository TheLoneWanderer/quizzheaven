﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Models.Common;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace QuizzHeaven.Services.Common
{
    public class UpdateDisplayOrderBeforeInsertStrategy<TDisplayOrderedEntity> : IUpdateDisplayOrderStrategy where TDisplayOrderedEntity : class, IDisplayOrderable, new()
    {
        DbContext dbContext;
        Expression<Func<TDisplayOrderedEntity, bool>> identifiersSubGroupPredicate;

        public UpdateDisplayOrderBeforeInsertStrategy(DbContext dbContext, Expression<Func<TDisplayOrderedEntity, bool>> identifiersSubGroupPredicate)
        {
            this.dbContext = dbContext;
            this.identifiersSubGroupPredicate = identifiersSubGroupPredicate;
        }

        public async Task UpdateDisplayOrderAsync(int newEntityDisplayOrder)
        {
            dbContext.Set<TDisplayOrderedEntity>()
                     .Where(entity => entity.DisplayOrder >= newEntityDisplayOrder)
                     .Where(identifiersSubGroupPredicate)
                     .Update(entity => new TDisplayOrderedEntity() { DisplayOrder = (short)(entity.DisplayOrder + 1) });

            await dbContext.SaveChangesAsync();
        }
    }
}
