﻿using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace QuizzHeaven.Services.Common
{
    public class UpdateDisplayOrderAfterDeleteStrategy<TDisplayOrderedEntity> : IUpdateDisplayOrderStrategy where TDisplayOrderedEntity : class, IDisplayOrderable, new()
    {
        DbContext dbContext;
        Expression<Func<TDisplayOrderedEntity, bool>> identifiersSubGroupPredicate;

        public UpdateDisplayOrderAfterDeleteStrategy(DbContext dbContext, Expression<Func<TDisplayOrderedEntity, bool>> identifiersSubGroupPredicate)
        {
            this.dbContext = dbContext;
            this.identifiersSubGroupPredicate = identifiersSubGroupPredicate;
        }

        public async Task UpdateDisplayOrderAsync(int deletedEntityDisplayOrder)
        {
            dbContext.Set<TDisplayOrderedEntity>()
                     .Where(entity => entity.DisplayOrder > deletedEntityDisplayOrder)
                     .Where(identifiersSubGroupPredicate)
                     .Update(entity => new TDisplayOrderedEntity() { DisplayOrder = (short)(entity.DisplayOrder - 1) });

            await dbContext.SaveChangesAsync();
        }
    }
}
