﻿using QuizzHeaven.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Services.Common
{
    public interface IUpdateDisplayOrderStrategy
    {
        public Task UpdateDisplayOrderAsync(int affectedEntityDisplayOrder);
    }
}
