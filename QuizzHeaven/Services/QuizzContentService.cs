﻿using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Helpers.ExtensionMethods.String;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace QuizzHeaven.Services
{
    public class QuizzContentService : IQuizzContentService
    {
        AppDbContext dbContext;
        ISolvedQuizzStatsService solvedStatsService;

        public QuizzContentService(AppDbContext dbContext, ISolvedQuizzStatsService solvedStatsService)
        {
            this.dbContext = dbContext;
            this.solvedStatsService = solvedStatsService;
        }

        public async Task<int> GetQuestionsPerSolveAttemptAmount(int quizzId)
        {
            var questionsLimit = (await GetAvailableQuestionsIds(quizzId)).Count();
            return questionsLimit;
        }

        public async Task<IEnumerable<int>> GetAvailableQuestionsIds(int quizzId)
        {
            List<int> availableQuestionsIds;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.RepeatableRead }, TransactionScopeAsyncFlowOption.Enabled))
            {
                await dbContext.EnsureEntityExists<Quizz, int>(quizz => quizz.Id, quizz => quizz.Id == quizzId);
                availableQuestionsIds = await (from questions in dbContext.Set<Question>()
                                                   where questions.QuizzId == quizzId && dbContext.Set<Answer>().Where(answer => answer.QuestionId == questions.Id).Count() >= 2
                                                   select questions.Id)
                                                   .ToListAsync();

                transaction.Complete();
            }   

            return availableQuestionsIds;
        }

        public async Task<IEnumerable<QuestionContentVm>> BeginStaticQuestionsAttemptAsync(int quizzId, string solvingUserId)
        {
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.RepeatableRead }, TransactionScopeAsyncFlowOption.Enabled))
            {
                await dbContext.EnsureEntityExists<Quizz, int>(quizz => quizz.Id, quizz => quizz.Id == quizzId);
                await dbContext.EnsureEntityExists<AppUser, string>(user => user.Id, user => user.Id == solvingUserId);

                var content = await (from questions in dbContext.Set<Question>()
                                     where questions.QuizzId == quizzId && dbContext.Set<Answer>().Where(answer => answer.QuestionId == questions.Id).Count() >= 2
                                     orderby questions.DisplayOrder ascending
                                     select new QuestionContentVm()
                                     {
                                         Id = questions.Id,
                                         Content = questions.Content,
                                         CorrectAnswerContentHash = dbContext.Set<Answer>().Where(answer => answer.QuestionId == questions.Id && answer.IsCorrectAnswer).Select(answer => answer.Content).First(),
                                         Answers = dbContext.Set<Answer>().Where(answer => answer.QuestionId == questions.Id).OrderBy(answer => answer.DisplayOrder).Select(answer => new AnswerContentVm()
                                         {
                                             Id = answer.Id,
                                             Content = answer.Content,
                                             DisplayOrder = answer.DisplayOrder
                                         }).ToList()
                                     }).ToListAsync();

                await solvedStatsService.BeginSolvingAttemptAsync(quizzId, solvingUserId);
                transaction.Complete();

                foreach (var question in content)
                {
                    question.CorrectAnswerContentHash = question.CorrectAnswerContentHash.AsMD5Hash();
                }

                return content;
            }
        }

        public async Task<IEnumerable<QuestionContentVm>> BeginRandomQuestionsAttemptAsync(int quizzId, string solvingUserId)
        {
            throw new NotImplementedException();
        }
    }
}
