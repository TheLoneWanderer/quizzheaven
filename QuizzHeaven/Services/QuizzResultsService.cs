﻿using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace QuizzHeaven.Services
{
    public class QuizzResultsService : IQuizzResultsService
    {
        AppDbContext dbContext;
        ISolvedQuizzStatsService solvedStatsService;

        public QuizzResultsService(AppDbContext dbContext, ISolvedQuizzStatsService solvedStatsService)
        {
            this.dbContext = dbContext;
            this.solvedStatsService = solvedStatsService;
        }

        public async Task<SolvedQuizzStatsVm> ValidateAnswersAsync(QuizzAnswersInput input)
        {
            using(var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.RepeatableRead }, TransactionScopeAsyncFlowOption.Enabled))
            {
                await dbContext.EnsureEntityExists<AppUser, string>(user => user.Id, user => user.Id == input.UserId);
                await dbContext.EnsureEntityExists<Quizz, int>(quizz => quizz.Id, quizz => quizz.Id == input.QuizzId);

                var inputQuestionsIds = input.AnsweredQuestions.Select(entity => entity.QuestionId);
                var correctAnswersIds = await dbContext.Set<Answer>()
                                                       .Where(answer => inputQuestionsIds.Contains(answer.QuestionId) && answer.IsCorrectAnswer)
                                                       .OrderBy(answer => answer.QuestionId)
                                                       .Select(answer => answer.Id)
                                                       .ToListAsync();

                var inputAnswersIds = input.AnsweredQuestions.OrderBy(entity => entity.QuestionId).Select(entity => entity.AnswerId).ToList();
                
                var correctAnswersAmount = 0;
                for(int answerIndex = 0; answerIndex < inputAnswersIds.Count(); answerIndex++)
                {
                    var inputAnswer = inputAnswersIds[answerIndex];
                    var correctAnswer = correctAnswersIds[answerIndex];

                    if(inputAnswer == correctAnswer)
                    {
                        correctAnswersAmount++;
                    }
                }

                float percentageScore = (float)correctAnswersAmount / correctAnswersIds.Count * 100;
                var solvedStatsVm = await solvedStatsService.CompleteSolvingAttemptAsync(input.QuizzId, input.UserId, percentageScore);
                
                transaction.Complete();

                return solvedStatsVm;
            }
        }
    }
}
