﻿using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Models.Common;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace QuizzHeaven.Services
{
    public class DisplayOrderedCrudFacade<TDisplayOrderedEntity> : CrudFacade<TDisplayOrderedEntity>, IDisplayOrderedCrudFacade<TDisplayOrderedEntity> where TDisplayOrderedEntity : class, IDisplayOrderable, new()
    {
        DbContext dbContext;
        Expression<Func<TDisplayOrderedEntity, bool>> orderingIdentifiersSubGroupPredicate;

        public DisplayOrderedCrudFacade(AppDbContext dbContext) : base(dbContext)
        {
            this.dbContext = dbContext;
        }

        protected override async Task BeforeAddAsync(TDisplayOrderedEntity addedEntity)
        {
            var updateDisplayOrderStrategy = new UpdateDisplayOrderBeforeInsertStrategy<TDisplayOrderedEntity>(dbContext, orderingIdentifiersSubGroupPredicate);
            await updateDisplayOrderStrategy.UpdateDisplayOrderAsync(addedEntity.DisplayOrder);
        }

        protected override async Task BeforeEditAsync(TDisplayOrderedEntity editedEntity, TDisplayOrderedEntity originalEntity)
        {
            if (editedEntity.DisplayOrder != originalEntity.DisplayOrder && editedEntity.DisplayOrder != 0)
            {
                var updateDisplayOrderStrategy = new UpdateDisplayOrderBeforeInsertStrategy<TDisplayOrderedEntity>(dbContext, orderingIdentifiersSubGroupPredicate);
                await updateDisplayOrderStrategy.UpdateDisplayOrderAsync(editedEntity.DisplayOrder);
            }
        }

        protected override async Task AfterEditAsync(TDisplayOrderedEntity editedEntity, TDisplayOrderedEntity originalEntity)
        {
            if (editedEntity.DisplayOrder != originalEntity.DisplayOrder && editedEntity.DisplayOrder != 0)
            {
                var updateDisplayOrderStrategy = new UpdateDisplayOrderAfterDeleteStrategy<TDisplayOrderedEntity>(dbContext, orderingIdentifiersSubGroupPredicate);
                await updateDisplayOrderStrategy.UpdateDisplayOrderAsync(originalEntity.DisplayOrder);
            }
        }

        protected override async Task AfterDeleteAsync(TDisplayOrderedEntity deletedEntity)
        {
            var updateDisplayOrderStrategy = new UpdateDisplayOrderAfterDeleteStrategy<TDisplayOrderedEntity>(dbContext, orderingIdentifiersSubGroupPredicate);
            await updateDisplayOrderStrategy.UpdateDisplayOrderAsync(deletedEntity.DisplayOrder);
        }


        public async Task<TDisplayOrderedEntity> AddAsync(TDisplayOrderedEntity newEntity, Expression<Func<TDisplayOrderedEntity, bool>> orderingIdentifiersSubGroupPredicate, params object[] entityKeys)
        {
            this.orderingIdentifiersSubGroupPredicate = orderingIdentifiersSubGroupPredicate;
            return await AddAsync(newEntity, entityKeys);
        }

        public async Task<TDisplayOrderedEntity> UpsertAsync(TDisplayOrderedEntity newEntity, Expression<Func<TDisplayOrderedEntity, bool>> orderingIdentifiersSubGroupPredicate, params object[] entityKeys)
        {
            this.orderingIdentifiersSubGroupPredicate = orderingIdentifiersSubGroupPredicate;
            return await UpsertAsync(newEntity, entityKeys);
        }

        public async Task EditPartiallyAsync(TDisplayOrderedEntity editedEntity, Expression<Func<TDisplayOrderedEntity, bool>> orderingIdentifiersSubGroupPredicate, params object[] entityKeys)
        {
            this.orderingIdentifiersSubGroupPredicate = orderingIdentifiersSubGroupPredicate;
            await EditPartiallyAsync(editedEntity, entityKeys);
        }

        public async Task DeleteAsync(Expression<Func<TDisplayOrderedEntity, bool>> orderingIdentifiersSubGroupPredicate, params object[] entityKeys)
        {
            this.orderingIdentifiersSubGroupPredicate = orderingIdentifiersSubGroupPredicate;
            await DeleteAsync(entityKeys);
        }
    }
}
