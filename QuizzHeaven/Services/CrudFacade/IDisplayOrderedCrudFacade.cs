﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Models.Common;

namespace QuizzHeaven.Services
{
    public interface IDisplayOrderedCrudFacade<TDisplayOrderedEntity> where TDisplayOrderedEntity : class, IDisplayOrderable, new()
    {
        Task<TDisplayOrderedEntity> AddAsync(TDisplayOrderedEntity newEntity, Expression<Func<TDisplayOrderedEntity, bool>> orderingIdentifiersSubGroupPredicate, params object[] entityKeys);
        Task<IEnumerable<TDisplayOrderedEntity>> GetAllAsync(int pageNumber, int pageSize);
        Task<TDisplayOrderedEntity> GetByKeyAsync(params object[] entityKeys);

        Task<TDisplayOrderedEntity> UpsertAsync(TDisplayOrderedEntity newEntity, Expression<Func<TDisplayOrderedEntity, bool>> orderingIdentifiersSubGroupPredicate, params object[] entityKeys);

        /// <summary>
        /// Copies values from the input model as long as there are properties with corresponding names on the domain model. Null values in the input model will be ignored, essentially updating only passed properties.
        /// </summary>
        /// <param name="editedEntity"></param>
        /// <param name="entityKeys"></param>
        /// <returns></returns>
        Task EditPartiallyAsync(TDisplayOrderedEntity editedEntity, Expression<Func<TDisplayOrderedEntity, bool>> orderingIdentifiersSubGroupPredicate, params object[] entityKeys);

        Task DeleteAsync(Expression<Func<TDisplayOrderedEntity, bool>> orderingIdentifiersSubGroupPredicate, params object[] entityKeys);
        DbContext Context { get; }
    }
}
