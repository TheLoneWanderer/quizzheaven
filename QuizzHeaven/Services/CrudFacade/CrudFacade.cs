﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Helpers.ExtensionMethods.Object;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace QuizzHeaven.Services
{
    public class CrudFacade<TDomainModel> : ICrudFacade<TDomainModel> where TDomainModel : class, new()
    {
        public DbContext Context { get; private set; }

        public CrudFacade(AppDbContext dbContext)
        {
            Context = dbContext;
        }

        public async Task<TDomainModel> AddAsync(TDomainModel newEntity, params object[] entityKeys)
        {
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.Serializable }, TransactionScopeAsyncFlowOption.Enabled))
            {
                if(!entityKeys[0].IsDefaultValue())
                {
                    var existingEntity = await Context.FindAsync<TDomainModel>(entityKeys);
                    if (existingEntity != null)
                    {
                        throw new ConflictException("Entity with provided key already exists");
                    }
                }

                await BeforeAddAsync(newEntity);

                Context.Add(newEntity);
                await Context.SaveChangesAsync();
                
                transaction.Complete();
            }

            return newEntity;
        }

        public async Task<IEnumerable<TDomainModel>> GetAllAsync(int pageNumber, int pageSize)
        {
            return await Context.Set<TDomainModel>()
                                .Page(pageNumber, pageSize)
                                .AsNoTracking()
                                .ToListAsync();
        }
        
        public async Task<TDomainModel> GetByKeyAsync(params object[] entityKeys)
        {
            var entity = await Context.Set<TDomainModel>().FindAsync(entityKeys);           
            if (entity == null)
            {
                throw new NotFoundException("Entity with provided key does not exist.");
            }

            return entity;
        }

        public async Task<TDomainModel> UpsertAsync(TDomainModel newEntity, params object[] entityKeys)
        {
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.Serializable }, TransactionScopeAsyncFlowOption.Enabled))
            {
                if (!entityKeys[0].IsDefaultValue())
                {
                    var existingEntity = await Context.Set<TDomainModel>().FindAsync(entityKeys);
                    if (existingEntity != null)
                    {
                        Context.Remove(existingEntity);
                        await Context.SaveChangesAsync();

                        await AfterDeleteAsync(existingEntity);
                    }
                }

                await BeforeAddAsync(newEntity);

                await Context.AddAsync(newEntity);
                await Context.SaveChangesAsync();

                transaction.Complete();
            }

            return newEntity;
        }

        public async Task EditPartiallyAsync(TDomainModel editedEntityInput, params object[] entityKeys)
        {
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.RepeatableRead }, TransactionScopeAsyncFlowOption.Enabled))
            {
                var originalEntity = await GetByKeyAsync(entityKeys);
                var originalEntry = Context.Entry(originalEntity);

                var unmodifiedOriginalEntity = (TDomainModel) originalEntry.CurrentValues.ToObject();

                await BeforeEditAsync(editedEntityInput, unmodifiedOriginalEntity);

                var efModelProperties = Context.Model.FindEntityType(typeof(TDomainModel)).GetProperties().Where(property => !property.IsPrimaryKey());
                foreach (var efModelProperty in efModelProperties)
                {
                    var editedProperty = editedEntityInput.GetType().GetProperty(efModelProperty.Name);
                    var editedPropertyValue = editedProperty.GetValue(editedEntityInput);

                    if(efModelProperty.IsForeignKey())
                    {
                        if (editedPropertyValue.IsAnEmptyString())
                        {
                            originalEntry.Property(efModelProperty.Name).CurrentValue = null;
                            continue;
                        }

                        else if (efModelProperty.ClrType == typeof(int?))
                        {
                            var propertyAsInt = (int?)editedPropertyValue;
                            if (propertyAsInt == 0)
                            {
                                originalEntry.Property(efModelProperty.Name).CurrentValue = null;
                                continue;
                            }
                        }
                    }

                    if (!editedPropertyValue.IsDefaultValue())
                    {
                        originalEntry.Property(efModelProperty.Name).CurrentValue = editedPropertyValue;
                    }
                }

                await Context.SaveChangesAsync();
                await AfterEditAsync(editedEntityInput, unmodifiedOriginalEntity);

                transaction.Complete();
            }
        }

        public async Task DeleteAsync(params object[] entityKeys)
        {
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.RepeatableRead }, TransactionScopeAsyncFlowOption.Enabled))
            {
                var existingEntity = await GetByKeyAsync(entityKeys);

                Context.Set<TDomainModel>().Remove(existingEntity);
                await Context.SaveChangesAsync();

                await AfterDeleteAsync(existingEntity);

                transaction.Complete();
            }
        }

        /// <summary>
        /// Hook method intended to be overriden in a subclass, it does nothing by default. <br></br>
        /// Runs in a single transaction with the Add query, right before it.
        /// </summary>
        protected virtual Task BeforeAddAsync(TDomainModel addedEntity)
        {
            return Task.CompletedTask;
        }

        /// <summary>
        /// Hook method intended to be overriden in a subclass, it does nothing by default. <br></br>
        /// Runs in a single transaction with the Edit query, right before it.
        /// </summary>
        protected virtual Task BeforeEditAsync(TDomainModel editedEntity, TDomainModel originalEntity)
        {
            return Task.CompletedTask;
        }

        /// <summary>
        /// Hook method intended to be overriden in a subclass, it does nothing by default. <br></br>
        /// Runs in a single transaction with the Edit query, right after it.
        /// </summary>
        protected virtual Task AfterEditAsync(TDomainModel editedEntity, TDomainModel originalEntity)
        {
            return Task.CompletedTask;
        }

        /// <summary>
        /// Hook method intended to be overriden in a subclass, it does nothing by default. <br></br>
        /// Runs in a single transaction with the Delete query, right after it.
        /// </summary>
        protected virtual Task AfterDeleteAsync(TDomainModel deletedEntity)
        {
            return Task.CompletedTask;
        }
    }
}
