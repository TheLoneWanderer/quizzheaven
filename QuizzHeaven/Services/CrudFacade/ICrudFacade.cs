﻿using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Models.DTO.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Services
{
    public interface ICrudFacade<TDomainEntity> where TDomainEntity : class, new()
    {
        Task<TDomainEntity> AddAsync(TDomainEntity newEntity, params object[] entityKeys);
        Task<IEnumerable<TDomainEntity>> GetAllAsync(int pageNumber, int pageSize);
        Task<TDomainEntity> GetByKeyAsync(params object[] entityKeys);

        Task<TDomainEntity> UpsertAsync(TDomainEntity newEntity, params object[] entityKeys);

        /// <summary>
        /// Copies values from the input model as long as there are properties with corresponding names on the domain model. Null values in the input model will be ignored, essentially updating only passed properties.
        /// </summary>
        /// <param name="editedEntity"></param>
        /// <param name="entityKeys"></param>
        /// <returns></returns>
        Task EditPartiallyAsync(TDomainEntity editedEntity, params object[] entityKeys);

        Task DeleteAsync(params object[] entityKeys);
        DbContext Context { get; }
    }
}
