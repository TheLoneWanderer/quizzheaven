﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Data.Configuration;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class EditAnswerInputValidator : AbstractValidator<EditAnswerInput>
    {
        public EditAnswerInputValidator(AppDbContext dbContext)
        {
            RuleFor(editInput => editInput.Content)
                .MaximumLength(AnswerConfigurationConsts.Answer_MaxLength);

            When(editInput => editInput.DisplayOrder != 0, () =>
            {
                RuleFor(editInput => editInput.DisplayOrder)
                    .GreaterThan((short) 0);

                RuleFor(editInput => editInput).CustomAsync(async (editInput, validationContext, cancellToken) =>
                {
                    var existingQuestionsAmount = await dbContext.Set<Answer>().CountAsync(answer => answer.QuestionId == dbContext.Set<Answer>()
                                                                                                                                   .Where(answer => answer.Id == editInput.Id)
                                                                                                                                   .Select(answer => answer.QuestionId)
                                                                                                                                   .First());
                    if (editInput.DisplayOrder > existingQuestionsAmount)
                    {
                        validationContext.AddFailure($"Display order value exceeds the answers amount. The maximal possible value is {existingQuestionsAmount}");
                    }
                });
            });
        }
    }
}
