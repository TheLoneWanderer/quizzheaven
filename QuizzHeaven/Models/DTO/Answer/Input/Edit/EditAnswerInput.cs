﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class EditAnswerInput
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public bool IsCorrectAnswer { get; set; }
        public short DisplayOrder { get; set; }
    }
}
