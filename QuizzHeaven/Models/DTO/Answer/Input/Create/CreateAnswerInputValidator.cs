﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Data.Configuration;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class CreateAnswerInputValidator : AbstractValidator<CreateAnswerInput>
    {
        public CreateAnswerInputValidator(AppDbContext dbContext)
        {
            RuleFor(createInput => createInput.Content)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .MaximumLength(AnswerConfigurationConsts.Answer_MaxLength);

            RuleFor(createInput => createInput.DisplayOrder)
                .GreaterThan((short) 0);

            RuleFor(createInput => createInput).CustomAsync(async (createInput, validationContext, cancellToken) =>
            {
                var existingAnswersAmount = await dbContext.Set<Answer>().CountAsync(answer => answer.QuestionId == createInput.QuestionId);
                var maxPossibleDisplayOrder = existingAnswersAmount + 1;
                if(createInput.DisplayOrder > maxPossibleDisplayOrder)
                {
                    validationContext.AddFailure($"Display order value is too high. Maximal possible value is {maxPossibleDisplayOrder}.");
                }
            });
        }
    }
}
