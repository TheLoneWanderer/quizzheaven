﻿using AutoMapper;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class CreateAnswerInputProfile : Profile
    {
        public CreateAnswerInputProfile()
        {
            CreateMap<CreateAnswerInput, Answer>();
        }
    }
}
