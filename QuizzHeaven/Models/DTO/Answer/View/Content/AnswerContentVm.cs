﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class AnswerContentVm
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public short DisplayOrder { get; set; }
    }
}
