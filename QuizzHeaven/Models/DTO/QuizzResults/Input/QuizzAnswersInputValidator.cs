﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class QuizzAnswersInputValidator : AbstractValidator<QuizzAnswersInput>
    {
        public QuizzAnswersInputValidator(IQuizzContentService quizzContentService)
        {
            RuleFor(input => input).CustomAsync(async (input, validationContext, cancellToken) =>
            {
                var questionsIds = input.AnsweredQuestions.Select(entity => entity.QuestionId);

                var questionsAmount = questionsIds.Count();
                var distinctQuestionsAmount = questionsIds.Distinct().Count();
                if (questionsAmount != distinctQuestionsAmount)
                {
                    validationContext.AddFailure("Questions need to be unique, the list cannot contain the same question twice.");
                }

                var allowedQuestionsAmount = await quizzContentService.GetQuestionsPerSolveAttemptAmount(input.QuizzId);
                if (allowedQuestionsAmount != questionsAmount)
                {
                    validationContext.AddFailure($"Either too many or too little questions were sent. There should be exactly {allowedQuestionsAmount}.");
                }

                var possibleQuestionsIds = await quizzContentService.GetAvailableQuestionsIds(input.QuizzId);
                var existingQuestionsMatchingInputAmount = possibleQuestionsIds.Intersect(questionsIds).Count();
                if (existingQuestionsMatchingInputAmount != questionsAmount)
                {
                    validationContext.AddFailure($"Not all questions match the specified quizz.");
                }
            });
        }
    }
}
