﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class QuizzAnswersInput
    {
        public int QuizzId { get; set; }
        public string UserId { get; set; }

        public List<AnsweredQuestionInput> AnsweredQuestions { get; set; }
    }
}
