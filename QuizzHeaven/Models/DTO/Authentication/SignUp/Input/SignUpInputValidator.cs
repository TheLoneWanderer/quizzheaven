﻿using FluentValidation;
using QuizzHeaven.Helpers.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO.Authentication.SignUp
{
    public class SignUpInputValidator : AbstractValidator<SignUpInput>
    {
        public SignUpInputValidator()
        {
            RuleFor(user => user.Login).NotEmpty();

            RuleFor(user => user.Password).NotEmpty();
            RuleFor(user => user.ConfirmedPassword).NotEmpty();
            RuleFor(user => user.Password).Equal(user => user.ConfirmedPassword).WithMessage("Passwords do not match");

            RuleFor(user => user.Email).NotEmpty().EmailAddress();
        }
    }
}
