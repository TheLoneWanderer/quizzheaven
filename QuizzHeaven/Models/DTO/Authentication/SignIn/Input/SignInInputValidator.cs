﻿using FluentValidation;
using QuizzHeaven.Helpers.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO.Authentication.SignIn
{
    public class SignInInputValidator : AbstractValidator<SignInInput>
    {
        public SignInInputValidator()
        {
            RuleFor(user => user.Login).NotEmpty();
            When(user => user.Login.Contains('@'), () =>
            {
                RuleFor(user => user.Login).EmailAddress();
            })
            .Otherwise(() =>
            {
                var loginValidator = new LoginValidator();
                RuleFor(user => user.Login).Must(loginValidator.IsValid);
            });

            RuleFor(user => user.Password).NotEmpty();
        }
    }
}
