﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class QuestionVm
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public byte DisplayOrder { get; set; }
    }
}
