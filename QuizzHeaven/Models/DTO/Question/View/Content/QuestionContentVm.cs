﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class QuestionContentVm
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public string CorrectAnswerContentHash { get; set; }

        public IEnumerable<AnswerContentVm> Answers { get; set; }
    }
}
