﻿using AutoMapper;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class EditQuestionInputProfile : Profile
    {
        public EditQuestionInputProfile()
        {
            CreateMap<EditQuestionInput, Question>();
        }
    }
}
