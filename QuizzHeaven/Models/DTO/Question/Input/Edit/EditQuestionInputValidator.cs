﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Data.Configuration;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class EditQuestionInputValidator : AbstractValidator<EditQuestionInput>
    {
        public EditQuestionInputValidator(AppDbContext dbContext)
        {
            RuleFor(editInput => editInput.Content)
                .MaximumLength(QuestionConfigurationConsts.Content_MaxLength);

            When(editInput => editInput.DisplayOrder != 0, () =>
            {
                RuleFor(editInput => editInput.DisplayOrder)
                    .GreaterThan((short) 0);

                RuleFor(editInput => editInput)
                    .CustomAsync(async (editInput, validationContext, cancellToken) =>
                    {
                        var existingQuestionsAmount = await dbContext.Set<Question>().CountAsync(question => question.QuizzId == dbContext.Set<Question>()
                                                                                                                                          .Where(question => question.Id == editInput.Id)
                                                                                                                                          .Select(question => question.QuizzId)
                                                                                                                                          .First());
                        if (editInput.DisplayOrder > existingQuestionsAmount)
                        {
                            validationContext.AddFailure($"Display order value exceeds the questions amount. The maximal possible value is {existingQuestionsAmount}");
                        }
                    });
            });
        }
    }
}
