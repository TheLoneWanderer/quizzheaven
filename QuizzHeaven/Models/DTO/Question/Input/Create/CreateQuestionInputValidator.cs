﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Data.Configuration;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class CreateQuestionInputValidator : AbstractValidator<CreateQuestionInput>
    {
        public CreateQuestionInputValidator(AppDbContext dbContext)
        {
            RuleFor(createInput => createInput.Content)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .MaximumLength(QuestionConfigurationConsts.Content_MaxLength);

            RuleFor(createInput => createInput.DisplayOrder)
                .GreaterThan((short) 0);

            RuleFor(createInput => createInput).CustomAsync(async (createInput, validationContext, cancellToken) =>
            {
                var existingQuestionsAmount = await dbContext.Set<Question>().CountAsync(question => question.QuizzId == createInput.QuizzId);
                var maxPossibleDisplayOrder = existingQuestionsAmount + 1;
                if(createInput.DisplayOrder > maxPossibleDisplayOrder)
                {
                    validationContext.AddFailure($"Display order value is too high. Maximal possible value is {maxPossibleDisplayOrder}.");
                }
            });
        }
    }
}
