﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class SolvedStatsByQuizzVm
    {
        public DateTime SolvedDate { get; set; }
        public float PercentageScore { get; set; }
        public string Username { get; set; }
    }
}
