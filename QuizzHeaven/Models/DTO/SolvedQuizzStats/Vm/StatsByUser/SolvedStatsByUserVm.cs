﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class SolvedStatsByUserVm
    {
        public DateTime SolvedDate { get; set; }
        public float PercentageScore { get; set; }
        public int QuizzId { get; set; }
        public string CurrentQuizzName { get; set; }
        public string QuizzNameSnapshot { get; set; }
    }
}
