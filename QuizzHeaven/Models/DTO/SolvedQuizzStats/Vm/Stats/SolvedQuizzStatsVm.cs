﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class SolvedQuizzStatsVm
    {
        public DateTime BeginningDate { get; set; }
        public DateTime? SolvedDate { get; set; }
        public float PercentageScore { get; set; }

        public string QuizzNameSnapshot { get; set; }
        public Guid QuizzVersionSnapshot { get; set; }
    }
}
