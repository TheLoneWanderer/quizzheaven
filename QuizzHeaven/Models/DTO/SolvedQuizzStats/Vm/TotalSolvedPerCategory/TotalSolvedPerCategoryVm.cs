﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class TotalSolvedPerCategoryVm
    {
        public string CategoryName { get; set; }
        public int TotalSolved { get; set; }
    }
}
