﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO.Search
{
    public class SearchParametersValidator : AbstractValidator<SearchParameters>
    {
        public SearchParametersValidator()
        {
            RuleFor(searchParams => searchParams.SearchKey.Trim()).NotEqual("").When(searchParams => searchParams.SearchKey != null);
            RuleFor(searchParams => searchParams.PageNumber).GreaterThanOrEqualTo(0);
            RuleFor(searchParams => searchParams.PageSize).GreaterThanOrEqualTo(0);
        }
    }
}
