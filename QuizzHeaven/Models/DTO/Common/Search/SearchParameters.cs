﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO.Search
{
    public class SearchParameters
    {
        public string SearchKey { get; set; }
        public string SortBy { get; set; }
        public bool IsDescending { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
