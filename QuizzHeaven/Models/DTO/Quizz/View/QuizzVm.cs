﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class QuizzVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AuthorUsername { get; set; }
        public string CategoryName { get; set; }
    }
}
