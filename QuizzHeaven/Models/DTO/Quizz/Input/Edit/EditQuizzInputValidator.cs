﻿using FluentValidation;
using QuizzHeaven.Data;
using QuizzHeaven.Data.Configuration;
using QuizzHeaven.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using System.Security.Claims;
using QuizzHeaven.Helpers.ExtensionMethods.String;
using QuizzHeaven.Helpers.FluentValidation.PropertyValidators;

namespace QuizzHeaven.Models.DTO
{
    public class EditQuizzInputValidator : AbstractValidator<EditQuizzInput>
    {
        public EditQuizzInputValidator(AppDbContext dbContext)
        {
            Include(new BaseQuizzInputValidator(dbContext));

            RuleFor(quizz => quizz.Name)
                .NotEmptyOrWhiteSpace();
        }
    }
}
