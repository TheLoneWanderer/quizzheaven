﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Data.Configuration;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class BaseQuizzInputValidator : AbstractValidator<BaseQuizzInput>
    {
        public BaseQuizzInputValidator(AppDbContext dbContext)
        {
            RuleFor(quizz => quizz.Name)
                .Cascade(CascadeMode.Stop)
                .MaximumLength(QuizzConfigurationConsts.Name_MaxLength)
                .CustomAsync(async (inputName, validationContext, cancellToken) =>
                {
                    var existingQuizzId = await dbContext.Set<Quizz>()
                                                         .Where(quizz => quizz.Name == inputName)
                                                         .Select(quizz => quizz.Id)
                                                         .FirstOrDefaultAsync();

                    if (existingQuizzId != 0)
                    {
                        throw new ConflictException("Quizz with the specified name already exists");
                    }

                }).When(quizz => !string.IsNullOrWhiteSpace(quizz.Name), ApplyConditionTo.CurrentValidator);

            RuleFor(quizz => quizz.Description)
                .MaximumLength(QuizzConfigurationConsts.Description_MaxLength);

            RuleFor(quizz => quizz.CategoryId)
                .CustomAsync(async (inputCategoryId, validationContext, cancellToken) =>
                {
                    var existingCategoryId = await dbContext.Set<Category>()
                                                              .Where(category => category.Id == inputCategoryId)
                                                              .Select(category => category.Id)
                                                              .FirstOrDefaultAsync();

                    if (existingCategoryId == 0)
                    {
                        throw new NotFoundException("Category does not exist");
                    }

                }).When(quizz => quizz.CategoryId != 0, ApplyConditionTo.CurrentValidator);
        }
    }
}
