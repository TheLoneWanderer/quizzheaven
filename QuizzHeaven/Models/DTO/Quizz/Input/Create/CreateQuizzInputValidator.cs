﻿using FluentValidation;
using QuizzHeaven.Data;
using QuizzHeaven.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Threading;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using Microsoft.AspNetCore.Http;
using QuizzHeaven.Helpers.ExtensionMethods;
using System.Security.Claims;
using QuizzHeaven.Data.Configuration;

namespace QuizzHeaven.Models.DTO
{
    public class CreateQuizzInputValidator : AbstractValidator<CreateQuizzInput>
    {
        public CreateQuizzInputValidator(AppDbContext dbContext)
        {
            Include(new BaseQuizzInputValidator(dbContext));

            RuleFor(quizz => quizz.Name)
                .NotEmpty();

            RuleFor(quizz => quizz.CategoryId)
                .NotEmpty();
        }
    }
}
