﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class BaseCategoryInput
    {
        public string Name { get; set; }
        public int? ParentId { get; set; }
    }
}
