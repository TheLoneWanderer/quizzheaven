﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Data.Configuration;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Helpers.ExtensionMethods.String;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class BaseCategoryInputValidator : AbstractValidator<BaseCategoryInput>
    {
        public BaseCategoryInputValidator(AppDbContext dbContext)
        {
            RuleFor(category => category.Name)
                .MaximumLength(CategoryConfigurationConsts.Name_MaxLength);

            RuleFor(category => category.ParentId)
                .CustomAsync(async (parentId, validationContext, cancellToken) =>
                {
                    var dbParentCategoryId = await dbContext.Set<Category>()
                                                          .Where(category => category.Id == parentId)
                                                          .Select(category => category.Id)
                                                          .FirstOrDefaultAsync();

                    if (dbParentCategoryId == 0)
                    {
                        throw new NotFoundException("Parent category does not exist");
                    }

                }).When(input => input.ParentId > 0, ApplyConditionTo.CurrentValidator);
        }
    }
}
