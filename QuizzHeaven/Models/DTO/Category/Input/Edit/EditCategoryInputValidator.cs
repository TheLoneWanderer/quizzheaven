﻿using FluentValidation;
using QuizzHeaven.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuizzHeaven.Helpers.ExtensionMethods.String;
using QuizzHeaven.Helpers.FluentValidation.PropertyValidators;
using QuizzHeaven.Models.Domain;
using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Helpers.ExtensionMethods;

namespace QuizzHeaven.Models.DTO
{
    public class EditCategoryInputValidator : AbstractValidator<EditCategoryInput>
    {
        public EditCategoryInputValidator(AppDbContext dbContext)
        {
            Include(new BaseCategoryInputValidator(dbContext));

            RuleFor(input => input)
                .Cascade(CascadeMode.Stop)
                .Must(input => input.Id > 0).WithMessage("Category ID cannot be empty.")
                .Must(input => !input.Name.IsWhiteSpaceOrEmpty()).WithMessage("Name cannot consist only of whitespaces.")
                .Must(input => !(input.ParentId < 0)).WithMessage("Parent ID cannot be a negative value.")
                .DependentRules(() =>
                {
                    RuleFor(input => input)
                    .CustomAsync(async (editInput, validationContext, cancellToken) =>
                    {
                        var existingCategory = await (from categories in dbContext.Set<Category>()
                                                      where categories.Id == editInput.Id
                                                      select new { Name = categories.Name, ParentId = categories.ParentId })
                                                      .FirstOrDefaultAsync();

                        if (existingCategory == null)
                        {
                            throw new NotFoundException("Category does not exist");
                        }

                        int? parentIdAfterEdit;
                        if (editInput.ParentId == null)
                        {
                            parentIdAfterEdit = existingCategory.ParentId;
                        }
                        else if (editInput.ParentId == 0)
                        {
                            parentIdAfterEdit = null;
                        }
                        else
                        {
                            parentIdAfterEdit = editInput.ParentId;
                        }

                        if (parentIdAfterEdit == editInput.Id)
                        {
                            throw new ConflictException("Category cannot be its own parent.");
                        }

                        string nameAfterEdit = editInput.Name ?? existingCategory.Name;

                        var pairAlreadyExists = await dbContext.ExistsAsync<Category, int>(cat => cat.Id, cat => (cat.Name == nameAfterEdit) && (cat.ParentId == parentIdAfterEdit) && (cat.Id != editInput.Id));
                        if (pairAlreadyExists)
                        {
                            throw new ConflictException("The specified combination of categories already exists. Please choose a different name or subcategories group.");
                        }
                    });
                });
        }
    }
}
