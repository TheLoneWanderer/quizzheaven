﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class EditCategoryInput : BaseCategoryInput
    {
        public int Id { get; set; }
    }
}
