﻿using FluentValidation;
using QuizzHeaven.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuizzHeaven.Helpers.ExtensionMethods.String;
using QuizzHeaven.Helpers.FluentValidation.PropertyValidators;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Helpers.ExtensionMethods;

namespace QuizzHeaven.Models.DTO
{
    public class CreateCategoryInputValidator : AbstractValidator<CreateCategoryInput>
    {
        public CreateCategoryInputValidator(AppDbContext dbContext)
        {
            Include(new BaseCategoryInputValidator(dbContext));

            RuleFor(category => new { category.Name, category.ParentId })
                .Cascade(CascadeMode.Stop)
                .Must(category => !string.IsNullOrWhiteSpace(category.Name)).WithMessage("Category name must have a value.")
                .Must(category => !(category.ParentId <= 0)).WithMessage("Category parent ID cannot be 0. It must either have a value or be null.")
                .DependentRules(() =>
                {
                    RuleFor(category => category)
                    .CustomAsync(async (input, validationContext, cancellToken) =>
                    {
                        var pairAlreadyExists = await dbContext.ExistsAsync<Category, int>(cat => cat.Id, cat => (cat.Name == input.Name) && (cat.ParentId == input.ParentId));
                        if (pairAlreadyExists)
                        {
                            throw new ConflictException("The specified combination of categories already exists. Please choose a different name or subcategories group.");
                        }
                    });
                });
        }
    }
}
