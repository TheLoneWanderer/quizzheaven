﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.DTO
{
    public class CategoryVm
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<CategoryVm> Subcategories { get; set; }
    }
}
