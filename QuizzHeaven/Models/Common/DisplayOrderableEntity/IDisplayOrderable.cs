﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.Common
{
    public interface IDisplayOrderable
    {
        public short DisplayOrder { get; set; }
    }
}
