﻿using QuizzHeaven.Models.Common;
using QuizzHeaven.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.Domain
{
    public class Question : IDisplayOrderable
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public short DisplayOrder { get; set; }


        public int QuizzId { get; set; }

        public virtual Quizz Quizz { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
    }
}
