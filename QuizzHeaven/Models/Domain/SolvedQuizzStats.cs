﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.Domain
{
    public class SolvedQuizzStats
    {
        public int Id { get; set; }
        public DateTime BeginningDate { get; set; }
        public DateTime? SolvedDate { get; set; }
        public float PercentageScore { get; set; }

        public string QuizzNameSnapshot { get; set; }
        public Guid QuizzVersionSnapshot { get; set; }


        public string UserId { get; set; }
        public int QuizzId { get; set; }

        public virtual AppUser User { get; set; }
        public virtual Quizz Quizz { get; set; }
    }
}
