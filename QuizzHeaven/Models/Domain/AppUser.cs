﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace QuizzHeaven.Models.Domain
{
    public class AppUser : IdentityUser
    { 
        public virtual ICollection<Quizz> CreatedQuizzes { get; set; }
        public virtual ICollection<SolvedQuizzStats> SolvedQuizzesStats { get; set; }
    }
}