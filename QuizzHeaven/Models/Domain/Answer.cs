﻿using QuizzHeaven.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.Domain
{
    public class Answer : IDisplayOrderable
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public bool IsCorrectAnswer { get; set; }
        public short DisplayOrder { get; set; }


        public int QuestionId { get; set; }
        public virtual Question Question { get; set; }
    }
}
