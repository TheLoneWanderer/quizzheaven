﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Models.Domain
{
    public class Quizz
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid VersionStamp { get; set; }


        public string AuthorId { get; set; }
        public int CategoryId { get; set; }

        public virtual AppUser Author { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<SolvedQuizzStats> SolvedStats { get; set; }

        public void OnModified()
        {
            VersionStamp = Guid.NewGuid();
        }
    }
}
