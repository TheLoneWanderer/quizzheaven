using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using QuizzHeaven.Data;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using QuizzHeaven.Helpers.DataSeeders;
using Microsoft.AspNetCore.Identity;
using QuizzHeaven.Models.Domain;

namespace QuizzHeaven
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<AppDbContext>();
                var identityRoleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

                var startupSeeder = new StartupDataSeeder(dbContext, identityRoleManager);
                await startupSeeder.SeedAsync();
            }

            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .UseWindsorContainerServiceProvider()
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });
    }
}
