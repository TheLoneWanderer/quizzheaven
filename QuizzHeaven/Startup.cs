using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.MsDependencyInjection;
using FluentValidation.AspNetCore;
using HybridModelBinding;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.Consts;
using QuizzHeaven.Helpers.ErrorHandling.HttpResponseError;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Services;
using QuizzHeaven.Services.OwnershipValidators;
using ScottBrady91.AspNetCore.Identity;

namespace QuizzHeaven
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureContainer(IWindsorContainer container)
        {
            container.Register(Component.For<IAuthenticationService>().ImplementedBy<AuthenticationService>().LifestyleCustom<MsScopedLifestyleManager>());

            container.Register(Component.For(typeof(ICrudFacade<>)).ImplementedBy(typeof(CrudFacade<>)).LifestyleCustom<MsScopedLifestyleManager>());
            container.Register(Component.For(typeof(IDisplayOrderedCrudFacade<>)).ImplementedBy(typeof(DisplayOrderedCrudFacade<>)).LifestyleCustom<MsScopedLifestyleManager>());

            container.Register(Component.For<ICategoriesService>().ImplementedBy<CategoriesService>().LifestyleCustom<MsScopedLifestyleManager>());
            container.Register(Component.For<IQuizzService>().ImplementedBy<QuizzService>().LifestyleCustom<MsScopedLifestyleManager>());
            container.Register(Component.For<IQuestionsService>().ImplementedBy<QuestionsService>().LifestyleCustom<MsScopedLifestyleManager>());
            container.Register(Component.For<IAnswersService>().ImplementedBy<AnswersService>().LifestyleCustom<MsScopedLifestyleManager>());

            container.Register(Component.For<IQuizzContentService>().ImplementedBy<QuizzContentService>().LifestyleCustom<MsScopedLifestyleManager>());
            container.Register(Component.For<IQuizzResultsService>().ImplementedBy<QuizzResultsService>().LifestyleCustom<MsScopedLifestyleManager>());
            container.Register(Component.For<ISolvedQuizzStatsService>().ImplementedBy<SolvedQuizzStatsService>().LifestyleCustom<MsScopedLifestyleManager>());

            container.Register(Component.For<IQuizzOwnershipValidator>().ImplementedBy<QuizzOwnershipValidator>().LifestyleCustom<MsScopedLifestyleManager>());
            container.Register(Component.For<IQuestionOwnershipValidator>().ImplementedBy<QuestionOwnershipValidator>().LifestyleCustom<MsScopedLifestyleManager>());
            container.Register(Component.For<IAnswerOwnershipValidator>().ImplementedBy<AnswerOwnershipValidator>().LifestyleCustom<MsScopedLifestyleManager>());
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(Startup));

            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder =>
                {
                    builder.SetIsOriginAllowed(origin => new Uri(origin).Host == "localhost")
                           .AllowAnyMethod()
                           .AllowAnyHeader();
                });
            });

            services.AddControllers(options =>
            {
                options.Filters.Add(new RequireHttpsAttribute());
            })
            .AddControllersAsServices()
            .ConfigureApiBehaviorOptions(setup =>
            {
                setup.InvalidModelStateResponseFactory = context =>
                {
                    var statusCode = HttpStatusCode.BadRequest;

                    IDictionary<string, IList<string>> errors = new Dictionary<string, IList<string>>();
                    foreach(var property in context.ModelState)
                    {
                        errors.Add(property.Key, property.Value.Errors.Select(modelError => modelError.ErrorMessage).ToList());
                    }

                    var errorsDto = new HttpResponseErrorDto(statusCode, errors);
                    return new BadRequestObjectResult(errorsDto);
                };
            })
            .AddFluentValidation(fv =>
            {
                fv.RegisterValidatorsFromAssembly(typeof(Startup).Assembly);
                fv.DisableDataAnnotationsValidation = true;
            })
            .AddHybridModelBinder(options => 
            { 
                options.FallbackBindingOrder = new[] { Source.Route, Source.QueryString, Source.Body, Source.Form, Source.Header }; 
            });

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });

            // Console logging queries doesn't work with pooling enabled
            //services.AddDbContextPool<AppDbContext>(options =>
            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("AppDbContext"));
                options.UseLazyLoadingProxies();
            });

            services.AddIdentity<AppUser, IdentityRole>(config =>
            {
                // User
                config.User.RequireUniqueEmail = true;
                config.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

                // Password
                config.Password.RequiredLength = 8;
                config.Password.RequiredUniqueChars = 5;

                config.Password.RequireLowercase = true;
                config.Password.RequireUppercase = false;

                config.Password.RequireNonAlphanumeric = false;               
                config.Password.RequireDigit = false;

                // Lockout
                config.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                config.Lockout.MaxFailedAccessAttempts = 20;
                config.Lockout.AllowedForNewUsers = true;

                //config.SignIn.RequireConfirmedEmail = true;
            })
            .AddEntityFrameworkStores<AppDbContext>()
            .AddDefaultTokenProviders()
            .AddTop100000PasswordValidator<AppUser>();

            services.AddAuthentication(option =>
            {
                option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["AuthJwt:Issuer"],
                    ValidAudience = Configuration["AuthJwt:Issuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["AuthJwt:Key"]))
                };
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(AuthConsts.Policies.AdminAccess, policy =>
                {
                    policy.RequireAssertion(context => 
                                            context.User.IsInRole(AuthConsts.Roles.AdminRole));
                });

                options.AddPolicy(AuthConsts.Policies.ModeratorAccess, policy =>
                {
                    policy.RequireAssertion(context => 
                                            context.User.IsInRole(AuthConsts.Roles.AdminRole) ||
                                            context.User.IsInRole(AuthConsts.Roles.ModeratorRole));
                });

                options.AddPolicy(AuthConsts.Policies.UserAccess, policy =>
                {
                    policy.RequireAssertion(context =>
                                            context.User.IsInRole(AuthConsts.Roles.AdminRole) ||
                                            context.User.IsInRole(AuthConsts.Roles.ModeratorRole) ||
                                            context.User.IsInRole(AuthConsts.Roles.UserRole));
                });
            });

            services.AddScoped<IPasswordHasher<AppUser>, Argon2PasswordHasher<AppUser>>();
            services.Configure<Argon2PasswordHasherOptions>(options =>
            {
                options.Strength = Argon2HashStrength.Moderate;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseCors();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
