﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using QuizzHeaven.Models;
using QuizzHeaven.Models.Domain;

namespace QuizzHeaven.Data
{
    public class AppDbContext : IdentityDbContext<AppUser> 
    {
        public DbSet<Category> QuizzCategories { get; set; }
        public DbSet<Quizz> Quizzes { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<SolvedQuizzStats> SolvedQuizzesStats { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AppDbContext).Assembly);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var consoleLogger = LoggerFactory.Create(builder =>
            {
                builder.AddConsole();
                builder.AddFilter((category, level) => category == DbLoggerCategory.Database.Command.Name && level == LogLevel.Information);
            });

            optionsBuilder.UseLoggerFactory(consoleLogger);
        }



        class Employee
        {
            public string name { get; set; }
            public List<string> skills { get; set; }
        }

        private void testingStuffDeleteThisMethodLater()
        {
            List<Employee> emps = new List<Employee>();
            Employee emp1 = new Employee
            {
                name = "Mehdi",
                skills = new List<string> { "Shocking Yourself", "Making cool stuff with electricity" }
            };
            
            Employee emp2 = new Employee
            {
                name = "Asmon",
                skills = new List<string> { "raiding stormwind with some weird drake", "collecting mounts", "streaming" }
            };

            emps.Add(emp1);
            emps.Add(emp2);

            IEnumerable<List<string>> select = emps.Select(e => e.skills);
            IEnumerable<string> selectMany = emps.SelectMany(e => e.skills);

            // Lazy loading
            IQueryable<AppUser> testLinqQuerySyntax = from u in Users
                                      where u.EmailConfirmed == true
                                      select u;
            IQueryable<AppUser> testLinqMethodSyntax = Users
                                        .Where(u => u.EmailConfirmed == true)
                                        .AsQueryable();

            // Eager loading
            // If users had for example a UserStats navigation property, you would use:
            IQueryable<AppUser> testLinqQuerySyntaxEager = from u in Users.Include("UserStats")
                                                      where u.EmailConfirmed == true
                                                      select u;

            IQueryable<AppUser> testLinqMethodSyntaxEager = Users.Include("UserStats")
                                        .Where(u => u.EmailConfirmed == true)
                                        .AsQueryable();

            // or... (assuming it's Email and not UserStats)
            IQueryable<AppUser> testLinqQuerySyntaxEagerSafeType = from u in Users.Include(e => e.Email)
                                                           where u.EmailConfirmed == true
                                                           select u;

            IQueryable<AppUser> testLinqMethodSyntaxEagerSafeType = Users.Include(e => e.Email)
                                        .Where(u => u.EmailConfirmed == true)
                                        .AsQueryable();
       
            // Lazy loading - items load "on demand" - at the first time they're being accessed (each one in loop equals one small SQL query?)
            // *** Assuming EmailConfirmed is an access property, otherwise it will be a single one ***
            foreach (var Xd in testLinqQuerySyntax)
            {
                var x = Xd.EmailConfirmed;
            }
        }      
    }
}
