﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuizzHeaven.Data.Consts.SQL;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Data.Configuration
{
    public static class AnswerConfigurationConsts
    {
        public const int Answer_MaxLength = 80;
    }

    public class AnswerConfiguration : IEntityTypeConfiguration<Answer>
    {
        public void Configure(EntityTypeBuilder<Answer> builder)
        {
            builder.HasKey(answer => answer.Id);

            builder.Property(answer => answer.Content)
                   .HasMaxLength(AnswerConfigurationConsts.Answer_MaxLength)
                   .IsRequired();

            builder.Property(answer => answer.IsCorrectAnswer)
                   .IsRequired();


            builder.HasOne(answer => answer.Question)
                   .WithMany(answer => answer.Answers)
                   .HasForeignKey(answer => answer.QuestionId)
                   .IsRequired();


            builder.HasIndex(answer => new { answer.QuestionId, answer.DisplayOrder })
                   .IsUnique();

            var correctAnswerColumnName = builder.GetColumnNameByProperty(nameof(Answer.IsCorrectAnswer));
            builder.HasIndex(answer => new { answer.QuestionId, answer.IsCorrectAnswer })
                   .IsUnique()
                   .HasFilter($@"{correctAnswerColumnName} = 1");
        }
    }
}
