﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Data.Configuration
{ 
    public class SolvedQuizzStatsConfiguration : IEntityTypeConfiguration<SolvedQuizzStats>
    {
        public void Configure(EntityTypeBuilder<SolvedQuizzStats> builder)
        {
            builder.HasKey(stats => stats.Id);

            builder.Property(stats => stats.BeginningDate)
                   .IsRequired();

            builder.Property(stats => stats.SolvedDate)
                   .IsRequired(false);

            builder.Property(stats => stats.PercentageScore)
                   .IsRequired();

            builder.Property(stats => stats.QuizzNameSnapshot)
                   .HasMaxLength(QuizzConfigurationConsts.Name_MaxLength)
                   .IsRequired();

            builder.Property(stats => stats.QuizzVersionSnapshot)
                   .IsRequired();

            builder.Property(stats => stats.UserId)
                   .IsRequired();
            
            builder.Property(stats => stats.QuizzId)
                   .IsRequired();


            builder.HasOne(stats => stats.User)
                   .WithMany(stats => stats.SolvedQuizzesStats)
                   .HasForeignKey(stats => stats.UserId)
                   .IsRequired()
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(stats => stats.Quizz)
                   .WithMany(stats => stats.SolvedStats)
                   .HasForeignKey(stats => stats.QuizzId)
                   .IsRequired()
                   .OnDelete(DeleteBehavior.Cascade);

            builder.HasIndex(stats => new { stats.UserId, stats.QuizzId });
            builder.HasIndex(stats => new { stats.SolvedDate });

            var statsTableName = builder.GetTableName();
            var highestScoreColumnName = builder.GetColumnNameByProperty(nameof(SolvedQuizzStats.PercentageScore));
            var highestScoreValueConstraintName = $"CK_{statsTableName}_{highestScoreColumnName}_RestrictScoreToPercentageRange";
            var highestScoreValueConstraintSQL = $"[{highestScoreColumnName}] >= 0 AND [{highestScoreColumnName}] <= 100";
            builder.HasCheckConstraint(highestScoreValueConstraintName, highestScoreValueConstraintSQL);
        }
    }
}
