﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Data.Configuration
{
    public static class QuizzConfigurationConsts
    {
        public const int Name_MaxLength = 100;
        public const int Description_MaxLength = 500;
    }

    public class QuizzConfiguration : IEntityTypeConfiguration<Quizz>
    {
        public void Configure(EntityTypeBuilder<Quizz> builder)
        {
            builder.HasKey(quizz => quizz.Id);

            builder.Property(quizz => quizz.Name)
                   .HasMaxLength(QuizzConfigurationConsts.Name_MaxLength)
                   .IsRequired();

            builder.Property(quizz => quizz.Description)
                   .HasMaxLength(QuizzConfigurationConsts.Description_MaxLength);


            builder.HasOne(quizz => quizz.Author)
                   .WithMany(quizz => quizz.CreatedQuizzes)
                   .IsRequired()
                   .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(quizz => quizz.Category)
                   .WithMany(quizz => quizz.Quizzes)
                   .HasForeignKey(quizz => quizz.CategoryId)
                   .IsRequired()
                   .OnDelete(DeleteBehavior.Cascade);
            

            builder.HasIndex(quizz => quizz.Name)
                   .IncludeProperties(quizz => new { quizz.Description, quizz.VersionStamp, quizz.AuthorId })
                   .IsUnique();
        }
    }
}
