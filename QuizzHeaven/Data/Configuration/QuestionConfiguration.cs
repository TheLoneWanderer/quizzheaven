﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Data.Configuration
{
    public static class QuestionConfigurationConsts
    {
        public const int Content_MaxLength = 240;
    }

    public class QuestionConfiguration : IEntityTypeConfiguration<Question>
    {
        public void Configure(EntityTypeBuilder<Question> builder)
        {
            builder.HasKey(question => question.Id);

            builder.Property(question => question.Content)
                   .HasMaxLength(QuestionConfigurationConsts.Content_MaxLength)
                   .IsRequired();

            builder.Property(question => question.DisplayOrder)
                   .IsRequired();
                   

            builder.HasOne(question => question.Quizz)
                   .WithMany(quizz => quizz.Questions)
                   .HasForeignKey(question => question.QuizzId)
                   .IsRequired();


            builder.HasIndex(question => new { question.QuizzId, question.DisplayOrder })
                   .IncludeProperties(question => new { question.Id, question.Content })
                   .IsUnique();
        }
    }
}
