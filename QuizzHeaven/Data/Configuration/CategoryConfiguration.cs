﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Data.Configuration
{
    public static class CategoryConfigurationConsts
    {
        public const int Name_MaxLength = 100;
    }

    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.HasKey(cat => cat.Id);

            builder.Property(cat => cat.Name)
                   .HasMaxLength(CategoryConfigurationConsts.Name_MaxLength);


            builder.HasMany(category => category.ChildCategories)
                   .WithOne(category => category.ParentCategory)
                   .HasForeignKey(category => category.ParentId);


            builder.HasIndex(quizz => new { quizz.Name, quizz.ParentId })
                    .IsUnique();
        }
    }
}
