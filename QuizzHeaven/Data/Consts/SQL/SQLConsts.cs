﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Data.Consts.SQL
{
    public static class DDLConsts
    {
        public const string CreateFunction = "create function";
        public const string CreateProcedure = "create procedure";

        public const string CreateOrAlterFunction = "create or alter function";
        public const string CreateOrAlterProcedure = "create or alter procedure";

        public const string DropFunction = "drop function";
        public const string DropProcedure = "drop procedure";
    }

    public static class FunctionsConsts
    {
        private static string FunctionNamePrefix = "dbo.ufn";
        public static string Categories_GetSubcategoriesFunctionName = $"{FunctionNamePrefix}_Category_GetSubcategoriesNamesIncludingCurrent";

        public static string Answers_GetCorrectAnswersAmountFunctionName = $"{FunctionNamePrefix}_Answer_GetCorrectAnswersAmount";
    }

    public static class StoredProceduresConsts
    {
        private static string StoredProcNamePrefix = "dbo.usp";
        public static string GetQuizzesByCategoryStoredProcName = $"{StoredProcNamePrefix}_Quizz_GetQuizzesByCategory";
    }

    public static class ConstraintsConsts
    {
        public static string CheckConstraintNamePrefix = "dbo.ck";

    }
}
