import jwtDecode, { JwtPayload } from "jwt-decode";
import { ClaimTypes, UserRole } from "../../utils/Consts/AuthConsts";
import { isEmptyOrFalsyString } from "../../utils/Helpers/StringHelpers";
import { getToken, removeToken, storeToken, tokenExists } from "../TokensStorage/TokensStorageManager";

export const userTokenStorageKey = "QuizzHeaven-UserAccount-ApiKey";

const getDecodedToken = (): object | null =>
{
    let authJwt = getToken(userTokenStorageKey);
    if (isEmptyOrFalsyString(authJwt))
    {
        return null;
    }

    let decodedToken = jwtDecode<JwtPayload>(authJwt);
    return decodedToken;
}

export const getCurrentUserId = (): string | null =>
{
    let decodedToken = getDecodedToken();
    if (!decodedToken)
    {
        return null;
    }

    let userId = (decodedToken as any)[ClaimTypes.Sub];
    return userId;
}

export const getCurrentUserName = (): string | null =>
{
    let decodedToken = getDecodedToken();
    if (!decodedToken)
    {
        return null;
    }

    let name = (decodedToken as any)[ClaimTypes.Name];
    return name;
}

export const getCurrentUserRoles = (): string[] =>
{
    let decodedToken = getDecodedToken();
    if (!decodedToken)
    {
        return [];
    }

    let rolesClaim = (decodedToken as any)[ClaimTypes.Role];
    if (Array.isArray(rolesClaim))
    {
        let roles = (rolesClaim as string[]).map(value => value.toUpperCase());
        return roles;
    }

    let normalizedRole = (rolesClaim as string).toUpperCase();
    let role: string[] = [normalizedRole];
    return role;
}

export const currentUserIsInRole = (requiredRole: UserRole) =>
{
    let userRoles = getCurrentUserRoles();
    return userRoles.includes(requiredRole.toUpperCase());
}

export const currentUserIsInRoleOrAdmin = (requiredRole: UserRole) =>
{
    let userRoles = getCurrentUserRoles();
    return userRoles.includes(requiredRole.toUpperCase()) || userRoles.includes(UserRole.AdminRole.toUpperCase());
}

export const isUserLoggedIn = () =>
{
    if (tokenExists(userTokenStorageKey))
    {
        return true;
    }

    return false;
}

export const signInUser = (authToken: string) =>
{
    storeToken(userTokenStorageKey, authToken);
}

export const logoutUser = () =>
{
    removeToken(userTokenStorageKey);
}
