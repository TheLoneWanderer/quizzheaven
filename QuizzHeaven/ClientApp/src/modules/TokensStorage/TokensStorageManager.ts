﻿import { isEmptyOrFalsyString as isEmptyOrFalsy } from "../../utils/Helpers/StringHelpers";

export const storeToken = (tokenKey: string, tokenValue: string): void =>
{
    localStorage.setItem(tokenKey, tokenValue);
}

export const getToken = (tokenKey: string): string =>
{
    let tokenValue = localStorage.getItem(tokenKey);
    return tokenValue ?? "";
}

export const tokenExists = (tokenKey: string): boolean =>
{
    let userAuthJwt = getToken(tokenKey);
    if (isEmptyOrFalsy(userAuthJwt))
    {
        return false;
    }

    return true;
}

export const removeToken = (tokenKey: string): void =>
{
    localStorage.removeItem(tokenKey);
}