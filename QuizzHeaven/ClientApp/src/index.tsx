import 'bootstrap/dist/css/bootstrap.css';

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import configureStore from './store/configureStore';
import registerServiceWorker from './registerServiceWorker';
import { Route } from "react-router";
import { Home } from './pages/Home/Home';
import { HomeLayout } from './pages/Layout/HomeLayout';
import { RoutePaths } from './utils/Consts/RoutingPaths';
import { SearchQuizzes } from './pages/SearchQuizzes/SearchQuizzes';
import { SolveQuizz } from './pages/SolveQuizz/SolveQuizz';
import { ProtectedRoute } from './components/Auth/ProtectedRoute/ProtectedRoute';
import { isUserLoggedIn } from './modules/UserManagers/QuizzUserManager';
import { UserProfile } from './pages/UserProfile/UserProfile';

// Create browser history to use in the Redux store
const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href') as string;
const history = createBrowserHistory();

// Get the application-wide store instance, prepopulating with state from the server where available.
const store = configureStore(history);

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <HomeLayout>
                <Route exact path={RoutePaths.homePage} component={Home} />
                <Route exact path={RoutePaths.searchQuizzesPage} component={SearchQuizzes} />

                <ProtectedRoute exact path={RoutePaths.solveQuizzPage} isAuthenticated={isUserLoggedIn()} redirectPath={RoutePaths.homePage} component={SolveQuizz} />
                <ProtectedRoute exact path={RoutePaths.userProfilePage} isAuthenticated={isUserLoggedIn()} redirectPath={RoutePaths.homePage} component={UserProfile} />
            </HomeLayout>
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root'));

registerServiceWorker();
