export interface SearchParams<TModel extends object>
{
    searchKey?: string,
    sortBy?: Extract<keyof (TModel), string>,
    isDescending?: boolean,
    pageNumber?: number,
    pageSize?: number
}
