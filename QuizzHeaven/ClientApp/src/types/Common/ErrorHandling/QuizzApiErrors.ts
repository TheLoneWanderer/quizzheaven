export interface ApiErrors<PropertiesErrors>
{
    statusCode: number,
    errors: string[],
    propertiesErrors: PropertiesErrors
}