export interface SignUpInput
{
    login: string,
    password: string,
    confirmedPassword: string,
    email: string
}
