export interface SignInInput
{
    login: string,
    password: string
}
