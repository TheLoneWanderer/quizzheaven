export interface CreateQuestionInput
{
    content: string, 
    displayOrder: number,
    quizzId: number
}
