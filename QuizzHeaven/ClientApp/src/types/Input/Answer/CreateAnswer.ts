export interface CreateAnswerInput
{
    content: string, 
    isCorrectAnswer: boolean, 
    displayOrder: number,
    questionId: number
}
