export interface CreateQuizzInput
{
    name: string,
    description: string,
    categoryId: number
}
