export interface SolveAttemptResult
{
    beginningDate: string,
    solvedDate: string,
    percentageScore: number,
    quizzNameSnapshot: string,
    quizzVersionSnapshot: string
}