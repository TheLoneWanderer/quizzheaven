export interface SolvedStatsForUserByQuizz
{
    beginningDate: string,
    solvedDate: string,
    percentageScore: number,
    quizzNameSnapshot: string,
    quizzVersionSnapshot: string
}
