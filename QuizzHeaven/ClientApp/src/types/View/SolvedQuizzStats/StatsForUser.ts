export interface SolvedStatsForUser
{
    solvedDate: string,
    percentageScore: number,
    quizzId: number,
    currentQuizzName: string,
    quizzNameSnapshot: string
}
