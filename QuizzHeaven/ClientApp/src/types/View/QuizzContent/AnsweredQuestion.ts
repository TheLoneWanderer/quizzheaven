export interface AnsweredQuestions
{
    answeredQuestions: AnsweredQuestion[]
}

export interface AnsweredQuestion
{
    questionId: number,
    answerId: number
}