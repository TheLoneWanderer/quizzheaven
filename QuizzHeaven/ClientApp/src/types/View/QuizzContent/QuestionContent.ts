import { AnswerContent } from "./AnswerContent"

export interface QuestionContent
{
    id: number,
    content: string,
    correctAnswerContentHash: string,
    answers: AnswerContent[]
}
