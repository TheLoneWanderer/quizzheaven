export interface AnswerContent
{
    id: number,
    content: string,
    displayOrder: number
}