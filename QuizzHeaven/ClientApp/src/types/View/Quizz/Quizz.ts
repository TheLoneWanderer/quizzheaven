export interface Quizz
{
    id: number,
    name: string,
    description: string,
    authorUsername: string,
    categoryName: string
}