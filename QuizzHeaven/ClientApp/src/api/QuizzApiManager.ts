import axios, { AxiosPromise, AxiosRequestConfig, AxiosRequestHeaders, InternalAxiosRequestConfig } from "axios";
import { getToken } from "../modules/TokensStorage/TokensStorageManager";
import { userTokenStorageKey } from "../modules/UserManagers/QuizzUserManager";
import { SearchParams } from "../types/Common/SearchParams";
import { Quizz } from "../types/View/Quizz/Quizz";
import { AnsweredQuestions } from "../types/View/QuizzContent/AnsweredQuestion";
import { QuestionContent } from "../types/View/QuizzContent/QuestionContent";
import { SolveAttemptResult } from "../types/View/QuizzResults/SolveAttemptResults";
import { SolvedStatsForUser } from "../types/View/SolvedQuizzStats/StatsForUser";
import { SolvedStatsForUserByQuizz } from "../types/View/SolvedQuizzStats/StatsForUserByQuizz";
import { isEmptyOrFalsyString } from "../utils/Helpers/StringHelpers";
import { CreateQuizzInput } from "../types/Input/Quizz/CreateQuizz";
import { CreateQuestionInput } from "../types/Input/Question/CreateQuestion";
import { CreateAnswerInput } from "../types/Input/Answer/CreateAnswer";
import { SignInInput } from "../types/Input/Authentication/SignIn";
import { SignUpInput } from "../types/Input/Authentication/SignUp";

const axiosInstance = axios.create({
    baseURL: "https://localhost:5001/api",
    timeout: 15000
});

const getAxiosConfig = () =>
{
    let config: AxiosRequestConfig =
    {
        headers: {}
    };

    let apiAuthToken = getToken(userTokenStorageKey);
    if (!isEmptyOrFalsyString(apiAuthToken))
    {
        (config.headers as AxiosRequestHeaders).Authorization = `Bearer ${apiAuthToken}`;
    }

    return config;
}

const requests =
{
    get: <TModel extends object>(url: string, searchParams?: SearchParams<TModel>) => axiosInstance.get(`${url}?${getQueryStringFromParams(searchParams)}`, getAxiosConfig()),
    post: (url: string, body: any) => axiosInstance.post(`${url}`, body, getAxiosConfig()),
    put: (url: string, body: any) => axiosInstance.put(`${url}`, body, getAxiosConfig()),
    patch: (url: string, body: any) => axiosInstance.patch(`${url}`, body, getAxiosConfig()),
    delete: (url: string) => axiosInstance.delete(`${url}`, getAxiosConfig())
}

export const Auth = {
    logIn: (signInInput: SignInInput) =>
    {
        return requests.post("/sessions", signInInput);
    },

    signUp: (signUpInput: SignUpInput) =>
    {
        return requests.post("/users", signUpInput);
    }
}

export const Categories = {
    getAll: () =>
    {
        return requests.get("/categories");
    }
}

export const Quizzes = {
    create: (quizzInput: CreateQuizzInput) =>
    {
        return requests.post("/quizzes", quizzInput); 
    },

    getAll: (searchParams?: SearchParams<Quizz>): AxiosPromise<Quizz[]> =>
    {
        return requests.get("/quizzes", searchParams);
    },

    getByCategory: (categoryId: number, searchParams?: SearchParams<Quizz>): AxiosPromise<Quizz[]> =>
    {
        return requests.get(`/categories/${categoryId}/quizzes`, searchParams);
    }
}

export const Questions = {
    create: (questionInput: CreateQuestionInput) =>
    {
        return requests.post("/questions", questionInput); 
    }
}

export const Answers = {
    create: (answerInput: CreateAnswerInput) =>
    {
        return requests.post("/answers", answerInput); 
    }
}

export const QuizzContent = {
    beginSolvingAttempt: (quizzId: number): AxiosPromise<QuestionContent[]> =>
    {
        return requests.get(`/quizzes-content/${quizzId}`);
    }
}

export const QuizzResults = {
    finishSolvingAttempt: (quizzId: number, answers: AnsweredQuestions): AxiosPromise<SolveAttemptResult> =>
    {
        return requests.post(`/quizzes/${quizzId}/results`, answers);
    }
}

export const SolvedQuizzStats = {
    getForUser: (userId: string, searchParams?: SearchParams<SolvedStatsForUser>): AxiosPromise<SolvedStatsForUser[]> =>
    {
        return requests.get(`/users/${userId}/quizzes/solved-stats`, searchParams);
    },

    getForUserByQuizz: (userId: string, quizzId: number, searchParams?: SearchParams<SolvedStatsForUserByQuizz>): AxiosPromise<SolvedStatsForUserByQuizz[]> =>
    {
        return requests.get(`/users/${userId}/quizzes/${quizzId}/solved-stats`, searchParams);
    }
}

const getQueryStringFromParams = <TModel extends object>(searchParams?: SearchParams<TModel>): string =>
{
    if (!searchParams)
    {
        return "";
    }

    let queryStringBuilder = new URLSearchParams();

    type searchParamsPropertiesKeys = keyof SearchParams<TModel>;

    if (!isEmptyOrFalsyString(searchParams.searchKey))
    {
        let paramKey: searchParamsPropertiesKeys = "searchKey";
        queryStringBuilder.append(paramKey, searchParams.searchKey as string);
    }

    if (!isEmptyOrFalsyString(searchParams.sortBy))
    {
        let sortByParamKey: searchParamsPropertiesKeys = "sortBy";
        queryStringBuilder.append(sortByParamKey, searchParams.sortBy as string);

        let isDescendingParamKey: searchParamsPropertiesKeys = "isDescending";
        let isDescendingAsString = searchParams.isDescending ? "true" : "false";
        queryStringBuilder.append(isDescendingParamKey, isDescendingAsString);
    }

    if (searchParams.pageNumber && searchParams.pageSize)
    {
        let pageNumberParamKey: searchParamsPropertiesKeys = "pageNumber";
        let pageSizeParamKey: searchParamsPropertiesKeys = "pageSize";

        queryStringBuilder.append(pageNumberParamKey, searchParams.pageNumber.toString());
        queryStringBuilder.append(pageSizeParamKey, searchParams.pageSize.toString());
    }

    return queryStringBuilder.toString();
}
