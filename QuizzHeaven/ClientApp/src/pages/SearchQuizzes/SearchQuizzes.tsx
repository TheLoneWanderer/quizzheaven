import { CategoriesListController } from "../../components/SearchQuizzes/CategoriesList/CategoriesListController";
import React, { FC, useState } from "react";
import { QuizzesListController } from "../../components/SearchQuizzes/QuizzesList/QuizzesListController";
import { SearchInput } from "../../components/Common/Input/SearchInput/SearchInput";
import "../../assets/styles/Input/Input.min.css";

export const SearchQuizzes: FC = () =>
{
    const [categoryId, setCategoryId] = useState(0);
    const [quizzName, setQuizzName] = useState("");

    return (
        <div className="row">
            <div className="col-lg-1 component-section">
                <CategoriesListController setCategory={setCategoryId}></CategoriesListController>
            </div>

            <div className="col-lg">
                <div className="row">
                    <SearchInput className="input-medium ms-auto me-1" name="quizzName" value={quizzName} valueSetter={setQuizzName}></SearchInput>
                </div>

                <div className="col-lg-10 component-section">
                    <QuizzesListController quizzName={quizzName} categoryId={categoryId}></QuizzesListController>
                </div>
            </div>
        </div>
    );
}
