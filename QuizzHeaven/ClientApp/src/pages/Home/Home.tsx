import React from "react";

export const Home = () =>
{
    return (
        <div className="col-lg-6 mx-auto">
            <div className="text-center">
                <h2> Welcome! </h2>
                <div>
                    <p> To find some quizzes to solve, head to the "Quizzes" tab on the upper left corner, but before you start, you need to create an account in order to save your progress. </p>
                    <p> In the upper right corner (when you log in), you can press your username to move into the user profile, where you can find statistics about your recently solved quizzes. </p>
                </div>
            </div>
        </div>
    );
}