import React from "react";
import "./custom.css";
import { Link } from "react-router-dom";
import { RoutePaths } from "../../utils/Consts/RoutingPaths";
import { RootModal } from "../../components/Layout/RootModal/RootModal";
import { UserNavbar } from "../../components/Layout/UserNavbar/UserNavbar";

export class HomeLayout extends React.PureComponent
{
    render()
    {
        return (
            <React.Fragment>
                <nav className="navbar navbar-expand-sm" id="mainNavbar">
                    <ul className="navbar-nav me-auto">
                        <li className="nav-item">
                            <Link className="nav-link" to={RoutePaths.homePage}> Home </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to={RoutePaths.searchQuizzesPage}> Quizzes </Link>
                        </li>
                    </ul>

                    <ul className="navbar-nav ms-auto">
                        <UserNavbar />
                    </ul>
                </nav>

                <div className="container-fluid">
                    <RootModal></RootModal>
                    {this.props.children}
                </div>
            </React.Fragment>
        );
    }
}
