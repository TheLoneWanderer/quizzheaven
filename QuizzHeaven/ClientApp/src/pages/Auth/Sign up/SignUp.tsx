﻿import * as React from 'react';
import './SignUp.min.css';
import { signInUser } from '../../../modules/UserManagers/QuizzUserManager';
import { Auth as authApi } from '../../../api/QuizzApiManager';
import { LabeledInput } from '../../../components/Common/Input/LabeledInput/LabeledInput';
import { FC, useState } from 'react';
import { ApiErrors } from '../../../types/Common/ErrorHandling/QuizzApiErrors';
import { ButtonInfoSpinner } from '../../../components/Common/Buttons/ButtonInfoSpinner';
import { AxiosError } from 'axios';
import { ErrorsList } from '../../../components/Common/ErrorHandling/ErrorsList/ErrorsList';

export interface SignUpProps
{
}

export const SignUp: FC<SignUpProps> = () =>
{
	const [login, setLogin] = useState("");
	const [password, setPassword] = useState("");
	const [confirmedPassword, setConfirmedPassword] = useState("");
	const [email, setEmail] = useState("");
	const [buttonIsLoading, setButtonIsLoading] = useState(false);

	type PropertiesErrors = {
		Login: string[],
		Password: string[],
		ConfirmedPassword: string[],
		Email: string[]
	}

	const [apiErrors, setApiErrors] = useState<ApiErrors<PropertiesErrors>>();

	return (
		<div>
			<div className="mb-4">
				<div className="mb-3">
					<LabeledInput id="signUpLogin" name="Login" type="text" value={login} valueSetter={setLogin} errors={apiErrors?.propertiesErrors.Login}></LabeledInput>
				</div>

				<div className="mb-3">
					<LabeledInput id="signUpPassword" name="Password" type="password" value={password} valueSetter={setPassword} errors={apiErrors?.propertiesErrors.Password}></LabeledInput>
				</div>

				<div className="mb-3">
					<LabeledInput id="signUpConfirmedPassword" name="Confirm password" type="password" value={confirmedPassword} valueSetter={setConfirmedPassword} errors={apiErrors?.propertiesErrors.ConfirmedPassword}></LabeledInput>
				</div>

				<div className="mb-3">
					<LabeledInput id="signUpEmail" name="Email" type="text" value={email} valueSetter={setEmail} errors={apiErrors?.propertiesErrors.Email}></LabeledInput>
				</div>
			</div>

			<div className="d-block mb-3">
				<ButtonInfoSpinner className="w-100" buttonText="Sign up" isLoading={buttonIsLoading} onClick={() => 
				{
					setButtonIsLoading(true);

					authApi.signUp({ login, password, confirmedPassword, email })
						.then(response =>
						{
							const authToken = response.data.token;
							signInUser(authToken);

							setButtonIsLoading(false);
							window.location.reload();
						})
						.catch((error: AxiosError) =>
						{
							setButtonIsLoading(false);
							setApiErrors(error.response?.data as ApiErrors<PropertiesErrors>);
						});
				}}></ButtonInfoSpinner>
			</div>

			<ErrorsList errors={apiErrors?.errors}></ErrorsList>
		</div>
	);
}
