﻿import './LogIn.min.css';
import { signInUser } from '../../../modules/UserManagers/QuizzUserManager';
import { Auth as authApi } from '../../../api/QuizzApiManager';
import { LabeledInput } from '../../../components/Common/Input/LabeledInput/LabeledInput';
import { ButtonInfoSpinner } from '../../../components/Common/Buttons/ButtonInfoSpinner';
import { FC, useState } from 'react';
import { ApiErrors } from '../../../types/Common/ErrorHandling/QuizzApiErrors';
import { AxiosError } from 'axios';
import { ErrorsList } from '../../../components/Common/ErrorHandling/ErrorsList/ErrorsList';

export interface LogInProps
{
}

export const LogIn: FC<LogInProps> = () =>
{
	const [login, setLogin] = useState("testAccount");
	const [password, setPassword] = useState("testPassword");
	const [buttonIsLoading, setButtonIsLoading] = useState(false);

	type PropertiesErrors = {
		Login: string[],
		Password: string[]
	};

	const [apiErrors, setApiErrors] = useState<ApiErrors<PropertiesErrors>>();

	return (
		<div>
			<div className="mb-4">
				<div className="mb-3">
					<LabeledInput id="signInUsername" name="Login" type="text" value={login} valueSetter={setLogin} errors={apiErrors?.propertiesErrors.Login}></LabeledInput>
				</div>

				<div className="mb-3">
					<LabeledInput id="signInPassword" name="Password" type="password" value={password} valueSetter={setPassword} errors={apiErrors?.propertiesErrors.Password}></LabeledInput>
				</div>
			</div>

			<div className="d-block mb-3">
				<ButtonInfoSpinner className="w-100" buttonText="Log in" isLoading={buttonIsLoading} onClick={() => 
				{
					setButtonIsLoading(true);

					authApi.logIn({ login, password })
						.then(response => 
						{
							const authToken = response.data.token;
							signInUser(authToken);

							setButtonIsLoading(false);
							window.location.reload();
						})
						.catch((error: AxiosError) => 
						{
							setButtonIsLoading(false);
							setApiErrors(error.response?.data as ApiErrors<PropertiesErrors>);
						});
				}}></ButtonInfoSpinner>
			</div>

			<ErrorsList errors={apiErrors?.errors}></ErrorsList>
		</div>
	);
}
