import { FC, useEffect, useMemo, useState } from "react";
import { Column, useTable } from "react-table";
import { SolvedQuizzStats as solvedStatsApi } from "../../api/QuizzApiManager";
import { BasicReadableDate } from "../../components/Common/Date/BasicReadableDate";
import { BasicTable } from "../../components/Common/Table/BasicTable";
import { getCurrentUserId } from "../../modules/UserManagers/QuizzUserManager";
import { SolvedStatsForUser } from "../../types/View/SolvedQuizzStats/StatsForUser";
import { SearchParams } from "../../types/Common/SearchParams";
import { AxiosError } from "axios";
import "./css/UserProfile.min.css";
import "../../assets/styles/Wrappers/ComponentSection/ComponentSection.min.css";
import { BasicMessage } from "../../components/Common/Messages/Basic/BasicMessage";
import { BasicLoadingStatus } from "../../components/Common/Messages/Loading/BasicLoadingStatus";

interface UserProfileProps
{
}

export const UserProfile: FC<UserProfileProps> = () =>
{
    const statsColumns = useMemo<Column<SolvedStatsForUser>[]>(
        () =>
            [
                {
                    Header: "Name",
                    accessor: stats => stats.quizzNameSnapshot
                },
                {
                    Header: "Solved date",
                    accessor: stats => 
                    {
                        return new Date(stats.solvedDate).toLocaleString();
                    }
                },
                {
                    Header: "Score",
                    accessor: stats => 
                    {
                        return `${stats.percentageScore}%`;
                    }
                }
            ],
        []
    );

    const [statsData, setStatsData] = useState<SolvedStatsForUser[]>([]);
    const [dataFetchingState, setFetchingState] = useState({ isFetching: false, fetchingFailed: false });

    useEffect(() =>
    {
        setFetchingState(state => ({ ...state, isFetching: true }));

        solvedStatsApi.getForUser(getCurrentUserId() as string, { sortBy: "solvedDate", isDescending: true, pageNumber: 1, pageSize: 15 })
            .then(response =>
            {
                setStatsData(response.data);
                setFetchingState(state => ({ ...state, isFetching: false }));
            })
            .catch((error: AxiosError) =>
            {
                console.error(error.response?.data);
                setFetchingState(state => ({ isFetching: false, fetchingFailed: true }));
            });
    }, []);

    return (
        <div className="col-lg-6 mx-auto">
            <div className="component-section">
                <h4> Recently solved: </h4>
                <BasicTable
                    tableClassName="profile-recently-solved-table"
                    columnsStructure={statsColumns}
                    columnsData={statsData}
                />

                <BasicLoadingStatus isLoading={dataFetchingState.isFetching} loadingFailed={dataFetchingState.fetchingFailed} loadingFailedMessage="Loading failed."></BasicLoadingStatus>
            </div>
        </div>
    );
}
