import { AxiosError } from "axios";
import React, { useRef } from "react";
import { FC, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { QuizzContent as quizzContentApi, QuizzResults as quizzResultsApi } from "../../api/QuizzApiManager";
import { BasicMessage } from "../../components/Common/Messages/Basic/BasicMessage";
import { QuestionContent } from "../../types/View/QuizzContent/QuestionContent";
import { AnsweredQuestion, AnsweredQuestions } from "../../types/View/QuizzContent/AnsweredQuestion";
import MD5 from "crypto-js/md5";
import classNames from "classnames";
import { SolveResults } from "../../components/SolveQuizz/SolveResults";
import { SolveAttemptResult } from "../../types/View/QuizzResults/SolveAttemptResults";
import { BasicLoadingStatus } from "../../components/Common/Messages/Loading/BasicLoadingStatus";
import "./css/SolveQuizz.min.css";
import "../../assets/styles/Text/Text.min.css";
import "../../assets/styles/Sizing/Viewport/ViewportSizing.min.css";

export interface SolveQuizzProps
{
    quizzId?: number
}

export const SolveQuizz: FC<SolveQuizzProps> = ({ quizzId }) =>
{
    const [questionsList, setQuestionsList] = useState<QuestionContent[]>();
    const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
    const [questionsFetchingState, setQuestionsFetchingState] = useState({ isFetching: false, fetchingFailed: false });

    const answeredQuestions = useRef<AnsweredQuestions>({ answeredQuestions: [] });
    const [isAnswerSelected, setIsAnswerSelected] = useState(false);

    const [solvingResult, setSolvingResult] = useState<SolveAttemptResult>();
    const [sendingAnswersState, setSendingAnswersState] = useState({ isSending: false, sendingFailed: false });

    const locationState = useLocation<SolveQuizzProps>().state;

    useEffect(() =>
    {
        setQuestionsFetchingState(state => ({ ...state, isFetching: true }));
        quizzContentApi.beginSolvingAttempt(quizzId ?? locationState.quizzId as number)
            .then(response =>
            {
                setQuestionsList(response.data);
                setQuestionsFetchingState(state => ({ ...state, isFetching: false }));
            })
            .catch((error: AxiosError) =>
            {
                console.error(error.response?.data);
                setQuestionsFetchingState({ isFetching: false, fetchingFailed: true });
            });
    }, []);

    useEffect(() =>
    {
        if (questionsList !== undefined && currentQuestionIndex >= questionsList.length)
        {
            setSendingAnswersState(state => ({ ...state, isSending: true }));
            quizzResultsApi.finishSolvingAttempt(quizzId ?? locationState.quizzId as number, answeredQuestions.current)
                .then(response =>
                {
                    setSolvingResult(response.data);
                    setSendingAnswersState(state => ({ ...state, isSending: false }));
                })
                .catch((error: AxiosError) =>
                {
                    console.error(error.response?.data);
                    setSendingAnswersState({ isSending: false, sendingFailed: true });
                });
        }
    }, [currentQuestionIndex])

    return (
        <React.Fragment>
            {
                questionsList !== undefined &&
                questionsList.length > 0 &&
                <div className="col-lg-6 mx-auto">
                    {
                        currentQuestionIndex < questionsList.length &&
                        <React.Fragment>
                            <div className="row mb-1">
                                <span className="questions-counter">
                                    Current question: {currentQuestionIndex + 1}/{questionsList.length}
                                </span>
                            </div>

                            <div className="row mb-2">
                                <span className="question-header"> {questionsList[currentQuestionIndex].content} </span>
                            </div>

                            <div className="row">
                                {
                                    questionsList[currentQuestionIndex].answers.map((answer) =>
                                    {
                                        let isCorrectAnswer = questionsList[currentQuestionIndex].correctAnswerContentHash === MD5(answer.content).toString().toUpperCase();

                                        return (
                                            <span
                                                key={answer.id}
                                                className={
                                                    classNames(
                                                        {
                                                            "clickable-answer-box": true,
                                                            "correct-answer": isCorrectAnswer && isAnswerSelected,
                                                            "wrong-answer": !isCorrectAnswer && isAnswerSelected
                                                        })
                                                }

                                                onClick={(!isAnswerSelected &&
                                                    (() => 
                                                    {
                                                        answeredQuestions.current.answeredQuestions.push({ questionId: questionsList[currentQuestionIndex].id, answerId: answer.id });
                                                        setIsAnswerSelected(true);

                                                        setTimeout(() => 
                                                        {
                                                            setCurrentQuestionIndex(previousValue => previousValue + 1);
                                                            setIsAnswerSelected(false);
                                                        }, 1800)
                                                    })) || undefined}>
                                                {answer.content}
                                            </span>
                                        )
                                    })
                                }
                            </div>
                        </React.Fragment>
                    }

                    <BasicLoadingStatus isLoading={questionsFetchingState.isFetching} loadingFailed={questionsFetchingState.fetchingFailed} loadingFailedMessage="Loading failed."></BasicLoadingStatus>
                    <BasicMessage message="This quizz does not contain any questions" isDisplayed={questionsList?.length === 0}></BasicMessage>
                </div>
            }

            {
                solvingResult !== undefined &&
                <div className="col-lg-2 mx-auto">
                    <SolveResults results={solvingResult}></SolveResults>
                    <BasicLoadingStatus isLoading={sendingAnswersState.isSending} loadingFailed={sendingAnswersState.sendingFailed} loadingFailedMessage="Loading failed."></BasicLoadingStatus>
                </div>
            }
        </React.Fragment>
    );
}
