export enum UserRole
{
    AdminRole = "admin"
}

export enum ClaimTypes
{
    Sub = "sub",
    Name = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name",
    Role = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role"
}