﻿interface RoutingPaths
{
    homePage: string,
    logInPage: string,
    signUpPage: string,
    searchQuizzesPage: string,
    solveQuizzPage: string,
    userProfilePage: string
}

export const RoutePaths: RoutingPaths =
{
    homePage: "/",
    logInPage: "/LogIn",
    signUpPage: "/SignUp",
    searchQuizzesPage: "/SearchQuizzes",
    solveQuizzPage: "/SolveQuizz",
    userProfilePage: "/UserProfile"
};
