import pluralize from "pluralize";

/**
 * Checks for nulls, undefined and empty string/only white-space value
 * @param value 
 * @returns 
 */
export const isEmptyOrFalsyString = (value: string | null | undefined): boolean =>
{
    return (!value || value.trim().length === 0);
}

/**
 * Checks whether a string is specifically empty or contains only whitespaces
 * @param value 
 * @returns 
 */
export const isEmptyOrWhiteSpacesOnly = (value: string): boolean =>
{
    return (value.trim().length === 0);
}

/**
 * Returns a singular/plural version of a word based on the amount
 * 
 * Optionally appends the value, ex. ("hour", 3, true) would return "3 hours"
 * @param word
 * @param amount 
 * @param valueIncluded
 * @returns 
 */
export const pluraliseWord = (word: string, amount: number, valueIncluded: boolean): string =>
{
    return pluralize(word, amount, valueIncluded);
}
