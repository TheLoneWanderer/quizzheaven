import { DateTime, DurationUnits } from "luxon"

/**
 * Returns the amount of time passed between two dates.
 * @param beginDate 
 * @param endDate 
 * @param units By default the difference sum is returned as milliseconds, but it's possible to split the units using an optional argument
 * @returns 
 */
export const getDateDifference = (beginDate: Date, endDate: Date, units?: DurationUnits) =>
{
    let startMoment = DateTime.fromJSDate(beginDate);
    let endMoment = DateTime.fromJSDate(endDate);

    let duration = endMoment.diff(startMoment, units);
    return duration;
}