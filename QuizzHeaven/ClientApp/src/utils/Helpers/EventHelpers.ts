export const isScrolledToTheBottom = (scrollEvent: React.UIEvent) =>
{
    let node = scrollEvent.currentTarget;
    let isBottom = node.scrollHeight - Math.ceil(node.scrollTop) === node.clientHeight;

    return isBottom;
}