import React, { useEffect, useState } from "react";

export const useDebounce = (value: string, timeoutMilliseconds: number) =>
{
    const [debouncedValue, setDebouncedValue] = useState(value);

    useEffect(() => 
    {
        var timeoutHandler = setTimeout(
            () => 
            {
                setDebouncedValue(value);
            }, timeoutMilliseconds);

        return () => 
        {
            clearTimeout(timeoutHandler);
        }
    }, [value, timeoutMilliseconds]);

    return debouncedValue;
}
