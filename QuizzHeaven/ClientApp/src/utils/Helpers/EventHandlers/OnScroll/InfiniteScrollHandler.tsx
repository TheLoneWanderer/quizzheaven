import React from "react";
import { isScrolledToTheBottom } from "../../EventHelpers";

export const infiniteScrollHandler = (scrollEvent: React.UIEvent, action: () => void, hasMorePages: boolean, isLoadingNewPage: boolean) =>
{
    if (isScrolledToTheBottom(scrollEvent))
    {
        if (hasMorePages && !isLoadingNewPage)
        {
            action();
        }
    }
}