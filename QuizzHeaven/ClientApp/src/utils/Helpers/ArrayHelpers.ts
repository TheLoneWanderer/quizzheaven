/**
 * Returns true if the array has at least one element
 * @param array 
 * @returns 
 */
export const notEmpty = (array?: any[]): boolean =>
{
    return array !== undefined && array.length > 0;
}

/**
 * Returns true if the array has zero elements
 * @param array 
 * @returns 
 */
export const isEmpty = (array: any[]): boolean =>
{
    return array.length === 0;
}