import { createStore, combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import { History } from "history";
import { AppState, storeReducers } from ".";

export default function configureStore(history: History, initialState?: AppState) 
{
    const rootReducer = combineReducers({
        ...storeReducers,
        router: connectRouter(history)
    });

    return createStore(
        rootReducer,
        initialState
    );
}
