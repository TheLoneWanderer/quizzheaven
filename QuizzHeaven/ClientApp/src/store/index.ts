import { RootModalState } from "./Layout/RootModal/RootModalTypes";
import { RootModalReducer } from "./Layout/RootModal/RootModalReducer";

export interface AppState
{
    rootModal: RootModalState;
}

export const storeReducers =
{
    rootModal: RootModalReducer
}