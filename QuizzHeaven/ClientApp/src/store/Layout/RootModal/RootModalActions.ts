import { SHOW_MODAL_ACTION_TYPE, ModalType, ShowModalAction, HIDE_MODAL_ACTION_TYPE, HideModalAction } from "./RootModalTypes";

export const RootModalActionCreators = {
    showModal: <TProps>(modalToOpenType: ModalType, modalProps: TProps) => ({ type: SHOW_MODAL_ACTION_TYPE, modalType: modalToOpenType, modalProps: modalProps }) as ShowModalAction,
    hideModal: () => ({ type: HIDE_MODAL_ACTION_TYPE }) as HideModalAction
}