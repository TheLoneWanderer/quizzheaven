import * as RootModalTypes from "./RootModalTypes";
import { Reducer, Action } from "redux";

const initialState: RootModalTypes.RootModalState =
{
    isOpen: false,
    modalType: RootModalTypes.EMPTY_MODAL_TYPE,
    modalProps: {}
}

export const RootModalReducer: Reducer<RootModalTypes.RootModalState> = (state: RootModalTypes.RootModalState = initialState, incomingAction: Action) =>
{
    if (state === undefined)
    {
        return initialState;
    }

    const modalAction = incomingAction as RootModalTypes.KnownRootModalAction;
    switch (modalAction.type)
    {
        case RootModalTypes.SHOW_MODAL_ACTION_TYPE:
            return {
                isOpen: true,
                modalType: modalAction.modalType,
                modalProps: modalAction.modalProps
            };

        case RootModalTypes.HIDE_MODAL_ACTION_TYPE:
            return initialState;

        default:
            return state;
    }
}