export const EMPTY_MODAL_TYPE = "EMPTY_MODAL";
export const LOG_IN_MODAL_TYPE = "LOG_IN_MODAL";
export const SIGN_UP_MODAL_TYPE = "SIGN_UP_MODAL";
export const QUIZZ_PREVIEW_MODAL_TYPE = "QUIZZ_PREVIEW_MODAL";

export type ModalType =
    typeof EMPTY_MODAL_TYPE |
    typeof LOG_IN_MODAL_TYPE |
    typeof SIGN_UP_MODAL_TYPE |
    typeof QUIZZ_PREVIEW_MODAL_TYPE;

export interface RootModalState
{
    isOpen: boolean,
    modalType: ModalType,
    modalProps: any
}

export const SHOW_MODAL_ACTION_TYPE = "OPEN_MODAL";
export const HIDE_MODAL_ACTION_TYPE = "CLOSE_MODAL";

export interface ShowModalAction { type: typeof SHOW_MODAL_ACTION_TYPE, modalType: ModalType, modalProps: any };
export interface HideModalAction { type: typeof HIDE_MODAL_ACTION_TYPE };

export type KnownRootModalAction = ShowModalAction | HideModalAction;
