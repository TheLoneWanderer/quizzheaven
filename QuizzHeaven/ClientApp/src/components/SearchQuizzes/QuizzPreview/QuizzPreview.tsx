import React, { FC } from 'react';
import { Quizz } from '../../../types/View/Quizz/Quizz';
import "../../../assets/styles/Button/Button.min.css";
import { Link } from 'react-router-dom';
import { RoutePaths } from '../../../utils/Consts/RoutingPaths';
import { useDispatch } from 'react-redux';
import { RootModalActionCreators } from '../../../store/Layout/RootModal/RootModalActions';
import { SolveQuizz, SolveQuizzProps } from '../../../pages/SolveQuizz/SolveQuizz';
import { ProtectedLink } from '../../Auth/ProtectedLink/ProtectedLink';
import "./css/QuizzPreview.min.css";

export interface QuizzPreviewProps
{
    quizz: Quizz
}

export const QuizzPreview: FC<QuizzPreviewProps> = ({ quizz }) =>
{
    const solveQuizzProps: SolveQuizzProps = { quizzId: quizz.id };

    return (
        <div className="quizz-preview-container">
            <div className="mb-3">
                <h5 className="fw-600"> {quizz.name} </h5>
            </div>

            <div>
                <div className="border-bottom pb-1 mb-1">
                    <span> {quizz.description ?? "(No description)"} </span>
                </div>

                <div className="d-flex justify-content-end">
                    <span className="fst-italic"> Made by: {quizz.authorUsername} </span>
                </div>
            </div>

            <div className="mt-auto">
                <ProtectedLink className="btn btn-success button-wide" to={{ pathname: RoutePaths.solveQuizzPage, state: solveQuizzProps }} value={"Play"}></ProtectedLink>
            </div>
        </div>
    )
}
