import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleRight } from '@fortawesome/free-solid-svg-icons'
import React, { FC, useState } from "react";
import { Category } from "../../../types/View/Category/Category";
import "../../../assets/styles/List/SelectableList/SelectableList.min.css";
import "../../../assets/styles/Transform/Transform.min.css";
import classNames from "classnames";
import { notEmpty } from "../../../utils/Helpers/ArrayHelpers";
import { CategoriesList } from "./CategoriesList";

interface CategoriesListItemProps
{
    category: Category,
    selectCategory: (categoryId: number, onDeselectedCallback: () => void) => void
}

export const CategoryListItem: FC<CategoriesListItemProps> = ({ category, selectCategory }) =>
{
    const [isSelected, setIsSelected] = useState(false);
    const [subcategoriesVisible, showSubcategories] = useState(false);

    return (
        <div
            className={
                classNames({
                    "selectable-list-dropdown-wrapper": notEmpty(category.subcategories)
                })}
            onMouseEnter={() => { showSubcategories(true); }}
            onMouseLeave={() => { showSubcategories(false); }}>

            <span
                className={
                    classNames({
                        "selectable-list-item": true,
                        "selected-item": isSelected
                    })
                }

                onClick={() => 
                {
                    isSelected ?
                        selectCategory(0, () => { }) :
                        selectCategory(category.id, () => { setIsSelected(false); });

                    setIsSelected(!isSelected);
                }}>

                {category.name}

                {
                    notEmpty(category.subcategories) &&
                    <FontAwesomeIcon icon={faAngleRight} size="sm"
                        className={
                            classNames({
                                "ms-2": true,
                                "smooth-rotations": true,
                                "rotated-90": subcategoriesVisible
                            })
                        }></FontAwesomeIcon>
                }
            </span>

            {
                notEmpty(category.subcategories) &&
                <CategoriesList
                    className={
                        classNames({
                            "d-none": !subcategoriesVisible,
                            "selectable-list-dropdown": true
                        })}
                    categories={category.subcategories}
                    selectCategory={selectCategory}>
                </CategoriesList>
            }
        </div>
    );
}
