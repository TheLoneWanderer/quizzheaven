import React, { FC } from "react";
import { Category } from "../../../types/View/Category/Category";
import { CategoryListItem } from "./CategoryListItem";
import "../../../assets/styles/List/SelectableList/SelectableList.min.css";
import "../../../assets/styles/Text/Text.min.css";

interface CategoriesListProps
{
    className?: string,
    categories: Category[],
    selectCategory: (categoryId: number, onDeselectedCallback: () => void) => void
}

export const CategoriesList: FC<CategoriesListProps> = ({ className, categories, selectCategory }) =>
{
    return (
        <div className={className}>
            <ul className="selectable-list mb-0">
                {
                    categories.map(category =>
                    (
                        <li key={category.id} className="fw-600">
                            <CategoryListItem category={category} selectCategory={selectCategory}></CategoryListItem>
                        </li>
                    ))
                }
            </ul>
        </div >
    );
}
