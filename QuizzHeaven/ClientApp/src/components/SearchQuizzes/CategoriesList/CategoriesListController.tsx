import { AxiosError } from "axios";
import React, { useRef, useState, FC, useEffect } from "react";
import { Categories as categoriesApi } from "../../../api/QuizzApiManager";
import { Category } from "../../../types/View/Category/Category";
import { BasicMessage } from "../../Common/Messages/Basic/BasicMessage";
import { BasicInfoMessage } from "../../Common/Messages/Info/BasicInfoMessage";
import { BasicLoadingStatus } from "../../Common/Messages/Loading/BasicLoadingStatus";
import { CategoriesList } from "./CategoriesList";

interface CategoriesListControllerProps
{
    setCategory: (categoryId: number) => void
}

export const CategoriesListController: FC<CategoriesListControllerProps> = ({ setCategory }) =>
{
    const [categoriesList, setCategoriesList] = useState<Category[]>([]);
    const [dataFetchingState, setFetchingState] = useState({ isFetching: false, fetchingFailed: false });

    useEffect(() =>
    {
        setFetchingState(state => ({ ...state, isFetching: true }));

        categoriesApi.getAll()
            .then(response =>
            {
                setCategoriesList(response.data);
                setFetchingState(state => ({ ...state, isFetching: false }));
            }).catch((error: AxiosError) =>
            {
                console.error(error.response?.data);
                setFetchingState({ isFetching: false, fetchingFailed: true });
            })
    }, []);

    const previousCategoryDeselectedCallback = useRef<() => void>();

    const selectNewCategory = (newCategoryId: number, onDeselectedCallback: () => void) =>
    {
        if (previousCategoryDeselectedCallback.current)
        {
            previousCategoryDeselectedCallback.current();
        }

        previousCategoryDeselectedCallback.current = onDeselectedCallback;
        setCategory(newCategoryId);
    }

    return (
        <React.Fragment>
            <CategoriesList
                categories={categoriesList}
                selectCategory={selectNewCategory}>
            </CategoriesList>

            <BasicLoadingStatus isLoading={dataFetchingState.isFetching} loadingFailed={dataFetchingState.fetchingFailed} loadingFailedMessage="Loading failed."></BasicLoadingStatus>
        </React.Fragment>
    );
}
