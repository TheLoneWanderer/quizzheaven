import { AxiosError } from "axios";
import React from "react";
import { FC, useEffect, useState } from "react";
import { Quizzes as quizzesApi } from "../../../api/QuizzApiManager";
import { SearchParams } from "../../../types/Common/SearchParams";
import { Quizz } from "../../../types/View/Quizz/Quizz";
import { useDebounce } from "../../../utils/Helpers/CustomHooks/UseDebounce/UseDebounce";
import { infiniteScrollHandler } from "../../../utils/Helpers/EventHandlers/OnScroll/InfiniteScrollHandler";
import { BasicMessage } from "../../Common/Messages/Basic/BasicMessage";
import { BasicInfoMessage } from "../../Common/Messages/Info/BasicInfoMessage";
import { BasicLoadingStatus } from "../../Common/Messages/Loading/BasicLoadingStatus";
import { QuizzesList } from "./QuizzesList";

interface QuizzesListControllerProps
{
    quizzName?: string,
    categoryId?: number
}

export const QuizzesListController: FC<QuizzesListControllerProps> = ({ quizzName, categoryId }) =>
{
    const [quizzes, setQuizzes] = useState<Quizz[]>([]);
    const debouncedQuizzName = useDebounce(quizzName ?? "", 1000);

    const [paginationState, setPaginationState] = useState({ pageNumber: 1, hasMorePages: true });
    const [dataFetchingState, setFetchingState] = useState({ isFetching: false, fetchingFailed: false });

    useEffect(() =>
    {
        setQuizzes([]);
        setPaginationState({ pageNumber: 1, hasMorePages: true });
    }, [debouncedQuizzName, categoryId]);

    useEffect(() =>
    {
        let searchParams: SearchParams<Quizz> = { searchKey: debouncedQuizzName ?? "", pageNumber: paginationState.pageNumber, pageSize: 80 };
        let searchEndpoint = (categoryId) ?
            quizzesApi.getByCategory(categoryId, searchParams) :
            quizzesApi.getAll(searchParams);

        setFetchingState(state => ({ ...state, isFetching: true }));

        searchEndpoint
            .then(response =>
            {
                let pageSize = searchParams.pageSize as number;
                if (pageSize > response.data.length)
                {
                    setPaginationState(state => ({ ...state, hasMorePages: false }));
                }

                setQuizzes(existingQuizzes => existingQuizzes.concat(response.data));
                setFetchingState(state => ({ ...state, isFetching: false }));
            })
            .catch((error: AxiosError) => 
            {
                console.error(error.response?.data);
                setFetchingState({ isFetching: false, fetchingFailed: true });
            });
    }, [debouncedQuizzName, categoryId, paginationState.pageNumber]);

    return (
        <React.Fragment>
            <QuizzesList quizzes={quizzes} onScrollHandler={(scrollEvent) => 
            {
                infiniteScrollHandler(
                    scrollEvent,
                    () =>
                    {
                        setPaginationState(state => ({ ...state, pageNumber: state.pageNumber + 1 }))
                    },
                    paginationState.hasMorePages,
                    dataFetchingState.isFetching
                );
            }}>
            </QuizzesList>

            <BasicLoadingStatus isLoading={dataFetchingState.isFetching} loadingFailed={dataFetchingState.fetchingFailed} loadingFailedMessage="Loading failed."></BasicLoadingStatus>
        </React.Fragment>
    );
}
