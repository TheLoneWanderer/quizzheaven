import { FC } from "react";
import { useDispatch } from "react-redux";
import { RootModalActionCreators } from "../../../store/Layout/RootModal/RootModalActions";
import { QUIZZ_PREVIEW_MODAL_TYPE } from "../../../store/Layout/RootModal/RootModalTypes";
import { Quizz } from "../../../types/View/Quizz/Quizz";
import { QuizzPreviewProps } from "../QuizzPreview/QuizzPreview";

interface QuizzListItemProps
{
    quizz: Quizz
}

export const QuizzListItem: FC<QuizzListItemProps> = ({ quizz }) =>
{
    const dispatch = useDispatch();
    const quizzPreviewProps: QuizzPreviewProps = { quizz: quizz };

    return (
        <span className="selectable-list-item" onClick={() => { dispatch(RootModalActionCreators.showModal<QuizzPreviewProps>(QUIZZ_PREVIEW_MODAL_TYPE, quizzPreviewProps)); }}>
            {quizz.name}
        </span>
    );
}
