import React, { FC } from "react";
import { Quizz } from "../../../types/View/Quizz/Quizz";
import { QuizzListItem } from "./QuizzListItem";
import "../../../assets/styles/List/ScrollableList/ScrollableList.min.css";

interface QuizzesListProps
{
    className?: string,
    quizzes: Quizz[],
    onScrollHandler?: (event: React.UIEvent<HTMLUListElement>) => void
}

export const QuizzesList: FC<QuizzesListProps> = ({ className, quizzes, onScrollHandler }) =>
{
    return (
        <div className={className}>
            <ul className="selectable-list scrollable-list-large mb-0" onScroll={onScrollHandler}>
                {
                    quizzes.map(quizz =>
                    (
                        <li key={quizz.id}>
                            <QuizzListItem quizz={quizz}></QuizzListItem>
                        </li>
                    ))
                }
            </ul>
        </div>
    );
}
