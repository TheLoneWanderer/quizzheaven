import classNames from "classnames";
import { FC } from "react";

interface ButtonInfoSpinnerProps
{
    buttonText: string,
    isLoading: boolean,
    className?: string,
    onClick: () => void
}

export const ButtonInfoSpinner: FC<ButtonInfoSpinnerProps> = ({ buttonText, isLoading, className, onClick }) =>
{
    return (
        <button className={`btn btn-outline-info ${className}`} type="button" onClick={onClick}>
            {isLoading ? "" : buttonText}
            <span className={classNames({ "spinner-border spinner-border-sm": isLoading })}></span>
        </button>
    );
}

ButtonInfoSpinner.defaultProps =
{
    buttonText: "",
    isLoading: false
}