import classNames from "classnames";
import React from "react";
import { FC } from "react";
import { InputType } from "reactstrap/es/Input";
import uuid from "uuid-random";
import "./css/BasicInput.min.css";

interface BasicInputProps
{
    id?: string,
    className?: string,
    name: string,
    type?: InputType,
    placeholder?: string,
    value: string,
    valueSetter: (value: string) => void,
    errors?: string[]
}

export const BasicInput: FC<BasicInputProps> = ({ id, className, name, type, placeholder, value, valueSetter, errors }) =>
{
    return (
        <React.Fragment>
            <input
                className={classNames(
                    {
                        "form-control": true,
                        "is-invalid": (errors?.length ?? 0) > 0
                    }, className)}
                id={id}
                name={name}
                type={type}
                placeholder={placeholder}
                value={value}
                onChange={(changeEvent) => { valueSetter(changeEvent.target.value); }}>
            </input>

            <div className="invalid-feedback">
                <ul>
                    {errors?.map((errorMessage) => <li key={uuid()}> {errorMessage} </li>)}
                </ul>
            </div>
        </React.Fragment >
    );
}

BasicInput.defaultProps =
{
    type: "text"
}
