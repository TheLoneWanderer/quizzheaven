import React, { FC } from "react";
import { InputType } from "reactstrap/es/Input";
import { BasicInput } from "../BasicInput/BasicInput";

interface LabeledInputProps
{
    id?: string,
    className?: string,
    name: string,
    type?: InputType,
    placeholder?: string,
    value: string,
    valueSetter: (value: string) => void,
    errors?: string[]
}

export const LabeledInput: FC<LabeledInputProps> = ({ id, className, name, type, placeholder, value, valueSetter, errors }) =>
{
    return (
        <div>
            <label className="control-label" htmlFor={id}> {name}: </label>
            <BasicInput id={id} className={className} name={name} type={type} placeholder={placeholder} value={value} valueSetter={valueSetter} errors={errors}></BasicInput>
        </div>
    );
}

LabeledInput.defaultProps =
{
    type: "text"
}
