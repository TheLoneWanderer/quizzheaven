import React, { FC } from "react";
import { InputType } from "reactstrap/es/Input";
import { BasicInput } from "../BasicInput/BasicInput";

interface SearchInputProps
{
    id?: string,
    className?: string,
    name: string,
    placeholder?: string,
    value: string,
    valueSetter: (value: string) => void,
    errors?: string[]
}

export const SearchInput: FC<SearchInputProps> = ({ id, className, name, placeholder, value, valueSetter, errors }) =>
{
    return (
        <BasicInput id={id} className={className} name={name} type="search" placeholder={placeholder} value={value} valueSetter={valueSetter} errors={errors}></BasicInput>
    );
}

SearchInput.defaultProps =
{
    placeholder: "Search"
}