import React from 'react';
import { Column, useTable } from 'react-table'

export interface BasicTableProps<TData extends object>
{
    tableClassName?: string,
    bodyTrClassName?: string,
    thClassName?: string,
    tdClassName?: string,
    columnsStructure: Column<TData>[],
    columnsData: TData[]
}

export const BasicTable = <TData extends object>(props: BasicTableProps<TData>) =>
{
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow
    } = useTable({ columns: props.columnsStructure, data: props.columnsData });

    return (
        <React.Fragment>
            <table className={props.tableClassName} {...getTableProps}>
                <thead>
                    {headerGroups.map(headerGroup =>
                    (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map(column =>
                            (
                                <th className={props.thClassName} {...column.getHeaderProps()}>
                                    {column.render("Header")}
                                </th>
                            ))}
                        </tr>
                    ))}
                </thead>

                <tbody {...getTableBodyProps()}>
                    {
                        rows.map(row =>
                        {
                            prepareRow(row);
                            return (
                                <tr className={props.bodyTrClassName} {...row.getRowProps()}>
                                    {row.cells.map(cell =>
                                    (
                                        <td className={props.tdClassName} {...cell.getCellProps()}>
                                            {cell.render("Cell")}
                                        </td>
                                    ))}
                                </tr>
                            );
                        })
                    }
                </tbody>
            </table>
        </React.Fragment>
    );
}
