import classNames from 'classnames'
import React, { FC } from 'react'

interface BasicMessageProps
{
    className?: string,
    message: string,
    isDisplayed: boolean
}

export const BasicMessage: FC<BasicMessageProps> = ({ className, message, isDisplayed }) =>
{
    if (!isDisplayed)
    {
        return null;
    }

    return (
        <h5 className={classNames("text-center", className)}>
            {message}
        </h5>
    )
}
