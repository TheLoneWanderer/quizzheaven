import classNames from "classnames";
import { FC } from "react";
import { BasicMessage } from "../Basic/BasicMessage";

interface BasicInfoMessageProps
{
    className?: string,
    message: string,
    isDisplayed: boolean
}

export const BasicInfoMessage: FC<BasicInfoMessageProps> = ({ className, message, isDisplayed }) =>
{
    return (
        <BasicMessage
            className={classNames({ "text-info": true }, className)}
            message={message}
            isDisplayed={isDisplayed}>
        </BasicMessage>
    );
}
