import classNames from "classnames";
import { FC } from "react";
import { BasicMessage } from "../Basic/BasicMessage";

interface BasicErrorMessageProps
{
    className?: string,
    message: string,
    isDisplayed: boolean
}

export const BasicErrorMessage: FC<BasicErrorMessageProps> = ({ className, message, isDisplayed }) =>
{
    return (
        <BasicMessage
            className={classNames({ "text-danger": true }, className)}
            message={message}
            isDisplayed={isDisplayed}>
        </BasicMessage>
    );
}
