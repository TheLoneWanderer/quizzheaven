import React, { FC } from "react"
import { BasicErrorMessage } from "../Errors/BasicErrorMessage";

export interface BasicLoadingStatusProps
{
    className?: string,
    isLoading: boolean,
    loadingFailed: boolean,
    loadingFailedMessage: string
}

export const BasicLoadingStatus: FC<BasicLoadingStatusProps> = (props) => 
{
    if (props.isLoading)
        return (
            <div className="d-flex justify-content-center">
                <div className="spinner-border" role="status">
                    <span className="visually-hidden"> Loading... </span>
                </div>
            </div>
        );

    else if (props.loadingFailed)
        return (
            <BasicErrorMessage isDisplayed={props.loadingFailed} message={props.loadingFailedMessage} />
        );

    return null;
}
