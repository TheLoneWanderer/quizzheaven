import classNames from "classnames";
import { FC } from "react";
import uuid from "uuid-random";

interface ErrorsListProps
{
    errors?: string[]
}

export const ErrorsList: FC<ErrorsListProps> = ({ errors }) =>
{
    return (
        <div className={classNames({
            "d-block": (errors?.length ?? 0) > 0,
            "invalid-feedback": true
        })}>
            <ul className="mb-1">
                {errors?.map((errorMessage) => <li key={uuid()}> {errorMessage} </li>)}
            </ul>
        </div >
    );
}