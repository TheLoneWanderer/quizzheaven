import React, { FC } from 'react'
import { pluraliseWord } from '../../../utils/Helpers/StringHelpers'

export interface BasicReadableDateProps
{
    years?: number,
    months?: number,
    weeks?: number,
    days?: number,
    hours?: number,
    minutes?: number,
    seconds?: number,
    milliseconds?: number
}

export const BasicReadableDate: FC<BasicReadableDateProps> = (props) => 
{
    let stringBuilderArray: string[] = [];

    if (props.years !== undefined && props.years > 0) stringBuilderArray.push(pluraliseWord("years", props.years, true));
    if (props.months !== undefined && props.months > 0) stringBuilderArray.push(pluraliseWord("months", props.months, true));
    if (props.weeks !== undefined && props.weeks > 0) stringBuilderArray.push(pluraliseWord("weeks", props.weeks, true));
    if (props.days !== undefined && props.days > 0) stringBuilderArray.push(pluraliseWord("days", props.days, true));
    if (props.hours !== undefined && props.hours > 0) stringBuilderArray.push(pluraliseWord("hours", props.hours, true));
    if (props.minutes !== undefined && props.minutes > 0) stringBuilderArray.push(pluraliseWord("minutes", props.minutes, true));
    if (props.seconds !== undefined && props.seconds > 0) stringBuilderArray.push(pluraliseWord("seconds", props.seconds, true));
    if (props.milliseconds !== undefined && props.milliseconds > 0) stringBuilderArray.push(pluraliseWord("milliseconds", props.milliseconds, true));

    let readableDate = stringBuilderArray.join(", ");

    return (
        <React.Fragment>
            {readableDate}
        </React.Fragment>
    )
}
