import React, { FC } from "react";
import { SolveAttemptResult } from "../../types/View/QuizzResults/SolveAttemptResults";
import { getDateDifference } from "../../utils/Helpers/DateHelpers";
import { BasicReadableDate } from "../Common/Date/BasicReadableDate";
import "../../assets/styles/Wrappers/ComponentSection/ComponentSection.min.css";

export interface SolveResultsProps
{
    results: SolveAttemptResult
}

export const SolveResults: FC<SolveResultsProps> = ({ results }) =>
{
    let beginningDate = new Date(results.beginningDate);
    let solvedDate = new Date(results.solvedDate);
    let timeScore = getDateDifference(beginningDate, solvedDate, ["hours", "minutes", "seconds"]);

    return (
        <div className="component-section p-1">
            <div className="mb-3">
                <h4 className="text-center"> Quizz finished! </h4>
            </div>

            <div>
                {`Your score is: ${results.percentageScore}%`}
            </div>

            <div>
                {`Your solving time: `}
                <BasicReadableDate hours={timeScore.hours} minutes={timeScore.minutes} seconds={timeScore.seconds} />
            </div>
        </div>
    )
}
