﻿import { Route, Redirect, RouteProps } from "react-router-dom";
import { RoutePaths } from "../../../utils/Consts/RoutingPaths";
import { FC } from "react";
import { ComponentType } from "react";

interface ProtectedRouteProps extends RouteProps
{
    isAuthenticated: boolean,
    redirectPath?: string
}

export const ProtectedRoute: FC<ProtectedRouteProps> = ({ isAuthenticated, redirectPath, ...routeProps }) => 
{
    if (!isAuthenticated)
    {
        return <Redirect to={{ pathname: redirectPath }} />
    }

    return <Route {...routeProps} />;
}

ProtectedRoute.defaultProps =
{
    redirectPath: RoutePaths.homePage
}
