import { Location, LocationDescriptor } from "history";
import React from "react";
import { FC } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { isUserLoggedIn } from "../../../modules/UserManagers/QuizzUserManager";
import { RootModalActionCreators } from "../../../store/Layout/RootModal/RootModalActions";
import { LOG_IN_MODAL_TYPE } from "../../../store/Layout/RootModal/RootModalTypes";

interface ProtectedLinkProps
{
    className: string,
    to: LocationDescriptor | ((location: Location) => LocationDescriptor),
    value: string
}

export const ProtectedLink: FC<ProtectedLinkProps> = ({ className, to, value }) =>
{
    const dispatch = useDispatch();

    if (!isUserLoggedIn())
    {
        return (
            <Link
                className={className}
                onClick={() =>
                {
                    dispatch(RootModalActionCreators.showModal(LOG_IN_MODAL_TYPE, {}));
                }} to="#">
                {value}
            </Link>
        );
    }

    return (
        <Link
            className={className}
            onClick={() => 
            {
                dispatch(RootModalActionCreators.hideModal());
            }}
            to={to}> {value}
        </Link>
    );
}