import React, { ComponentType, ReactElement } from "react";
import { LogIn, LogInProps } from "../../../pages/Auth/LogIn/LogIn";
import { SignUp, SignUpProps } from "../../../pages/Auth/Sign up/SignUp";
import { LOG_IN_MODAL_TYPE, QUIZZ_PREVIEW_MODAL_TYPE, SIGN_UP_MODAL_TYPE } from "../../../store/Layout/RootModal/RootModalTypes";
import { QuizzPreview, QuizzPreviewProps } from "../../SearchQuizzes/QuizzPreview/QuizzPreview";

interface IReactNodeDictionary
{
    [key: string]: (props: any) => ReactElement<any>
}
export const ModalBodiesContainer: IReactNodeDictionary =
{
    [LOG_IN_MODAL_TYPE]: (props: LogInProps) => { return React.createElement(LogIn, props); },
    [SIGN_UP_MODAL_TYPE]: (props: SignUpProps) => { return React.createElement(SignUp, props); },
    [QUIZZ_PREVIEW_MODAL_TYPE]: (props: QuizzPreviewProps) => { return React.createElement(QuizzPreview, props); }
};
