import * as React from "react";
import { AppState } from "../../../store";
import { useDispatch, useSelector } from "react-redux";
import { HideModalAction, ModalType } from "../../../store/Layout/RootModal/RootModalTypes";
import { RootModalActionCreators } from "../../../store/Layout/RootModal/RootModalActions";
import { Modal, ModalBody } from "reactstrap";
import { ModalBodiesContainer } from "./RootModalBodies";
import { FC } from "react";

export const RootModal: FC = () =>
{
    const isOpen: boolean = useSelector((state: AppState) => state.rootModal.isOpen);
    const modalTypeKey: ModalType = useSelector((state: AppState) => state.rootModal.modalType);
    const modalProps: any = useSelector((state: AppState) => state.rootModal.modalProps);

    const dispatch = useDispatch();

    const modalContentFactory = ModalBodiesContainer[modalTypeKey];
    if (modalContentFactory == null)
    {
        return (
            <React.Fragment>
            </React.Fragment>
        );
    }

    return (
        <div>
            <Modal isOpen={isOpen} toggle={() => { dispatch<HideModalAction>(RootModalActionCreators.hideModal()); }}>
                <ModalBody>
                    {modalContentFactory(modalProps)}
                </ModalBody>
            </Modal>
        </div>
    );
}
