import { Link } from "react-router-dom";
import { getCurrentUserName } from "../../../modules/UserManagers/QuizzUserManager";
import { RoutePaths } from "../../../utils/Consts/RoutingPaths";

export const UserNavbarAccount = () =>
{
    return (
        <Link className="nav-link" to={RoutePaths.userProfilePage}> {getCurrentUserName() ?? "Empty username"} </Link>
    );
}