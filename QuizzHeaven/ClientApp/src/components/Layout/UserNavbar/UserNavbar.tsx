import * as React from 'react';
import { useDispatch } from 'react-redux';
import { isUserLoggedIn, logoutUser } from '../../../modules/UserManagers/QuizzUserManager';
import { LogInProps } from '../../../pages/Auth/LogIn/LogIn';
import { SignUpProps } from '../../../pages/Auth/Sign up/SignUp';
import { RootModalActionCreators } from '../../../store/Layout/RootModal/RootModalActions';
import { LOG_IN_MODAL_TYPE, SIGN_UP_MODAL_TYPE } from '../../../store/Layout/RootModal/RootModalTypes';
import { UserNavbarAccount } from './UserNavbarAccount';

export const UserNavbar = () =>
{
    const dispatch = useDispatch();

    if (isUserLoggedIn())
    {
        return (
            <React.Fragment>
                <li>
                    <UserNavbarAccount />
                </li>
                <li>
                    <button className="btn-nav-link nav-link" onClick={() => { logoutUser(); window.location.reload(); }} type="button"> Log out </button>
                </li>
            </React.Fragment>
        );
    }

    return (
        <React.Fragment>
            <li>
                <button className="btn-nav-link" onClick={() => { dispatch(RootModalActionCreators.showModal<LogInProps>(LOG_IN_MODAL_TYPE, {})); }}> Log in </button>
            </li>

            <li>
                <button className="btn-nav-link" onClick={() => { dispatch(RootModalActionCreators.showModal<SignUpProps>(SIGN_UP_MODAL_TYPE, {})); }}> Sign up </button>
            </li>
        </React.Fragment>
    );
}
