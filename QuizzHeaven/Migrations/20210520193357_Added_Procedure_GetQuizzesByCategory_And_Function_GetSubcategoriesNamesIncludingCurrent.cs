﻿using Microsoft.EntityFrameworkCore.Migrations;
using QuizzHeaven.Data.Configuration;
using QuizzHeaven.Data.Consts.SQL;

namespace QuizzHeaven.Migrations
{
    public partial class Added_Procedure_GetQuizzesByCategory_And_Function_GetSubcategoriesNamesIncludingCurrent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            const string RootCategoryNameParam = "@RootCategoryName";

            migrationBuilder.Sql($@"
                    {DDLConsts.CreateOrAlterFunction} {FunctionsConsts.Categories_GetSubcategoriesFunctionName}({RootCategoryNameParam} nvarchar({CategoryConfigurationConsts.Name_MaxLength}))
                    returns table

                    return(
	                    with cteWrapper as (
		                    select RootCategory.Name 
		                    from QuizzCategories RootCategory
		                    where Name = {RootCategoryNameParam}

		                    union all
		                    select ChildCategory.Name
		                    from QuizzCategories ChildCategory
		                    join cteWrapper cte on cte.Name = ChildCategory.ParentName
		                    )

	                    select * from cteWrapper
                    );"
                );

            migrationBuilder.Sql($@"
                    {DDLConsts.CreateOrAlterProcedure} {StoredProceduresConsts.GetQuizzesByCategoryStoredProcName}({RootCategoryNameParam} nvarchar({CategoryConfigurationConsts.Name_MaxLength}))
                    as

                    select * from Quizzes
                    where CategoryName in (select * from {FunctionsConsts.Categories_GetSubcategoriesFunctionName}({RootCategoryNameParam}));
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"{DDLConsts.DropFunction} {FunctionsConsts.Categories_GetSubcategoriesFunctionName}");
            migrationBuilder.Sql($"{DDLConsts.DropProcedure} {StoredProceduresConsts.GetQuizzesByCategoryStoredProcName}");
        }

    }
}
