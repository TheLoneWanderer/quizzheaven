﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuizzHeaven.Migrations
{
    public partial class Added_Answers_OnlyOneCorrectAnswerAllowed_FilteredUniqueIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Answers_QuestionId_IsCorrectAnswer",
                table: "Answers",
                columns: new[] { "QuestionId", "IsCorrectAnswer" },
                unique: true,
                filter: "IsCorrectAnswer = 1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Answers_QuestionId_IsCorrectAnswer",
                table: "Answers");
        }
    }
}
