﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuizzHeaven.Migrations
{
    public partial class Fix_Added_Missing_Unique_Keyword_For_Questions_DisplayOrderQuizzId_Constraint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Questions_QuizzId_DisplayOrder",
                table: "Questions");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_QuizzId_DisplayOrder",
                table: "Questions",
                columns: new[] { "QuizzId", "DisplayOrder" },
                unique: true)
                .Annotation("SqlServer:Include", new[] { "Id", "Content" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Questions_QuizzId_DisplayOrder",
                table: "Questions");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_QuizzId_DisplayOrder",
                table: "Questions",
                columns: new[] { "QuizzId", "DisplayOrder" })
                .Annotation("SqlServer:Include", new[] { "Id", "Content" });
        }
    }
}
