﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuizzHeaven.Migrations
{
    public partial class Modified_Categories_AddedIdAsPrimaryKeyAndNameParentNameCombinationUniqueIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_QuizzCategories_QuizzCategories_ParentName",
                table: "QuizzCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_Quizzes_QuizzCategories_CategoryName",
                table: "Quizzes");

            migrationBuilder.DropIndex(
                name: "IX_Quizzes_CategoryName",
                table: "Quizzes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_QuizzCategories",
                table: "QuizzCategories");

            migrationBuilder.DropIndex(
                name: "IX_QuizzCategories_ParentName",
                table: "QuizzCategories");

            migrationBuilder.DropColumn(
                name: "CategoryName",
                table: "Quizzes");

            migrationBuilder.AddColumn<int>(
                name: "CategoryId",
                table: "Quizzes",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "ParentName",
                table: "QuizzCategories",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "QuizzCategories",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100);

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "QuizzCategories",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_QuizzCategories",
                table: "QuizzCategories",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Quizzes_CategoryId",
                table: "Quizzes",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_QuizzCategories_Name_ParentName",
                table: "QuizzCategories",
                columns: new[] { "Name", "ParentName" },
                unique: true,
                filter: "[Name] IS NOT NULL AND [ParentName] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Quizzes_QuizzCategories_CategoryId",
                table: "Quizzes",
                column: "CategoryId",
                principalTable: "QuizzCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Quizzes_QuizzCategories_CategoryId",
                table: "Quizzes");

            migrationBuilder.DropIndex(
                name: "IX_Quizzes_CategoryId",
                table: "Quizzes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_QuizzCategories",
                table: "QuizzCategories");

            migrationBuilder.DropIndex(
                name: "IX_QuizzCategories_Name_ParentName",
                table: "QuizzCategories");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Quizzes");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "QuizzCategories");

            migrationBuilder.AddColumn<string>(
                name: "CategoryName",
                table: "Quizzes",
                type: "nvarchar(100)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "ParentName",
                table: "QuizzCategories",
                type: "nvarchar(100)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "QuizzCategories",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_QuizzCategories",
                table: "QuizzCategories",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Quizzes_CategoryName",
                table: "Quizzes",
                column: "CategoryName");

            migrationBuilder.CreateIndex(
                name: "IX_QuizzCategories_ParentName",
                table: "QuizzCategories",
                column: "ParentName");

            migrationBuilder.AddForeignKey(
                name: "FK_QuizzCategories_QuizzCategories_ParentName",
                table: "QuizzCategories",
                column: "ParentName",
                principalTable: "QuizzCategories",
                principalColumn: "Name",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Quizzes_QuizzCategories_CategoryName",
                table: "Quizzes",
                column: "CategoryName",
                principalTable: "QuizzCategories",
                principalColumn: "Name",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
