﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuizzHeaven.Migrations
{
    public partial class Added_FullTextCatalog_To_The_DB_And_FullTextIndex_On_Quizzes_QuizzName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($@"
                CREATE FULLTEXT CATALOG fulltext_catalog_QuizzHeaven AS DEFAULT;

                CREATE FULLTEXT INDEX
                    ON Quizzes(name)
                    KEY INDEX PK_Quizzes
                    WITH STOPLIST = system;
            ", true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($@"
                DROP FULLTEXT INDEX ON Quizzes
                DROP FULLTEXT CATALOG fulltext_catalog_QuizzHeaven
            ", true);
        }
    }
}
