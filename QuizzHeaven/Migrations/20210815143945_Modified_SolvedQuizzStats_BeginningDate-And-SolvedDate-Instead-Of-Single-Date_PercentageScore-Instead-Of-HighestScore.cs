﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuizzHeaven.Migrations
{
    public partial class Modified_SolvedQuizzStats_BeginningDateAndSolvedDateInsteadOfSingleDate_PercentageScoreInsteadOfHighestScore : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SolvedQuizzesStats_UserId_QuizzId",
                table: "SolvedQuizzesStats");

            migrationBuilder.DropCheckConstraint(
                name: "CK_SolvedQuizzesStats_HighestScorePercentage_RestrictScoreToPercentageRange",
                table: "SolvedQuizzesStats");

            migrationBuilder.RenameColumn(
                name: "HighestScorePercentage",
                table: "SolvedQuizzesStats",
                newName: "PercentageScore");

            migrationBuilder.AlterColumn<DateTime>(
                name: "SolvedDate",
                table: "SolvedQuizzesStats",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<DateTime>(
                name: "BeginningDate",
                table: "SolvedQuizzesStats",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_SolvedQuizzesStats_UserId_QuizzId",
                table: "SolvedQuizzesStats",
                columns: new[] { "UserId", "QuizzId" },
                unique: true)
                .Annotation("SqlServer:Include", new[] { "SolvedDate", "PercentageScore", "QuizzNameSnapshot", "QuizzVersionSnapshot" });

            migrationBuilder.AddCheckConstraint(
                name: "CK_SolvedQuizzesStats_PercentageScore_RestrictScoreToPercentageRange",
                table: "SolvedQuizzesStats",
                sql: "[PercentageScore] >= 0 AND [PercentageScore] <= 100");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SolvedQuizzesStats_UserId_QuizzId",
                table: "SolvedQuizzesStats");

            migrationBuilder.DropCheckConstraint(
                name: "CK_SolvedQuizzesStats_PercentageScore_RestrictScoreToPercentageRange",
                table: "SolvedQuizzesStats");

            migrationBuilder.DropColumn(
                name: "BeginningDate",
                table: "SolvedQuizzesStats");

            migrationBuilder.RenameColumn(
                name: "PercentageScore",
                table: "SolvedQuizzesStats",
                newName: "HighestScorePercentage");

            migrationBuilder.AlterColumn<DateTime>(
                name: "SolvedDate",
                table: "SolvedQuizzesStats",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SolvedQuizzesStats_UserId_QuizzId",
                table: "SolvedQuizzesStats",
                columns: new[] { "UserId", "QuizzId" },
                unique: true)
                .Annotation("SqlServer:Include", new[] { "SolvedDate", "HighestScorePercentage", "QuizzNameSnapshot", "QuizzVersionSnapshot" });

            migrationBuilder.AddCheckConstraint(
                name: "CK_SolvedQuizzesStats_HighestScorePercentage_RestrictScoreToPercentageRange",
                table: "SolvedQuizzesStats",
                sql: "[HighestScorePercentage] >= 0 AND [HighestScorePercentage] <= 100");
        }
    }
}
