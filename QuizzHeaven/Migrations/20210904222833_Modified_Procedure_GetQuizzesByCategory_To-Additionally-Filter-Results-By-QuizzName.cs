﻿using Microsoft.EntityFrameworkCore.Migrations;
using QuizzHeaven.Data.Configuration;
using QuizzHeaven.Data.Consts.SQL;

namespace QuizzHeaven.Migrations
{
    public partial class Modified_Procedure_GetQuizzesByCategory_ToAdditionallyFilterResultsByQuizzName : Migration
    {
        const string RootCategoryNameParam = "@RootCategoryName";
        const string QuizzNameParam = "@QuizzName";
        const string PageNumberParam = "@PageNumber";
        const string PageSizeParam = "@PageSize";

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($@"
                {DDLConsts.CreateOrAlterProcedure} {StoredProceduresConsts.GetQuizzesByCategoryStoredProcName}(
	                {RootCategoryNameParam} nvarchar({CategoryConfigurationConsts.Name_MaxLength}), 
	                {QuizzNameParam} nvarchar({QuizzConfigurationConsts.Name_MaxLength}),
	                {PageNumberParam} int = 1,
	                {PageSizeParam} int = {int.MaxValue})
                as

                SET {QuizzNameParam} = IIF(trim({QuizzNameParam}) = '' OR {QuizzNameParam} IS NULL, '""', {QuizzNameParam})
                SET {PageNumberParam} = IIF({PageNumberParam} > 0, {PageNumberParam}, 1)
                SET {PageSizeParam} = IIF({PageSizeParam} > 0, {PageSizeParam}, {int.MaxValue})

                SELECT * FROM Quizzes
                WHERE CategoryName IN (SELECT * FROM {FunctionsConsts.Categories_GetSubcategoriesFunctionName}({RootCategoryNameParam})) 
                AND (FREETEXT(Name, {QuizzNameParam}) OR {QuizzNameParam} = '""')
                ORDER BY Id
                OFFSET ({PageNumberParam}-1)*{PageSizeParam} ROWS 
                FETCH NEXT {PageSizeParam} ROWS ONLY"
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($@"
                    {DDLConsts.CreateOrAlterProcedure} {StoredProceduresConsts.GetQuizzesByCategoryStoredProcName}({RootCategoryNameParam} nvarchar({CategoryConfigurationConsts.Name_MaxLength}))
                    as

                    select * from Quizzes
                    where CategoryName in (select * from {FunctionsConsts.Categories_GetSubcategoriesFunctionName}({RootCategoryNameParam}));
            ");
        }
    }
}
