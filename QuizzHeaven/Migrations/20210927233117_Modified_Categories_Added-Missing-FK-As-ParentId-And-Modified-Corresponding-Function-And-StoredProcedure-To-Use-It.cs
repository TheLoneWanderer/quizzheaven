﻿using Microsoft.EntityFrameworkCore.Migrations;
using QuizzHeaven.Data.Configuration;
using QuizzHeaven.Data.Consts.SQL;

namespace QuizzHeaven.Migrations
{
    public partial class Modified_Categories_AddedMissingFKAsParentIdAndModifiedCorrespondingFunctionAndStoredProcedureToUseIt : Migration
    {
        const string QuizzNameParam = "@QuizzName";
        const string PageNumberParam = "@PageNumber";
        const string PageSizeParam = "@PageSize";

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_QuizzCategories_Name_ParentName",
                table: "QuizzCategories");

            migrationBuilder.DropColumn(
                name: "ParentName",
                table: "QuizzCategories");

            migrationBuilder.AddColumn<int>(
                name: "ParentId",
                table: "QuizzCategories",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_QuizzCategories_Name_ParentId",
                table: "QuizzCategories",
                columns: new[] { "Name", "ParentId" },
                unique: true,
                filter: "[Name] IS NOT NULL AND [ParentId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_QuizzCategories_ParentId",
                table: "QuizzCategories",
                column: "ParentId");

            migrationBuilder.AddForeignKey(
                name: "FK_QuizzCategories_QuizzCategories_ParentId",
                table: "QuizzCategories",
                column: "ParentId",
                principalTable: "QuizzCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            const string RootCategoryIdParam = "@RootCategoryId";

            migrationBuilder.Sql($@"
                    {DDLConsts.CreateOrAlterFunction} {FunctionsConsts.Categories_GetSubcategoriesFunctionName}({RootCategoryIdParam} int)
                    returns table

                    return(
	                    with cteWrapper as (
		                    select RootCategory.Id, RootCategory.Name
		                    from QuizzCategories RootCategory
		                    where Id = {RootCategoryIdParam}

		                    union all
		                    select ChildCategory.Id, ChildCategory.Name
		                    from QuizzCategories ChildCategory
		                    join cteWrapper cte on cte.Id = ChildCategory.ParentId
		                    )

	                    select Id from cteWrapper
                    );"
                );

            migrationBuilder.Sql($@"
                {DDLConsts.CreateOrAlterProcedure} {StoredProceduresConsts.GetQuizzesByCategoryStoredProcName}(
	                {RootCategoryIdParam} int, 
	                {QuizzNameParam} nvarchar({QuizzConfigurationConsts.Name_MaxLength}),
	                {PageNumberParam} int = 1,
	                {PageSizeParam} int = {int.MaxValue})
                as

                SET {QuizzNameParam} = IIF(trim({QuizzNameParam}) = '' OR {QuizzNameParam} IS NULL, '""', {QuizzNameParam})
                SET {PageNumberParam} = IIF({PageNumberParam} > 0, {PageNumberParam}, 1)
                SET {PageSizeParam} = IIF({PageSizeParam} > 0, {PageSizeParam}, {int.MaxValue})

                SELECT * FROM Quizzes
                WHERE CategoryId IN (SELECT * FROM {FunctionsConsts.Categories_GetSubcategoriesFunctionName}({RootCategoryIdParam})) 
                AND (FREETEXT(Name, {QuizzNameParam}) OR {QuizzNameParam} = '""')
                ORDER BY Id
                OFFSET ({PageNumberParam}-1)*{PageSizeParam} ROWS 
                FETCH NEXT {PageSizeParam} ROWS ONLY"
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_QuizzCategories_QuizzCategories_ParentId",
                table: "QuizzCategories");

            migrationBuilder.DropIndex(
                name: "IX_QuizzCategories_Name_ParentId",
                table: "QuizzCategories");

            migrationBuilder.DropIndex(
                name: "IX_QuizzCategories_ParentId",
                table: "QuizzCategories");

            migrationBuilder.DropColumn(
                name: "ParentId",
                table: "QuizzCategories");

            migrationBuilder.AddColumn<string>(
                name: "ParentName",
                table: "QuizzCategories",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_QuizzCategories_Name_ParentName",
                table: "QuizzCategories",
                columns: new[] { "Name", "ParentName" },
                unique: true,
                filter: "[Name] IS NOT NULL AND [ParentName] IS NOT NULL");

            const string RootCategoryNameParam = "@RootCategoryName";

            migrationBuilder.Sql($@"
                    {DDLConsts.CreateOrAlterFunction} {FunctionsConsts.Categories_GetSubcategoriesFunctionName}({RootCategoryNameParam} nvarchar({CategoryConfigurationConsts.Name_MaxLength}))
                    returns table

                    return(
	                    with cteWrapper as (
		                    select RootCategory.Name 
		                    from QuizzCategories RootCategory
		                    where Name = {RootCategoryNameParam}

		                    union all
		                    select ChildCategory.Name
		                    from QuizzCategories ChildCategory
		                    join cteWrapper cte on cte.Name = ChildCategory.ParentName
		                    )

	                    select * from cteWrapper
                    );"
                );

            migrationBuilder.Sql($@"
                {DDLConsts.CreateOrAlterProcedure} {StoredProceduresConsts.GetQuizzesByCategoryStoredProcName}(
	                {RootCategoryNameParam} nvarchar({CategoryConfigurationConsts.Name_MaxLength}), 
	                {QuizzNameParam} nvarchar({QuizzConfigurationConsts.Name_MaxLength}),
	                {PageNumberParam} int = 1,
	                {PageSizeParam} int = {int.MaxValue})
                as

                SET {QuizzNameParam} = IIF(trim({QuizzNameParam}) = '' OR {QuizzNameParam} IS NULL, '""', {QuizzNameParam})
                SET {PageNumberParam} = IIF({PageNumberParam} > 0, {PageNumberParam}, 1)
                SET {PageSizeParam} = IIF({PageSizeParam} > 0, {PageSizeParam}, {int.MaxValue})

                SELECT * FROM Quizzes
                WHERE CategoryName IN (SELECT * FROM {FunctionsConsts.Categories_GetSubcategoriesFunctionName}({RootCategoryNameParam})) 
                AND (FREETEXT(Name, {QuizzNameParam}) OR {QuizzNameParam} = '""')
                ORDER BY Id
                OFFSET ({PageNumberParam}-1)*{PageSizeParam} ROWS 
                FETCH NEXT {PageSizeParam} ROWS ONLY"
            );
        }
    }
}
