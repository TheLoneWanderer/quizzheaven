﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuizzHeaven.Migrations
{
    public partial class Modified_SolvedQuizzStats_RemovedUniqueConstraintFromTheUserIdQuizzIdCompositeIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SolvedQuizzesStats_UserId_QuizzId",
                table: "SolvedQuizzesStats");

            migrationBuilder.CreateIndex(
                name: "IX_SolvedQuizzesStats_UserId_QuizzId",
                table: "SolvedQuizzesStats",
                columns: new[] { "UserId", "QuizzId" })
                .Annotation("SqlServer:Include", new[] { "SolvedDate", "PercentageScore", "QuizzNameSnapshot", "QuizzVersionSnapshot" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SolvedQuizzesStats_UserId_QuizzId",
                table: "SolvedQuizzesStats");

            migrationBuilder.CreateIndex(
                name: "IX_SolvedQuizzesStats_UserId_QuizzId",
                table: "SolvedQuizzesStats",
                columns: new[] { "UserId", "QuizzId" },
                unique: true)
                .Annotation("SqlServer:Include", new[] { "SolvedDate", "PercentageScore", "QuizzNameSnapshot", "QuizzVersionSnapshot" });
        }
    }
}
