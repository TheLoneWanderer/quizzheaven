﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuizzHeaven.Migrations
{
    public partial class Added_Constraint_For_SolvedQuizzStats_HighestScorePercentage_RestrictScoreToPercentageRange_And_Added_Various_MaxLength_Constraints : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SolvedQuizzesStats_UserId_QuizzId",
                table: "SolvedQuizzesStats");

            migrationBuilder.DropIndex(
                name: "IX_Quizzes_Name",
                table: "Quizzes");

            migrationBuilder.AddColumn<string>(
                name: "QuizzNameSnapshot",
                table: "SolvedQuizzesStats",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "QuizzVersionSnapshot",
                table: "SolvedQuizzesStats",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "VersionStamp",
                table: "Quizzes",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AlterColumn<string>(
                name: "Content",
                table: "Questions",
                type: "nvarchar(240)",
                maxLength: 240,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "AnswerD",
                table: "Questions",
                type: "nvarchar(80)",
                maxLength: 80,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AnswerC",
                table: "Questions",
                type: "nvarchar(80)",
                maxLength: 80,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AnswerB",
                table: "Questions",
                type: "nvarchar(80)",
                maxLength: 80,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "AnswerA",
                table: "Questions",
                type: "nvarchar(80)",
                maxLength: 80,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.CreateIndex(
                name: "IX_SolvedQuizzesStats_UserId_QuizzId",
                table: "SolvedQuizzesStats",
                columns: new[] { "UserId", "QuizzId" },
                unique: true)
                .Annotation("SqlServer:Include", new[] { "SolvedDate", "HighestScorePercentage", "QuizzNameSnapshot", "QuizzVersionSnapshot" });

            migrationBuilder.AddCheckConstraint(
                name: "CK_SolvedQuizzesStats_HighestScorePercentage_RestrictScoreToPercentageRange",
                table: "SolvedQuizzesStats",
                sql: "[HighestScorePercentage] >= 0 AND [HighestScorePercentage] <= 100");

            migrationBuilder.CreateIndex(
                name: "IX_Quizzes_Name",
                table: "Quizzes",
                column: "Name",
                unique: true)
                .Annotation("SqlServer:Include", new[] { "Description", "VersionStamp", "AuthorId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SolvedQuizzesStats_UserId_QuizzId",
                table: "SolvedQuizzesStats");

            migrationBuilder.DropCheckConstraint(
                name: "CK_SolvedQuizzesStats_HighestScorePercentage_RestrictScoreToPercentageRange",
                table: "SolvedQuizzesStats");

            migrationBuilder.DropIndex(
                name: "IX_Quizzes_Name",
                table: "Quizzes");

            migrationBuilder.DropColumn(
                name: "QuizzNameSnapshot",
                table: "SolvedQuizzesStats");

            migrationBuilder.DropColumn(
                name: "QuizzVersionSnapshot",
                table: "SolvedQuizzesStats");

            migrationBuilder.DropColumn(
                name: "VersionStamp",
                table: "Quizzes");

            migrationBuilder.AlterColumn<string>(
                name: "Content",
                table: "Questions",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(240)",
                oldMaxLength: 240);

            migrationBuilder.AlterColumn<string>(
                name: "AnswerD",
                table: "Questions",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(80)",
                oldMaxLength: 80,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AnswerC",
                table: "Questions",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(80)",
                oldMaxLength: 80,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AnswerB",
                table: "Questions",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(80)",
                oldMaxLength: 80);

            migrationBuilder.AlterColumn<string>(
                name: "AnswerA",
                table: "Questions",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(80)",
                oldMaxLength: 80);

            migrationBuilder.CreateIndex(
                name: "IX_SolvedQuizzesStats_UserId_QuizzId",
                table: "SolvedQuizzesStats",
                columns: new[] { "UserId", "QuizzId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Quizzes_Name",
                table: "Quizzes",
                column: "Name",
                unique: true);
        }
    }
}
