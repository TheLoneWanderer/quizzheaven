﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuizzHeaven.Migrations
{
    public partial class Added_Questions_QuizzIdDisplayOrder_And_Answers_QuestionIdDisplayOrder_Unique_Constraints : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Questions_QuizzId",
                table: "Questions");

            migrationBuilder.DropIndex(
                name: "IX_Answers_QuestionId",
                table: "Answers");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_QuizzId_DisplayOrder",
                table: "Questions",
                columns: new[] { "QuizzId", "DisplayOrder" })
                .Annotation("SqlServer:Include", new[] { "Id", "Content" });

            migrationBuilder.CreateIndex(
                name: "IX_Answers_QuestionId_DisplayOrder",
                table: "Answers",
                columns: new[] { "QuestionId", "DisplayOrder" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Questions_QuizzId_DisplayOrder",
                table: "Questions");

            migrationBuilder.DropIndex(
                name: "IX_Answers_QuestionId_DisplayOrder",
                table: "Answers");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_QuizzId",
                table: "Questions",
                column: "QuizzId")
                .Annotation("SqlServer:Include", new[] { "Id", "Content" });

            migrationBuilder.CreateIndex(
                name: "IX_Answers_QuestionId",
                table: "Answers",
                column: "QuestionId");
        }
    }
}
