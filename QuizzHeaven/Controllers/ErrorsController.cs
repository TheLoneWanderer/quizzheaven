﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using QuizHeaven.Helpers;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Helpers.CustomExceptions.Server;
using QuizzHeaven.Helpers.ErrorHandling.HttpResponseError;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace QuizzHeaven.Controllers
{
    [ApiController]
    public class ErrorsController : ControllerBase
    {
        [Route("/error")]
        public IActionResult Error()
        {
            var exception = HttpContext.Features.Get<IExceptionHandlerFeature>().Error;
            IList<string> errors = new List<string>() { exception.Message };

            switch (exception)
            {
                case BadRequestException badRequestException:
                {
                    return BadRequest(new HttpResponseErrorDto(HttpStatusCode.BadRequest, errors));
                }

                case UnauthorizedException unauthorizedException:
                {
                    return Unauthorized(new HttpResponseErrorDto(HttpStatusCode.Unauthorized, errors));
                }

                case ForbiddenException forbiddenException:
                {
                    return StatusCode((int)HttpStatusCode.Forbidden, new HttpResponseErrorDto(HttpStatusCode.Forbidden, errors));
                }

                case NotFoundException notFoundException:
                {
                    return NotFound(new HttpResponseErrorDto(HttpStatusCode.NotFound, errors));
                }

                case ConflictException conflictException:
                {
                    return Conflict(new HttpResponseErrorDto(HttpStatusCode.Conflict, errors));
                }

                case UnprocessableLogicException unprocessableLogicException:
                {
                    errors = unprocessableLogicException.Errors;
                    return UnprocessableEntity(new HttpResponseErrorDto(HttpStatusCode.UnprocessableEntity, errors));
                }

                case ServiceUnavailableException serviceUnavailableException:
                {
                    return StatusCode((int)HttpStatusCode.ServiceUnavailable, new HttpResponseErrorDto(HttpStatusCode.ServiceUnavailable, errors));
                }

                default:
                {
                    var unspecifiedErrorCode = (int)HttpStatusCode.InternalServerError;
                    return StatusCode(unspecifiedErrorCode);
                }
            }
        }
    }
}
