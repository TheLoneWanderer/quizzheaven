﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuizzHeaven.Helpers.CustomExceptions;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Services;

namespace QuizzHeaven.Controllers
{
    [ApiController]
    [Route("api")]
    public class AuthenticationController : ControllerBase
    {
        private IAuthenticationService authenticationService;

        public AuthenticationController(IAuthenticationService AuthenticationService)
        {
            authenticationService = AuthenticationService;
        }

        [HttpPost]
        [Route("users")]
        public async Task<IActionResult> SignUp(SignUpInput signUpInput)
        {
            await authenticationService.SignUpAsync(signUpInput);
            return Ok();
        }     

        [HttpPost]
        [Route("sessions")]
        public async Task<IActionResult> SignIn(SignInInput signInInput)
        {            
            var accessToken = await authenticationService.SignInAsync(signInInput);
            return Ok(accessToken);
        }
    }
}
