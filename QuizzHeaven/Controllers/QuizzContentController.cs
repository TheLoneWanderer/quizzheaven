﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuizzHeaven.Helpers.Consts;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Controllers
{
    [Authorize(Policy = AuthConsts.Policies.UserAccess)]
    [Route("api")]
    [ApiController]
    public class QuizzContentController : ControllerBase
    {
        private IQuizzContentService quizzContentService;

        public QuizzContentController(IQuizzContentService contentService)
        {
            quizzContentService = contentService;
        }


        [HttpGet("quizzes-content/{quizzId}")]
        public async Task<IActionResult> GetContent(int quizzId)
        {
            var content = await quizzContentService.BeginStaticQuestionsAttemptAsync(quizzId, User.GetCurrentUserId());
            return Ok(content);
        }
    }
}
