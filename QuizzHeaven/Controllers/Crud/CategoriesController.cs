﻿using AutoMapper;
using HybridModelBinding;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using QuizzHeaven.Helpers.Consts;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Models.DTO.Search;
using QuizzHeaven.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Policy = AuthConsts.Policies.AdminAccess)]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private ICategoriesService categoriesService;

        public CategoriesController(ICategoriesService categoriesService)
        {
            this.categoriesService = categoriesService;
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateCategoryInput categoryInput)
        {
            var createdEntity = await categoriesService.CreateAsync(categoryInput);
            return CreatedAtAction(nameof(Get), new { Id = createdEntity.Id }, createdEntity);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] SearchParameters searchParams)
        {
            var categoriesVm = await categoriesService.GetAllAsync(searchParams);
            return Ok(categoriesVm);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var categoryVm = await categoriesService.GetByKeyAsync(id);
            return Ok(categoryVm);
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> Edit(int id, [FromHybrid] EditCategoryInput categoryInput)
        {
            await categoriesService.EditPartiallyAsync(categoryInput, id);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await categoriesService.DeleteAsync(id);
            return NoContent();
        }
    }
}
