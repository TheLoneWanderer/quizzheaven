﻿using HybridModelBinding;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuizzHeaven.Helpers.Consts;
using QuizzHeaven.Helpers.CustomAttributes;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Controllers
{
    [Route("api")]
    [ApiController]
    public class AnswersController : ControllerBase
    {
        IAnswersService answersService;

        public AnswersController(IAnswersService answersService)
        {
            this.answersService = answersService;
        }

        [HttpGet("questions/{questionId}/answers")]
        public async Task<IActionResult> GetByQuestion(int questionId)
        {
            var answersByQuestion = await answersService.GetByQuestionAsync(questionId);
            return Ok(answersByQuestion);
        }

        [HttpGet("answers/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var answer = await answersService.GetByKeyAsync(id);
            return Ok(answer);
        }

        [AuthorizeWithOwnership(typeof(IQuestionOwnershipValidator), idArgumentName: "questionId")]
        [HttpPost("questions/{questionId}/answers")]
        public async Task<IActionResult> Create(CreateAnswerInput createInput)
        {
            var createdAnswer = await answersService.CreateAsync(createInput);
            return CreatedAtAction(nameof(Get), new { id = createdAnswer.Id }, createdAnswer);
        }

        [AuthorizeWithOwnership(typeof(IAnswerOwnershipValidator))]
        [HttpPatch("answers/{id}")]
        public async Task<IActionResult> Edit(int id, [FromHybrid] EditAnswerInput editInput)
        {
            await answersService.EditPartiallyAsync(editInput, id);
            return NoContent();
        }

        [AuthorizeWithOwnership(typeof(IAnswerOwnershipValidator), policiesRequiredIfNonOwner: AuthConsts.Policies.ModeratorAccess)]
        [HttpDelete("answers/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await answersService.DeleteAsync(id);
            return NoContent();
        }
    }
}
