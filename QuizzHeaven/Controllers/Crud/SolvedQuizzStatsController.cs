﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuizzHeaven.Models.DTO.Search;
using QuizzHeaven.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Controllers
{
    [Route("api")]
    [ApiController]
    public class SolvedQuizzStatsController : ControllerBase
    {
        private ISolvedQuizzStatsService solvedStatsService;

        public SolvedQuizzStatsController(ISolvedQuizzStatsService solvedStatsService)
        {
            this.solvedStatsService = solvedStatsService;
        }

        [HttpGet("quizzes/{quizzId}/solved-stats")]
        public async Task<IActionResult> GetByQuizz(int quizzId, [FromQuery] SearchParameters searchParams)
        {
            var stats = await solvedStatsService.GetByQuizzAsync(quizzId, searchParams);
            return Ok(stats);
        }

        [HttpGet("users/{userId}/quizzes/solved-stats")]
        public async Task<IActionResult> GetByUser(string userId, [FromQuery] SearchParameters searchParams)
        {
            var stats = await solvedStatsService.GetByUserAsync(userId, searchParams);
            return Ok(stats);
        }

        [HttpGet("users/{userId}/quizzes/{quizzId}/solved-stats")]
        public async Task<IActionResult> GetForUserByQuizz(string userId, int quizzId, [FromQuery] SearchParameters searchParams)
        {
            var stats = await solvedStatsService.GetForUserByQuizzAsync(userId, quizzId, searchParams);
            return Ok(stats);
        }

        [HttpGet("users/{userId}/categories/total-solved")]
        public async Task<IActionResult> GetTotalSolvedForUserByCategories(string userId, [FromQuery] SearchParameters searchParams)
        {
            var stats = await solvedStatsService.GetTotalSolvedForUserByCategoriesAsync(userId, searchParams);
            return Ok(stats);
        }
    }
}
