﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuizzHeaven.Helpers.Consts;
using QuizzHeaven.Helpers.CustomAttributes;
using QuizzHeaven.Models.Domain;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Models.DTO.Search;
using QuizzHeaven.Services;
using QuizzHeaven.Services.OwnershipValidators;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace QuizzHeaven.Controllers
{
    [Route("api")]
    [ApiController]
    public class QuizzesController : ControllerBase
    {
        private IQuizzService quizzService;

        public QuizzesController(IQuizzService quizzService)
        {
            this.quizzService = quizzService;
        }

        [HttpGet("quizzes")]
        public async Task<IActionResult> Get([FromQuery] SearchParameters searchParams)
        {
            var quizzes = await quizzService.GetAllAsync(searchParams);
            return Ok(quizzes);
        }

        [HttpGet("quizzes/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var quizz = await quizzService.GetByKeyAsync(id);
            return Ok(quizz);
        }

        [HttpGet("categories/{categoryId}/quizzes")]
        public async Task<IActionResult> GetByCategory(int categoryId, [FromQuery] SearchParameters searchParams)
        {
            var quizzesByCategory = await quizzService.GetByCategoryAsync(categoryId, searchParams);
            return Ok(quizzesByCategory);
        }

        [HttpGet("users/{username}/quizzes")]
        public async Task<IActionResult> GetByUser(string username)
        {
            var quizzesByUser = await quizzService.GetByUserAsync(username);
            return Ok(quizzesByUser);
        }

        [Authorize(Policy = AuthConsts.Policies.UserAccess)]
        [ModelBindUserId(nameof(CreateQuizzInput.AuthorId))]
        [HttpPost("quizzes")]
        public async Task<IActionResult> Create(CreateQuizzInput inputEntity)
        {
            var createdEntity = await quizzService.CreateAsync(inputEntity);
            return CreatedAtAction(nameof(Get), new { id = createdEntity.Id }, createdEntity);
        }

        [AuthorizeWithOwnership(typeof(IQuizzOwnershipValidator))]
        [HttpPatch("quizzes/{id}")]
        public async Task<IActionResult> Edit(int id, [FromBody] EditQuizzInput inputEntity)
        {
            await quizzService.EditPartiallyAsync(inputEntity, id);
            return NoContent();
        }

        [AuthorizeWithOwnership(typeof(IQuizzOwnershipValidator), policiesRequiredIfNonOwner: AuthConsts.Policies.ModeratorAccess)]
        [HttpDelete("quizzes/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await quizzService.DeleteAsync(id);
            return NoContent();
        }
    }
}
