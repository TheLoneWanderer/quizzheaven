﻿using HybridModelBinding;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuizzHeaven.Helpers.Consts;
using QuizzHeaven.Helpers.CustomAttributes;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Models.DTO.Search;
using QuizzHeaven.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Controllers
{
    [Route("api")]
    [ApiController]
    public class QuestionsController : ControllerBase
    {
        IQuestionsService questionsService;

        public QuestionsController(IQuestionsService questionsService)
        {
            this.questionsService = questionsService;
        }

        [HttpGet("quizzes/{quizzId}/questions")]
        public async Task<IActionResult> GetByQuizz(int quizzId)
        {
            var questions = await questionsService.GetByQuizzAsync(quizzId);
            return Ok(questions);
        }

        [HttpGet("questions/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var question = await questionsService.GetByKeyAsync(id);
            return Ok(question);
        }

        [AuthorizeWithOwnership(typeof(IQuizzOwnershipValidator), idArgumentName: "quizzId")]
        [HttpPost("quizzes/{quizzId}/questions")]
        public async Task<IActionResult> Create([FromHybrid] CreateQuestionInput input)
        {
            var createdQuestion = await questionsService.CreateAsync(input);
            return CreatedAtAction(nameof(Get), new { id = createdQuestion.Id }, createdQuestion);
        }

        [AuthorizeWithOwnership(typeof(IQuestionOwnershipValidator))]
        [HttpPatch("questions/{id}")]
        public async Task<IActionResult> Edit(int id, [FromHybrid] EditQuestionInput input)
        {
            await questionsService.EditPartiallyAsync(input, id);
            return NoContent();
        }

        [AuthorizeWithOwnership(typeof(IQuestionOwnershipValidator), policiesRequiredIfNonOwner: AuthConsts.Policies.ModeratorAccess)]
        [HttpDelete("questions/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await questionsService.DeleteAsync(id);
            return NoContent();
        }
    }
}
