﻿using HybridModelBinding;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuizzHeaven.Helpers.Consts;
using QuizzHeaven.Helpers.CustomAttributes;
using QuizzHeaven.Models.DTO;
using QuizzHeaven.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Controllers
{
    [Authorize(Policy = AuthConsts.Policies.UserAccess)]
    [Route("api")]
    [ApiController]
    public class QuizzResultsController : ControllerBase
    {
        private IQuizzResultsService resultsService;

        public QuizzResultsController(IQuizzResultsService quizzResultsService)
        {
            this.resultsService = quizzResultsService;
        }

        [ModelBindUserId(nameof(QuizzAnswersInput.UserId))]
        [HttpPost("quizzes/{quizzId}/results")]
        public async Task<IActionResult> ValidateAnswers([FromHybrid] QuizzAnswersInput inputAnswers)
        {
            var answersResults = await resultsService.ValidateAnswersAsync(inputAnswers);
            return Ok(answersResults);
        }
    }
}
