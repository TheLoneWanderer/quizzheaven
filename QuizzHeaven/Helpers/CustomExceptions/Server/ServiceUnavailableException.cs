﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.CustomExceptions.Server
{
    public class ServiceUnavailableException : Exception
    {
        public ServiceUnavailableException(string errorMessage)
            : base (errorMessage)
        {
        }
    }
}
