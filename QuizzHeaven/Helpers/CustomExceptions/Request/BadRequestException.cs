﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.CustomExceptions.Request
{
    public class BadRequestException : Exception
    {
        public BadRequestException(string errorMessage)
            : base(errorMessage)
        {
        }
    }
}
