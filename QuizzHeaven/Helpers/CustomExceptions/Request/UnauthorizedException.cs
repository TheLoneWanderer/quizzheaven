﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.CustomExceptions.Request
{
    public class UnauthorizedException : Exception
    {
        public UnauthorizedException(string errorMessage)
            : base(errorMessage)
        {
        }
    }
}
