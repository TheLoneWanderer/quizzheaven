﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.CustomExceptions.Request
{
    public class ConflictException : Exception
    {
        public ConflictException(string errorMessage)
            : base(errorMessage)
        { 
        }
    }
}
