﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.CustomExceptions.Request
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string errorMessage)
            : base(errorMessage)
        {
        }
    }
}
