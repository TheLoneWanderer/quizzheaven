﻿using QuizHeaven.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.CustomExceptions.BusinessLogic
{
    public abstract class MultipleFailuresException : Exception
    {
        public MultipleFailuresException()
        {
            Errors = new List<string>();
        }

        public MultipleFailuresException(string errorDescription)
            : this()
        {
            Errors.Add(errorDescription);
        }

        public MultipleFailuresException(List<string> errorsDescriptions)
            : this()
        {
            Errors.AddRange(errorsDescriptions);
        }

        public List<string> Errors { get; }
    }
}
