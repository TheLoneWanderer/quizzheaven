﻿using QuizzHeaven.Helpers.CustomExceptions.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.CustomExceptions.Request
{
    public class UnprocessableLogicException : MultipleFailuresException
    {
        public UnprocessableLogicException() : base("Error occured while processing application logic")
        {
        }

        public UnprocessableLogicException(string errorDescription) : base(errorDescription)
        {
        }

        public UnprocessableLogicException(List<string> errorDescriptions) : base(errorDescriptions)
        {
        }
    }
}
