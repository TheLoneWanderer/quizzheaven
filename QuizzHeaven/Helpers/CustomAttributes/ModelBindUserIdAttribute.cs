﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.CustomAttributes
{
    /// <summary>
    /// Search through the action method input models and assign the requesting user's ID if they contain any property with the specified name
    /// </summary>
    public class ModelBindUserIdAttribute : Attribute, IActionFilter
    {
        string userIdPropertyName;

        public ModelBindUserIdAttribute(string userIdPropertyName)
        {
            this.userIdPropertyName = userIdPropertyName;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var userIdValue = context.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;         

            var actionMethodInputs = context.ActionArguments.Where(argumentEntry => argumentEntry.Value.GetType().IsClass).Select(argumentEntry => argumentEntry.Value);
            foreach(var inputArgument in actionMethodInputs)
            {
                var userIdProperties = inputArgument.GetType().GetProperties().Where(property => IsStringWithName(property, userIdPropertyName));
                foreach (var property in userIdProperties)
                {
                    property.SetValue(inputArgument, userIdValue);
                }
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        private bool IsStringWithName(PropertyInfo property, string requiredName)
        {
            return (property.PropertyType == typeof(string) && property.Name == requiredName);
        }
    }
}
