﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Helpers.ExtensionMethods;
using QuizzHeaven.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.CustomAttributes
{
    /// <summary>
    /// Works pretty much like the default .NET AuthorizeAttribute, but also checks whether the requested entity is owned by the current user
    /// </summary>
    public class AuthorizeWithOwnershipAttribute : AuthorizeAttribute, IAsyncAuthorizationFilter
    {
        readonly Type validatorType;
        readonly string entityIdParameterName;
        readonly string alternativeNonOwnerPolicies;

        public AuthorizeWithOwnershipAttribute(Type ownershipValidatorType, string idArgumentName = "id") : this(ownershipValidatorType, "", idArgumentName)
        {
        }

        public AuthorizeWithOwnershipAttribute(Type ownershipValidatorType, string policiesRequiredIfNonOwner, string idArgumentName = "id")
        {
            validatorType = ownershipValidatorType;
            entityIdParameterName = idArgumentName;
            alternativeNonOwnerPolicies = policiesRequiredIfNonOwner;
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var ownershipValidator = (IOwnershipValidator)context.HttpContext.RequestServices.GetService(validatorType);
            var userId = context.HttpContext.User.GetCurrentUserId();
            var entityId = context.HttpContext.Request.RouteValues[entityIdParameterName].ToString();

            var isOwner = await ownershipValidator.IsOwner(userId, entityId);
            var nonOwnerPoliciesFulfilled = false;

            if (!string.IsNullOrWhiteSpace(alternativeNonOwnerPolicies))
            {
                var identityAuthService = (IAuthorizationService)context.HttpContext.RequestServices.GetService(typeof(IAuthorizationService));
                nonOwnerPoliciesFulfilled = (await identityAuthService.AuthorizeAsync(context.HttpContext.User, alternativeNonOwnerPolicies)).Succeeded;
            }

            if (!isOwner && !nonOwnerPoliciesFulfilled)
            {
                throw new ForbiddenException("This entity does not belong to you. You are not allowed to access it.");
            }
        }
    }
}
