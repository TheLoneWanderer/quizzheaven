﻿using FluentValidation;
using FluentValidation.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuizzHeaven.Helpers.ExtensionMethods.String;

namespace QuizzHeaven.Helpers.FluentValidation.PropertyValidators
{
    public static partial class PropertyValidatorsExtension
    {
        public static IRuleBuilderOptions<T, string> NotEmptyOrWhiteSpace<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new NotEmptyOrWhiteSpaceValidator<T>());
        }
    }

    public class NotEmptyOrWhiteSpaceValidator<T> : PropertyValidator<T, string>
    {
        public override bool IsValid(ValidationContext<T> context, string value)
        {
            return !value.IsWhiteSpaceOrEmpty();
        }

        public override string Name => nameof(NotEmptyOrWhiteSpaceValidator<T>);

        protected override string GetDefaultMessageTemplate(string errorCode)
        {
            return "Value must be either null or contain any characters.";
        }
    }
}
