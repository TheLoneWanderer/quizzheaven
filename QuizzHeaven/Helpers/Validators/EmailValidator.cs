﻿using QuizHeaven.Helpers.OperationResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.Validators
{
    public class EmailValidator : IValidator<string>
    {
        public bool IsValid(string emailCandidate)
        {
            return MailAddress.TryCreate(emailCandidate, out _);
        }
    }
}
