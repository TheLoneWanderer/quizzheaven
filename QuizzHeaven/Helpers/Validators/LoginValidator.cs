﻿using QuizHeaven.Helpers.OperationResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.Validators
{
    public class LoginValidator : IValidator<string>
    {
        public bool IsValid(string loginCandidate)
        {
            string loginRegexStr = @"^[a-zA-Z0-9]*$";
            return Regex.IsMatch(loginCandidate, loginRegexStr);
        }
    }
}
