﻿using QuizHeaven.Helpers.OperationResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.Validators
{
    interface IValidator<TEntity>
    {
        public bool IsValid(TEntity entityToValidate);
    }
}
