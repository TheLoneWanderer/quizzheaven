﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizHeaven.Helpers.OperationResult
{
    public class OperationResultFromIdentityResult : OperationResult
    {
        public OperationResultFromIdentityResult(IdentityResult identityResult)
        {
            List<string> errorsList = new List<string>();

            foreach (var error in identityResult.Errors)
            {
                errorsList.Add(error.Description);
            }

            AddErrors(errorsList);
        }
    }
}
