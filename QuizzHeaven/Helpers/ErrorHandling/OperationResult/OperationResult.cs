﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace QuizHeaven.Helpers.OperationResult
{
    public class OperationResult : IOperationResult
    {
        /// <summary>
        /// In case the operation succeeded
        /// </summary>
        public OperationResult()
        {
            succeeded = true;
            errorsDictionary = new ErrorsDictionary();
        }

        /// <summary>
        /// In case the operation failed and returned an error message
        /// </summary>
        /// <param name="errorsGroupKey"></param>
        /// <param name="errorMessage"></param>
        public OperationResult(string errorMessage, string errorsGroupKey = "Errors")
        {
            succeeded = false;
            errorsDictionary = new ErrorsDictionary(errorMessage, errorsGroupKey);
        }

        /// <summary>
        /// In case the operation failed and you already have the errors dictionary created
        /// </summary>
        /// <param name="errorsDictionary"></param>
        public OperationResult(Dictionary<string, List<string>> errorsDictionary)
        {
            succeeded = false;
            this.errorsDictionary = new ErrorsDictionary(errorsDictionary);
        }

        /// <summary>
        /// In case the result came from the Identity framework
        /// </summary>
        /// <param name="identityResult"></param>
        public OperationResult(IdentityResult identityResult)
        {
            succeeded = false;
            errorsDictionary = new ErrorsDictionary();

            List<string> errorsList = new List<string>();
            foreach (var error in identityResult.Errors)
                errorsList.Add(error.Description);

            AddErrors(errorsList);
        }

        public bool IsSuccess()
        {
            return succeeded;
        }

        public void AddError(string errorMessage, string errorsGroupKey = "Errors")
        {
            succeeded = false;
            errorsDictionary.AddError(errorMessage, errorsGroupKey);
        }

        public void AddErrors(List<string> errorMessages, string errorsGroupKey = "Errors")
        {
            succeeded = false;
            errorsDictionary.AddErrors(errorMessages, errorsGroupKey);
        }

        public Dictionary<string, List<string>> Errors { get => errorsDictionary.GetErrors(); }

        private ErrorsDictionary errorsDictionary { get; set; }
        private bool succeeded;
    }


    public class OperationResult<TResult> : OperationResult, IOperationResult<TResult> where TResult : class, new()
    {
        /// <summary>
        /// In case the operation succeeded
        /// </summary>
        public OperationResult() : base()
        {
            Result = new TResult();
        }

        /// <summary>
        /// In case the operation succeeded with a result
        /// </summary>
        /// <param name="result"></param>
        public OperationResult(TResult result) : base()
        {
            Result = result;
        }

        /// <summary>
        /// In case the operation failed and returned an error message and an empty result
        /// </summary>
        /// <param name="errorsGroupKey"></param>
        /// <param name="errorMessage"></param>
        public OperationResult(string errorMessage, string errorsGroupKey = "Errors") : base(errorMessage, errorsGroupKey)
        {
            Result = new TResult();
        }

        /// <summary>
        /// In case the operation failed and you already have the errors dictionary created
        /// </summary>
        /// <param name="errorsDictionary"></param>
        public OperationResult(Dictionary<string, List<string>> errorsDictionary) : base(errorsDictionary)
        {
            Result = new TResult();
        }

        /// <summary>
        /// In case the result came from the Identity framework
        /// </summary>
        /// <param name="identityResult"></param>
        public OperationResult(IdentityResult identityResult) : base(identityResult)
        {
            Result = new TResult();
        }

        public TResult Result { get; private set; }
    }
}
