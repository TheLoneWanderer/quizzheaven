﻿using System.Collections.Generic;

namespace QuizHeaven.Helpers.OperationResult
{
    public interface IOperationResult
    {
        bool IsSuccess();
        void AddError(string errorMessage, string errorGroupKey = "Errors");
        void AddErrors(List<string> errorMessages, string errorGroupKey = "Errors");
        Dictionary<string, List<string>> Errors { get; }
    }

    public interface IOperationResult<TResult> : IOperationResult
    {
        TResult Result { get; }
    }
}
