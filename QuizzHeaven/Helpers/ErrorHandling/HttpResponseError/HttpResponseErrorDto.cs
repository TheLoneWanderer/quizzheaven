﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.ErrorHandling.HttpResponseError
{
    public class HttpResponseErrorDto
    {
        public HttpResponseErrorDto(HttpStatusCode statusCode, IList<string> errors)
        {
            StatusCode = statusCode;
            Errors = errors;
            PropertiesErrors = new Dictionary<string, IList<string>>();
        }

        public HttpResponseErrorDto(HttpStatusCode statusCode, IDictionary<string, IList<string>> propertiesErrors)
        {
            StatusCode = statusCode;
            Errors = new List<string>();
            PropertiesErrors = propertiesErrors;
        }

        public HttpStatusCode StatusCode { get; }
        public IList<string> Errors { get; }
        public IDictionary<string, IList<string>> PropertiesErrors { get; }
    }
}
