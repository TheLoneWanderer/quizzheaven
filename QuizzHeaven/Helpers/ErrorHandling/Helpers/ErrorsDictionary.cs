﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizHeaven.Helpers
{
    public class ErrorsDictionary
    {
        /// <summary>
        /// Creates an object with no errors
        /// </summary>
        public ErrorsDictionary()
        {
            errors = new Dictionary<string, List<string>>();
        }

        /// <summary>
        /// Creates an object with a single error
        /// </summary>
        public ErrorsDictionary(string errorMessage, string errorsGroupKey = "Errors") : this()
        {
            AddError(errorMessage, errorsGroupKey);
        }

        /// <summary>
        /// Creates an object with a list of errors
        /// </summary>
        public ErrorsDictionary(List<string> errorMessages, string errorsGroupKey = "Errors") : this()
        {
            AddErrors(errorMessages, errorsGroupKey);
        }

        public ErrorsDictionary(Dictionary<string, List<string>> errorsDictionary)
        {
            errors = errorsDictionary;
        }

        public void AddError(string errorMessage, string errorsGroupKey = "Errors")
        {
            List<string> listAssignedToKey;
            if(!errors.TryGetValue(errorsGroupKey, out listAssignedToKey))
            {
                listAssignedToKey = new List<string>();
                errors[errorsGroupKey] = listAssignedToKey;
            };

            listAssignedToKey.Add(errorMessage);
        }

        public void AddErrors(List<string> errorMessages, string errorsGroupKey = "Errors")
        {
            List<string> listAssignedToKey;
            if(!errors.TryGetValue(errorsGroupKey, out listAssignedToKey))
            {
                errors[errorsGroupKey] = errorMessages;
            }
            else
            {
                listAssignedToKey.AddRange(errorMessages);
            }
        }

        public Dictionary<string, List<string>> GetErrors()
        {
            return errors;
        }

        private Dictionary<string, List<string>> errors;
    }
}
