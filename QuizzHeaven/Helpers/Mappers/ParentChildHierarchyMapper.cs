﻿using AutoMapper;
using QuizzHeaven.Helpers.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.Mappers
{
    public class ParentChildHierarchyMapper
    {
        IMapper mapper;

        public ParentChildHierarchyMapper(IMapper mapper)
        {
            this.mapper = mapper;
        }

        public List<TNestedEntity> UnflattenList<TFlatEntity, TNestedEntity, TEntityIdentifier>(
        List<TFlatEntity> flatEntities,
        Func<TFlatEntity, TEntityIdentifier> flatIdSelector,
        Func<TFlatEntity, TEntityIdentifier> flatParentIdSelector,
        Func<TNestedEntity, TEntityIdentifier> nestedIdSelector,
        Expression<Func<TNestedEntity, IList<TNestedEntity>>> childEntitiesSelector)
        where TFlatEntity : class
        where TNestedEntity : class, new()
        {
            var entitiesByParentLookup = flatEntities.ToLookup(entity => flatParentIdSelector(entity), entity => mapper.Map<TNestedEntity>(entity));
            var entitiesList = entitiesByParentLookup.SelectMany(lookupGrouping => lookupGrouping.AsEnumerable());

            foreach (var entity in entitiesList)
            {
                var currentEntityId = nestedIdSelector(entity);
                var childEntities = entitiesByParentLookup[currentEntityId].ToList();

                childEntitiesSelector.SetValue(entity, childEntities);
            }

            var rootEntitiesIds = flatEntities.Where(entity => flatParentIdSelector(entity) == null).Select(entity => flatIdSelector(entity));
            var entitiesHierarchyRoot = entitiesList.Where(entity => rootEntitiesIds.Contains(nestedIdSelector(entity))).ToList();

            return entitiesHierarchyRoot;
        }
    }
}
