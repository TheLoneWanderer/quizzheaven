﻿using Microsoft.AspNetCore.Identity;
using QuizzHeaven.Data;
using QuizzHeaven.Helpers.Consts;
using QuizzHeaven.Models.Domain;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.DataSeeders
{
    public class StartupDataSeeder
    {
        AppDbContext dbContext;
        RoleManager<IdentityRole> roleManager;

        public StartupDataSeeder(AppDbContext dbContext, RoleManager<IdentityRole> identityRoleManager)
        {
            this.dbContext = dbContext;
            this.roleManager = identityRoleManager;
        }

        public async Task SeedAsync()
        {
            await seedUserRolesAsync();
        }

        private async Task seedUserRolesAsync()
        {
            foreach(var roleName in AuthConsts.Roles.AvailableRoles)
            {
                var roleExists = await roleManager.RoleExistsAsync(roleName);
                if(!roleExists)
                {
                    await roleManager.CreateAsync(new IdentityRole(roleName));
                }
            }
        }
    }
}
