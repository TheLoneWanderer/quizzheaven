﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.Consts
{
    public static class AuthConsts
    {
        public static class Policies
        {
            public const string AdminAccess = "AdminAccess";
            public const string ModeratorAccess = "ModeratorAccess";
            public const string UserAccess = "UserAccess";
        }

        public static class Roles
        {
            public static string[] AvailableRoles => new string[] { AdminRole, ModeratorRole, UserRole };

            public const string AdminRole = "Admin";
            public const string ModeratorRole = "Moderator";
            public const string UserRole = "User";
        }
    }
}
