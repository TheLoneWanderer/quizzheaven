﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.ExtensionMethods.String
{
    public static class StringConditionalExtensionMethods
    {
        public static bool IsWhiteSpaceOrEmpty(this string value)
        {
            if(value == null)
            {
                return false;
            }

            return value.Trim().Length == 0;
        }
    }
}
