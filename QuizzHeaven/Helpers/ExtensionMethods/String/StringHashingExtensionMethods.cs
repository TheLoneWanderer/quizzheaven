﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.ExtensionMethods.String
{
    public static class StringHashingExtensionMethods
    {
        /// <summary>
        /// Returns a MD5 hash of the original string
        /// <br></br>
        /// It should only be used for non security related data since it's too fast to crack
        /// </summary>
        /// <param name="clearText"></param>
        /// <returns></returns>
        public static string AsMD5Hash(this string clearText)
        {
            var clearTextBytes = Encoding.UTF8.GetBytes(clearText);

            var hashGenerator = MD5.Create();
            var hashBytes = hashGenerator.ComputeHash(clearTextBytes);
            var hashAsString = BitConverter.ToString(hashBytes).Replace("-", string.Empty);

            return hashAsString;
        }
    }
}
