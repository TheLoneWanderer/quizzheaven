﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.ExtensionMethods.String
{
    public static class StringNormalizationExtensionMethods
    {
        /// <summary>
        /// <br> Basically the ToUpperInvariant method, but it replaces the string with an empty one if it's null. </br>
        /// <br> It's used mainly for Dictionaries to normalize the user input key. </br>
        /// </summary>
        /// <param name="inputString"></param>
        public static string ToUpperInvariantOrEmpty(this string inputString)
        {
            if(inputString != null)
            {
                return inputString.ToUpperInvariant();
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
