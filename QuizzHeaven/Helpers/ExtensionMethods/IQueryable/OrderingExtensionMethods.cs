﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace QuizzHeaven.Helpers.ExtensionMethods.IQueryable
{
    public static class OrderingExtensionMethods
    {
        public static IOrderedQueryable<TEntity> OrderByDirectionally<TEntity, TKey>(this IQueryable<TEntity> query, Expression<Func<TEntity, TKey>> sortPropertySelector, bool sortDescending)
        {
            if(sortDescending)
            {
                return query.OrderByDescending(sortPropertySelector);
            }
            else
            {
                return query.OrderBy(sortPropertySelector);
            }
        }
    }
}
