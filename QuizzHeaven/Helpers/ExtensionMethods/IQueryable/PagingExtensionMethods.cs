﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.ExtensionMethods
{
    public static class PagingExtensionMethods
    {
        public static IQueryable<TEntity> Page<TEntity>(this IQueryable<TEntity> query, int pageNumber, int pageSize)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            if (pageSize < 1)
                pageSize = 1000;

            return query.Skip((pageNumber-1) * pageSize).Take(pageSize);
        }
    }
}
