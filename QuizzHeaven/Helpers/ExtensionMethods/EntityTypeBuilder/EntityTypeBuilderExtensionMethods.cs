﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.ExtensionMethods
{
    public static class EntityTypeBuilderExtensionMethods
    {
        public static string GetTableName<TDomainModel>(this EntityTypeBuilder<TDomainModel> builder) where TDomainModel : class
        {
            return builder.Metadata.Model.FindEntityType(typeof(TDomainModel)).GetTableName();
        }

        public static string GetColumnNameByProperty<TDomainModel>(this EntityTypeBuilder<TDomainModel> builder, string propertyName) where TDomainModel : class
        {
            var tableName = GetTableName<TDomainModel>(builder);
            return builder.Metadata.Model.FindEntityType(typeof(TDomainModel)).GetProperties().ToDictionary(prop => prop.Name, prop => prop.GetColumnName(StoreObjectIdentifier.Table(tableName, null)))[propertyName];
        }
    }
}
