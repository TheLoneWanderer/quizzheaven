﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.ExtensionMethods
{
    public static class ClaimsPrincipalExtensionMethods
    {
        public static string GetCurrentUserId(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.FindFirstValue(ClaimTypes.NameIdentifier);
        }

        public static bool IsCurrentUser(this ClaimsPrincipal claimsPrincipal, string userId)
        {
            var currentUserId = GetCurrentUserId(claimsPrincipal);
            return currentUserId == userId;
        }
    }
}
