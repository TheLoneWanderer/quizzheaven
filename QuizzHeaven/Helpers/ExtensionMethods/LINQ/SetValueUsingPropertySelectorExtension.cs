﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.ExtensionMethods
{
    public static class SetValueUsingPropertySelectorExtension
    {
        public static void SetValue<TEntity, TProperty>(this Expression<Func<TEntity, TProperty>> selector, TEntity targetEntity, TProperty value) where TEntity : class
        {
            var memberExpression = (MemberExpression) selector.Body;
            var propertyInfo = (PropertyInfo) memberExpression.Member;

            propertyInfo.SetValue(targetEntity, value, null);
        }
    }
}
