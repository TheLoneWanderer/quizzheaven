﻿using Microsoft.EntityFrameworkCore;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Helpers.ExtensionMethods.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.ExtensionMethods
{
    public static class DbContextExtensionMethods
    {
        /// <summary>
        /// Stops tracking every entity regardless of its state
        /// </summary>
        /// <param name="dbContext"></param>
        public static void DetachAllEntities(this DbContext dbContext)
        {
            var entityEntries = dbContext.ChangeTracker.Entries()
                                                       .Where(e => e.State != EntityState.Detached)
                                                       .ToList();

            foreach (var entry in entityEntries)
                entry.State = EntityState.Detached;
        }

        public static bool Exists<TEntity>(this DbContext dbContext, params object[] entityKey) where TEntity : class
        {
            var entity = dbContext.Find<TEntity>(entityKey);
            return entity != null;
        }

        public static async Task<bool> ExistsAsync<TEntity, TEntityKey>(this DbContext dbContext, Expression<Func<TEntity, TEntityKey>> entityKeySelector, Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            var entity = await dbContext.Set<TEntity>().Where(predicate).Select(entityKeySelector).FirstOrDefaultAsync();
            return !entity.IsDefaultValue();
        }

        /// <summary>
        /// Throws a custom NotFoundException when entity does not exist
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="dbContext"></param>
        /// <param name="entityKeys"></param>
        /// <returns></returns>
        public static async Task EnsureEntityExists<TEntity, TEntityKey>(this DbContext dbContext, Expression<Func<TEntity, TEntityKey>> entityKeySelector, Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            var entityExists = await ExistsAsync(dbContext, entityKeySelector, predicate);
            if(!entityExists)
            {
                throw new NotFoundException($"{typeof(TEntity).Name} does not exist");
            }
        }
    }
}
