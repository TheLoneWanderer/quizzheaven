﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.ExtensionMethods
{
    public static class DbContextMetadataExtensionMethods
    {
        public static string GetTableName<TModel>(this DbContext dbContext) where TModel : class
        {
            return dbContext.Model.FindEntityType(typeof(TModel)).GetTableName();
        }

        public static string GetColumnNameByProperty<TModel>(this DbContext dbContext, string propertyName) where TModel : class
        {
            var tableName = GetTableName<TModel>(dbContext);
            var columnName = dbContext.Model.FindEntityType(typeof(TModel)).GetProperty(propertyName).GetColumnName(StoreObjectIdentifier.Table(tableName, null));

            return columnName;
        }
    }
}
