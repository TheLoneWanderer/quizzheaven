﻿using Microsoft.AspNetCore.Identity;
using QuizzHeaven.Helpers.CustomExceptions.Request;
using QuizzHeaven.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.ExtensionMethods.IdentityUserManager
{
    public static class UserManagerExtensionMethods
    {
        /// <summary>
        /// Finds and returns the user by either an email or username
        /// </summary>
        /// <typeparam name="TIdentityUser"></typeparam>
        /// <param name="userManager"></param>
        /// <param name="login"></param>
        /// <returns></returns>
        public static async Task<TIdentityUser> FindByNameOrEmailAsync<TIdentityUser>(this UserManager<TIdentityUser> userManager, string login) where TIdentityUser : IdentityUser
        {
            TIdentityUser user;
            if (login.Contains('@'))
            {
                user = await userManager.FindByEmailAsync(login);
            }
            else
            {
                user = await userManager.FindByNameAsync(login);
            }

            if (user == null)
            {
                throw new UnauthorizedException("Entered credentials don't match any account");
            }

            return user;
        }
    }
}
