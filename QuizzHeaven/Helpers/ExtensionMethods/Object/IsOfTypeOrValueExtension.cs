﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.ExtensionMethods.Object
{
    public static class IsOfTypeOrValueExtension
    {
        /// <summary>
        /// Checks whether object is a boxed empty string or contains only whitespaces
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        public static bool IsAnEmptyString(this object property)
        {
            if(property is not string)
            {
                return false;
            }

            var propertyAsString = (string)property;
            var isEmptyString = propertyAsString.Trim().Length == 0;

            return isEmptyString;
        }
    }
}
