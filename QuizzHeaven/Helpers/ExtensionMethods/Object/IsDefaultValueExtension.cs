﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzHeaven.Helpers.ExtensionMethods.Object
{
    public static class IsDefaultValueExtension
    {
        public static bool IsDefaultValue<TProperty>(this TProperty property)
        {
            // deal with normal scenarios
            if (property == null) 
                return true;

            if (object.Equals(property, default(TProperty))) 
                return true;

            // deal with non-null nullables
            Type genericType = typeof(TProperty);
            if (Nullable.GetUnderlyingType(genericType) != null) 
                return false;

            // deal with boxed value types
            Type propertyType = property.GetType();
            if (propertyType.IsValueType && propertyType != genericType)
            {
                object propertyInstance = Activator.CreateInstance(propertyType);
                return propertyInstance.Equals(property);
            }

            return false;
        }
    }
}
